# SPDX-Filename: ./Makefile
# SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
# SPDX-License-Identifier: GPL-3.0-only

CUR_DATE=$(shell date +%F_%X)
BUNDLE_NAME=${CUR_DATE}-meta-press-ext_7zip

7zip:
	7z a -tzip -mx=9 \
			-xr!.git* \
			-xr!*.swp \
			-xr!*.swo \
			-x!.jsdoc_conf.json \
			-x!.eslintrc.json \
			-xr!LICENSE \
			-xr!README.adoc \
			-x!*.gitmodules \
			-x!Makefile \
			-x!TODO.adoc \
			-x!geckodriver.log \
			-x!lost_keys* \
			-x!.config \
			-x!css/bootstrap/css/bootstrap.css \
			-x!css/bootstrap/config.json \
			-x!css/codemirror.css \
			-x!css/codemirror_lint.css \
			-x!html_locales/template.json \
			-x!html_locales/black_list.json \
			-x!html_locales/update_black_list \
			-x!img/src \
			-x!img/Meta-Press.es_title.png \
			-x!img/metapressv4-ralenti__.gif \
			-x!img/Meta-Press.es_title_from_eps.svg \
			-x!img/Meta-Press.es_title_from_eps_dark.svg \
			-x!js/gettext_html_auto.js/update_locales.js \
			-x!js/deps/codemirror/codemirror.js \
			-x!js/deps/codemirror/codemirror_mode_javascript.js \
			-x!js/deps/codemirror/codemirror_addon_json-lint.js \
			-x!js/deps/codemirror/codemirror_addon_lint.js \
			-x!js/deps/codemirror/jsonlint.js \
			-x!js/deps/jsondiffpatch.umd.slim.js \
			-x!json/broken_sources.json \
			-x!json/test_meta-press.es.json \
			-x!scripts \
			-x!wiki \
		../${BUNDLE_NAME} .


backend_xpi:
	# Deprecated, Selenium can load a WebExtension local dev folder
	# jq, from eponymous Debian package ; sponge from moreutils package
	jq '.permissions += ["downloads", "<all_urls>"]' manifest.json | sponge manifest.json
	make 7zip
	mv ../${BUNDLE_NAME}.zip ../${BUNDLE_NAME}.xpi
	jq '.permissions -= ["downloads", "<all_urls>"]' manifest.json | sponge manifest.json


selenium_setup:
	# - Install selenium and python wrapper : pacman -S python-selenium
	# - Download latest geckodriver : wget https://github.com/mozilla/geckodriver/releases/XXX
	# -- at '/usr/bin/geckodriver' or FF_DRIVER_PATH (environment variable)
	# - Download Firefox developer edition
	# -- at '~/.mozilla/firefox-developer-edition/firefox/firefox-bin' or FF_BINARY_PATH

selenium_ff_tests_headless:
	date
	d=$$(date +%s); \
	SELENIUM_BROWSER='firefox' HEADLESS_DRIVER=1 scripts/py/selenium_testing.py \
	&& echo '$$(($$(date +%s)-d)) seconds'

selenium_ff_headed_tests:
	date
	d=$$(date +%s); \
	SELENIUM_BROWSER='firefox' HEADLESS_DRIVER='' scripts/py/selenium_testing.py \
	&& echo '$$(($$(date +%s)-d)) seconds'

selenium_cium_headed_tests:
	date
	d=$$(date +%s);\
	OFFLINE_TESTS=1 SELENIUM_BROWSER='chromium' HEADLESS_DRIVER='' scripts/py/selenium_testing.py\
	&& echo '$$(($$(date +%s)-d)) seconds'

selenium_test_all: selenium_ff_tests_headless selenium_cium_headed_tests

DL_DIR=/tmp
PORT=9876
BIND_ADR=127.0.0.1
HOST=http://${BIND_ADR}:${PORT}
HTTP_srv_args=-m http.server --bind ${BIND_ADR} ${PORT}
CHROMIUM=chromium --user-data-dir=.config --incognito
FIREFOX=scripts/sh/firefox-priv.sh
BROWSER=${CHROMIUM}

rm_new_template:
	rm '${DL_DIR}/template.json'

mv_template:
	mv '${DL_DIR}/template.json' html_locales/

start_HTTP_server:
	python ${HTTP_srv_args} &  # binding sor 127.0.0.1 only speed up start up time 10s -> 0.25s
	while ! curl -s -D - ${HOST} -o /dev/null | head -n1 | grep 200; \
		do echo wait for http.server; sleep 0.25; done;

black_list_update: start_HTTP_server
	rm -f html_locales/black_list.json
	# rm -f '${HOME}/Téléchargements/black_list.json'
	rm -f '${DL_DIR}/black_list.json'
	# node html_locales/update_black_list.js	# node's Intl.DisplayNames can't do 'region' 2022-05
	# ${FIREFOX} ${HOST}/html_locales/update_black_list/update_black_list.html
	${BROWSER} "${HOST}/html_locales/update_black_list/update_black_list.html" &
	inotifywait -e close_write --quiet --include 'black_list.json' '${DL_DIR}'
	# mv '${HOME}/Téléchargements/black_list.json' html_locales/
	mv '${DL_DIR}/black_list.json' html_locales/
	# make kill_browser
	# make kill_HTTP_server

kill_HTTP_server:
	pkill -f 'python ${HTTP_srv_args}'

kill_browser:
	pkill -f '${BROWSER}'

diff_template:
	# colordiff html_locales/template.json ~/Téléchargements/template.json
	jdiff -i 2 html_locales/template.json '${DL_DIR}/template.json'

renew_template: black_list_update
	rm -f '${DL_DIR}/template.json'
	rm -f html_locales/template.json
	rm -rf .config/* || true
	echo '{}' >  html_locales/template.json
	# reload index.html in english with no tag selected
	# make start_HTTP_server
	${BROWSER} '${HOST}/html/index.html?xgettext=1&keep_empty_tags=1' &
	inotifywait -e close_write --quiet --include 'template.json' '${DL_DIR}'
	make mv_template
	# make kill_browser
	${BROWSER} '${HOST}/html/settings.html?xgettext=1' &
	inotifywait -e close_write --quiet --include 'template.json' '${DL_DIR}'
	make mv_template
	# make kill_browser
	${BROWSER} '${HOST}/html/welcome.html?xgettext=1' &
	inotifywait -e close_write --quiet --include 'template.json' '${DL_DIR}'
	make mv_template
	# make kill_browser
	${BROWSER} '${HOST}/html/custom_source.html?xgettext=1' &
	inotifywait -e close_write --quiet --include 'template.json' '${DL_DIR}'
	make mv_template
	# make kill_browser
	${BROWSER} '${HOST}/html/edit_source.html?xgettext=1' &
	inotifywait -e close_write --quiet --include 'template.json' '${DL_DIR}'
	make mv_template
	# make update_locales
	make kill_browser
	make kill_HTTP_server
	# review new keys, one page could be missing (like the Welcome page)

update_locales:
	node js/gettext_html_auto.js/update_locales.js

install_ReDOS_checker:
	cd .. && git clone https://github.com/NicolaasWeideman/RegexStaticAnalysis
	# mvn stands for the Apache Java 'maven' project management tool
	cd RegexStaticAnalysis && mvn package

check_for_ReDOS:
	cat json/sources.json | grep -P '(r_dt_fmt_[1-9]|res_nb_re|r_.+_re|warn_mask")' --after-context=1 | grep -Po '(?<=\t").*(?=",)' | sed 's/\\\\\\\\/\\\\/g' | sort | uniq > ../autogenerated_regex_list.txt
	cd ../RegexStaticAnalysis/ && ./run.sh --simple --if=../autogenerated_regex_list.txt > ../`date +%F_%X`_ReDOS_checker_report.txt
	# Verification performed, check ReDOS_checker_report.txt
	# Problematic regex are vulnerable to Exponential Degree of Ambiguity (or Infinite D.A.)
	# [INFO] Total time:	01:20 min with Intel Core i7 6Y75
	# ex : 51s	Analysed: 180 ; safe 155
	# ex : 3,5s Analysed: 167 ; safe 164
	# ex : 28s  Analysed: 243 ; safe 234 20221031
	# ex : 12s  Analysed: 251 ; safe 251 20221031

RELEASE_VERSION = `jq -r '.version' manifest.json`
release:
	# check dependencies versions
	# check everything works also in Chromium
	# check HTML against W3C validator
	# update i18n
	# check_for_ReDOS
	git tag v${RELEASE_VERSION}
	git push origin v${RELEASE_VERSION}
	git branch v${RELEASE_VERSION}
	# yui-compressor css/meta-press.css -o css/meta-press.css
	# uglifyjs --compress --mangle -o codemirror.min.js -- codemirror.js
	make 7zip
	git checkout master
	# bump version number in manifest.json (ex. 1.X.Y+1)
	# update documentation for new features
	# announce the update on the website (and active the update announcer)
	# upload on addons.Mozilla.org

doc:
	asciidoctor README.adoc
	asciidoctor documentation.adoc
	jsdoc js -c .jsdoc_conf.json -d docs
	mv 'documentation.html' ./docs/
	mv 'README.html' ./docs/

.PHONY: zip black_list_update mv_template update_locales
