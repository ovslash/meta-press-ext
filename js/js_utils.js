// SPDX-FileName: ./js_utils.js
// SPDX-FileCopyrightText: 2017-2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

export const is = (val) => typeof val !== 'undefined'
export const is_str = (val) => typeof val === 'string'
export const is_obj = (val) => typeof val === 'object'
export const is_nb  = (val) => typeof val === 'number'
export const is_arr = (val) => Array.isArray(val)
export const is_in  = (lst, val) => lst.indexOf(val) !== -1
/**
 * Remove the 1st occurrence of the value in the given array.
 * @param {Array} array work space
 * @param {*} value The value to remove
 * @returns {Array} without value
 */
export function array_remove_once(arr, val) {
	let idx = arr.indexOf(val)
	if (idx > -1) arr.splice(idx, 1)
}
/**
 * Add a value in an Array only if it's not already in.
 * @param {Array} Array work space
 * @param {*} value The value to add
 * @returns {Array} The entry Array that might have been modified
 */
export function array_add_once(arr, val) {
	if(arr.indexOf(val) === -1) arr.push(val)
	return arr  // should be removed
}
/**
 * Removes duplicata in a array.
 * @param {Array} array The working array
 * @returns A new clean array
 */
export function remove_duplicate_in_array(arr) { return [...new Set(arr)] }
/*
 *
 */
export function shuffle_array(arr) {
	for (let i = arr.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[arr[i], arr[j]] = [arr[j], arr[i]]
	}
}
/** Returns a deep copy of an object, via JSON.parse(JSON.stringify(obj))
 * @param {Object} obj The object to duplicate
 * @returns {Object} a clone of the given object
 */
export function deep_copy(obj){ return JSON.parse(JSON.stringify(obj)) }
/**
 * Get the value at the given path of a JSON JSON object
 * @param {string} JSON_path The JSON path like : sources.invidius.tags.name
 * @param {Object} JSON_obj The object to search in
 * @returns The found value or undefined
 */
export function deref_JSON_path(JSON_path, JSON_obj) {
	let val = JSON_obj
	if (JSON_path)
		for (let JSON_path_elt of JSON_path.split('.')) {
			try {
				val = val[JSON_path_elt]
			} catch (exc) {
				return ''
			}
		}
	return val
}
/**
 * Format the given string to remove thousand separators from integers (so US ',' or Fr '.'/' ')
 * @param {string} str The string to be converted to Number()
 * @returns the string without any /,./g
 */
export function do_int (str) { return str && str.replace(/[,.\s]/g, '') || 0 }
/**
 * Generate a UUIDv4 (Unique Universal Identifier version 4)
 * @returns {string} UUIDv4
 */
export function uuidv4() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	)
}
/**
 * Encode the string for xml (<, >, &, ', ").
 * @param {string} unsafe The string to encode
 * @returns {string} The encoded string
 */
export function encode_XML(unsafe) {
	unsafe = removeXMLInvalidChars(unsafe, false)
	// if true, all text is removed (removeDiscouragedChars)
	return unsafe.replace(new RegExp('[<>&\'"]', 'g'), (c) => {
		switch (c) {
		case '<': return '&lt;'
		case '>': return '&gt;'
		case '&': return '&amp;'
		case "'": return '&apos;'
		case '"': return '&quot;'
		}
	} )
}
/**
 * Removes invalid XML characters from a string
 * From https://gist.github.com/john-doherty/b9195065884cdbfd2017a4756e6409cc
 * @param {string} str - a string containing potentially invalid XML char. (non-UTF8, STX, EOX)
 * @param {boolean} removeDiscouragedChars - should it remove discouraged but valid XML char.
 * @return {string} a sanitized string stripped of invalid XML characters
 */
function removeXMLInvalidChars(str, removeDiscouragedChars=true) {
	// remove everything forbidden by XML 1.0 specifications, plus the unicode replacement
	// character U+FFFD
	let regex = new RegExp('((?:[\0-\x08\x0B\f\x0E-\x1F\uFFFD\uFFFE\uFFFF]|[\uD800-\uDBFF]' +
		'(?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]))', 'g')
	str = str.replace(regex, '')
	if (removeDiscouragedChars) {
		// remove everything discouraged by XML 1.0 specifications
		regex = new RegExp(
			'([\\x7F-\\x84]|[\\x86-\\x9F]|[\\uFDD0-\\uFDEF]|(?:\\uD83F[\\uDFFE\\uDFFF])|' +
		' (?:\\uD87F[\\uDFFE\\uDFFF])|(?:\\uD8BF[\\uDFFE\\uDFFF])|(?:\\uD8FF[\\uDFFE\\uDFFF])|'+
		'(?:\\uD93F[\\uDFFE\\uDFFF])|(?:\\uD97F[\\uDFFE\\uDFFF])|(?:\\uD9BF[\\uDFFE\\uDFFF])|'+
		'(?:\\uD9FF[\\uDFFE\\uDFFF])|(?:\\uDA3F[\\uDFFE\\uDFFF])|(?:\\uDA7F[\\uDFFE\\uDFFF])|'+
		'(?:\\uDABF[\\uDFFE\\uDFFF])|(?:\\uDAFF[\\uDFFE\\uDFFF])|(?:\\uDB3F[\\uDFFE\\uDFFF])|'+
		'(?:\\uDB7F[\\uDFFE\\uDFFF])|(?:\\uDBBF[\\uDFFE\\uDFFF])|(?:\\uDBFF[\\uDFFE\\uDFFF])|'+
		'(?:[\\0-\\t\\x0B\\f\\x0E-\\u2027\\u202A-\\uD7FF\\uE000-\\uFFFF]|[\\uD800-\\uDBFF]'+
		'[\\uDC00-\\uDFFF]|[\\uD800-\\uDBFF](?![\\uDC00-\\uDFFF])|(?:[^\\uD800-\\uDBFF]|^)'+
		'[\\uDC00-\\uDFFF]))', 'g')
		str = str.replace(regex, '')
	}
	return str
}
/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
 * $& means the whole matched string
 */
export function escape_RegExp(string) { return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') }
/**
 * Cut the string to the at size if string is too long.
 * @param {string} str The string
 * @param {number} at The needed size
 * @returns {string} the shorten str
 */
export function shorten(s, at) { return s && s.length > at ? `${s.slice(0, at)}…` : s || '' }
/**
 * Remove trailing space before and after the string.
 * @param {string} str Working string
 * @returns {string} The clean string
 */
export function trim(str) { return str && str.replace(/^\s*|\s*$|&/g, '') || ''}
/**
 * Remove trailing space before and after the string and the double space (\s\s) in the string.
 * @param {string} str Working string
 * @returns {string}
 */
export function triw(str) { return str && str.replace(/^\s*|\s*$|(\s\s)+/g, '') || '' }
/**
 * Remove all spaces.
 * @param {string} str Working string
 * @returns {string}
 */
export function dry_triw(str) { return str && str.replace(/\s*/g, '') }
export function drop_low_unprintable_utf8(str) {
	str = str.replace(/[\u0000-\u0009]/g, '')					 // eslint-disable-line no-control-regex
	str.replace(/[\u000B-\u000C]/g, '')								 // eslint-disable-line no-control-regex
	return str && str.replace(/[\u000E-\u001F]/g, '')  // eslint-disable-line no-control-regex
}
/**
 * Quote the special character in the string.
 * From : http://kevin.vanzonneveld.net, http://magnetiq.com
 * @param {string} str Working string
 * @example preg_quote("How many? $40") returns 'How many\? \$40'
 * @example preg_quote("\\.+*?[^]$(){}=!<>|:"); returns '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'
 * @returns {string} The string prepared
 */
export function preg_quote(str) {
	// str = String(str).replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1")
	str = String(str).replace(/([\\.+*?[^\]$(){}=!<>|:])/g, '\\$1')
	return str.replace(/\s/g, '\\s')	// ensure we match converted &nbsp;
}
export function bolden (str, search) {
	if (typeof str !== 'string') return ''
	let search_term_list = search.split(' ')
	for (let i of search_term_list)
		str = str.replace(new RegExp(`(${preg_quote(i)})`, 'gi'), '<b>$1</b>')
	return str
}
/**
 * Quote the special character in the string for a file.
 * @param {string} str Working string
 * @returns {string} The string prepared
 */
export function preg_quote_file(str) { return str.replace(/([\\"',])/g, '\\$1') }
export function drop_escaped_quote_file(str) { return str.replace(/\\(["',\n])/g, '$1') }
/**
 * Remove the \' (and \") in a string.
 * @param {string} str The string to clean
 * @returns {string}
 */
export function drop_escaped_quotes(str) { return str.replace(/\\'/g, "'").replace(/\\"/g,'"') }
/**
 * Extract the domain of the given url.
 * @param {string} url The url
 * @returns {string} The domain extracted
 */
export function domain_part(url) {
	let [htt, , dom] = url.split('/')
	return `${htt}//${dom}`
}
/**
 * Extract the domain name of the given url.
 * @param {string} url The url
 * @returns {string} The domain extracted
 */
export function domain_name(url) {
	let [ , , dom] = url.split('/')
	let s_dom = dom.split('.')
	if (s_dom[0] === 'www') s_dom = s_dom.slice(1, s_dom.length)
	return title_case(s_dom.join('.'))
}
/**
 * Return a string formated in title case : 1st letter uppercase and others lowercase.
 * @function
 * @param {string} str string to tranform
 * @returns {string} string title formated
 */
export function title_case(str) {
	return str.replace(/\w\S*/g, a => `${a.charAt(0).toUpperCase()}${a.substr(1).toLowerCase()}`)
}
/**
 * Return a 'random' number of n digits
 * @function
 * @param {number} n The number of wanted digits in the returned integer
 * @returns {number} A 'random' integer of given n digits
 */
export function rnd(n) { return Math.ceil (Math.random () * Math.pow (10, n)) }
/**
 * Pick a 'random' number between a and b.
 * @function
 * @param {number} a Inferior limit
 * @param {number} b Superior limit
 * @returns {number} The random number
 */
export function pick_between(a, b) { return Math.floor(Math.random() * b) + a }
/**
 * Extract the given regex matching group(s) and return it (them)
 * @param {string} re The matching groups of the regular expression to apply, everything before
 * and after them will be matched outside replacement groups and discarded.
 * @param {string} str The string to search in
 * @param {string} repl The replacement tokens used to keep some matching groups (default : $1)
 * @returns {string} the string of the replaced tokens
 */
export function regextract (re, str, repl='$1', p='(?:.|\\n)*') {
	return str.replace(new RegExp(`${p}${re}${p}`, 'm'), repl)
}
/**
 * Format the string with tokens : $1 -> $i.
 * @example tr_fmt('$1-ok', ["ko"]) return 'ko-ok'
 * @param {string} a The string to format
 * @param {Array} tokens The tokens to use
 * @returns {string} The formated string
 */
export function str_fmt (str, tokens) {
	// console.log('str_fmt', str, tokens)
	for (let [k, v] of Object.entries(tokens)) {
		str = str.replace(new RegExp(`\\$${Number(k)+1}`, 'g'), String(v || ''))
		// a = a.replace(new RegExp(`\\$${Number(k)+1}`, 'g'), HTML_encode_UTF8(String(tokens[k])))
	}
	return str
}
/**
 * Generate string of the current date in a generic human readable format
 * @returns {string} Human readable current date representation
 */
export function ndt_human_readable() {
	return new Date().toISOString().replace('T', '_').replace(':', 'h').split(':')[0]
}
/**
 * Allow to wait for a certain delay, in milliseconds
 * From : https://www.delftstack.com/howto/javascript/javascript-wait-for-x-seconds/#use-promises-and-async%2fawait-to-wait-for-x-seconds-in-javascripthttps://www.revue.lu/search/
 * @param {Number} n the delay in ms
 */
export function delay(n) {	return new Promise((resolve) => setTimeout(resolve, n)) }
export function slugify(str) { return str.replace(/ /g, '_') }
