// SPDX-FileName: ./source_testing.js
// SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
import * as _     from './js_utils.js'
import * as sµ 		from './source_utils.js'
/**
 * @module source_testing
 */
/**
 * All the function for test (format, unit).
 * @namespace source_testing
 */

export function edit_style_class(elt, err_status) {
	if (!err_status) throw 'no err_status ' + window.location.pathname
	elt.classList.remove('default','success','warn','error','no_result_error', 'network_error')
	if(Array.isArray(err_status))
		for(let i of err_status)
			elt.classList.add(i)
	else
		elt.classList.add(err_status)
}
/**
 *
 */
export function add_result_test_to_query(src_def, elt, txt, query, val, err_status) {
	txt = txt.toString()
	let local_url = new URL(window.location)
	local_url.pathname = '/html/index.html'
	local_url.searchParams.delete('test_sources')
	local_url.searchParams.set('tech', 'cherry-pick_sources')
	local_url.searchParams.set('add_src_sel', elt.dataset.src_key)
	local_url.searchParams.set('q', query)
	local_url.searchParams.set('submit', '1')
	let div_q, link_q
	if(elt.querySelector(`[data-query='${query}']`) !== null) {
		div_q = elt.querySelector(`[data-query='${query}']`)
		link_q = div_q.getElementsByTagName('a')[0]
		link_q.textContent = `${val} ${query} [MP ↗] `
	} else {
		div_q = document.createElement('div')
		div_q.dataset.query = query  // 1st case of dataset usb
		elt.appendChild(div_q)
		link_q = document.createElement('a')
		link_q.textContent = query + '[MP ↗] '
		link_q.target = '_blank'
		link_q.href = local_url
		div_q.appendChild(link_q)
		link_q = document.createElement('a')
		link_q.textContent = '[web ↗]'
		link_q.target = '_blank'
		link_q.href = sµ.format_search_url(sµ.get_search_url(src_def), query, 10)
		div_q.appendChild(link_q)
	}
	edit_style_class(link_q, err_status)
	let p = document.createElement('p')
	p.textContent = txt
	div_q.appendChild(p)
}
/**
 *
 */
export function no_err(src_def, src_key, msg, dest, doc) {  // dest is always 'test_src'
	let local_url = new URL(window.location)
	let query = local_url.searchParams.get('q')
	let res_btn = doc.getElementById(`btn_${src_key}`)
	/*if(!res_btn.classList.contains('error') &&
		!res_btn.classList.contains('warn') &&
			!res_btn.classList.contains('no_result_error')
	) {
		edit_style_class(res_btn, 'success')
		res_btn.textContent = 'V'
	}*/
	edit_style_class(res_btn, 'success')
	res_btn.textContent = 'V'
	let res_link = doc.querySelector(`[id='report_${src_key}']>.${dest}>[data-query='${query}']`)
	if(!res_link.classList.contains('error') &&
		!res_link.classList.contains('warn') &&
		!res_link.classList.contains('no_result_error')
	) {
		let report_frame = doc.querySelector(`[id='report_${src_key}']>.${dest}`)
		add_result_test_to_query(src_def, report_frame, msg, query, 'V', 'success')
	}
}
/**
 *
 */
export function add_err(src_def, error_code, txt, src_key, err_status, dest, doc=document) {
	console[err_status](src_key, txt)
	let local_url = new URL(window.location)
	let query = local_url.searchParams.get('q')
	let val
	let elt_btn = doc.getElementById(`btn_${src_key}`)
	let report_frame = doc.querySelector(`[id='report_${src_key}']>.${dest}`)
	let res_card_btn = doc.querySelector(`[id='report_${src_key}'] .${dest}_btn`)
	if (!elt_btn)  // when called from a normal search
		return
	if(dest === 'test_src') {
		switch(error_code) {
		case sµ.error_code.NO_RESULT: val = '0' ;break
		case sµ.error_code.NETWORK_ERROR: val = 'N'
			err_status = 'network_error'
			edit_style_class(elt_btn, err_status) ;break
		default: val = 'X'
		}
		let report_peas = report_frame.querySelectorAll(`[data-query='${query}'] > p`)
		if (report_peas.length && report_peas[0].textContent == 'No error') {
			// why is there an empty "p" on top sometimes
			err_status = sµ.error_status.WARNING
			val = 'W'
		}
		if (_.is_str(txt) && txt.length > 0 && src_def.tags.warn_mask) {
			let is_masked = txt.replace(new RegExp(src_def.tags.warn_mask), '')
			if (is_masked.length == 0) {
				err_status = sµ.error_status.WARNING
				val = 'M'
			}
		}
		// below it 'write the result'
		edit_style_class(report_frame, `${err_status}_`)
		elt_btn.textContent = val
		elt_btn.classList.add(error_code)
		add_result_test_to_query(src_def, report_frame, txt, query, val, err_status)
	} else if(dest === 'test_integrity') {
		elt_btn.textContent = 'I'
		let p = document.createElement('p')
		p.dataset.error = error_code
		p.textContent = txt
		report_frame.appendChild(p)
		edit_style_class(p, err_status)
	} else throw `unknow add_err dest ${dest}`
	if(!elt_btn.classList.contains('error') && !elt_btn.classList.contains(err_status))
		edit_style_class(elt_btn, err_status)
	edit_style_class(res_card_btn, err_status)
	if (res_card_btn.getElementsByClassName('fld_ico')[0].classList.contains('fld_ico_hide'))
		res_card_btn.click()  // open it only once
}
/**
 *
 */
export function add_errors(all_error, doc) {
	for(let error of all_error)
		add_err(error.src_def, error.code, error.text, error.src_key, error.level, error.dest, doc)
}
