// SPDX-FileName: ./background.js
// SPDX-FileCopyrightText: 2017-2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals browser */

import * as _ from '/js/js_utils.js'
import * as xµ from '/js/webext_utils.js'
import { build_src } from '/js/source_utils.js'
import { init_table_sch_s, create_alarm } from '/js/scheduled_searchs.js'

/**
 * Opens the welcome.html for new user or for an update.
 * @param {string} stored_version The current version
 */
async function announce_updates (stored_version) {
	let manifest_version = browser.runtime.getManifest().version
	if (!_.is(stored_version)) {
		browser.tabs.create({'url': '/html/welcome.html'})
	} else {
		let do_announce_updates = false
		if (do_announce_updates)
			if (stored_version !== manifest_version)
				browser.tabs.create({'url': 'https://www.meta-press.es/category/journal.html'})
	}
	xµ.try_store('version', manifest_version)
}
/**
 * Executes in function call with a message.
 * @param {Object} data `function`'s name of the function and its `params` array of parameters
 * @param {Object} sender The information sender
 * @returns The result of the function or false
 */
async function handle_function_calls(data, sender) {
	let dbg = false
	if(dbg) console.log(`request : ${data.function} ${data.params}, sender ${sender.tab.id}`)
	if (_.is_str(data.function) && window[data.function]) {
		let res = window[data.function].apply(null, data.params)
		if (_.is(res) && _.is(res.then))
			res = await res
		return res
	}
	return false
}
browser.runtime.onMessage.addListener(handle_function_calls)
browser.browserAction.onClicked.addListener(() => browser.tabs.create({url:'/html/index.html'}))

/**
 * Cache.add: Request URL moz-extension://[…]/json/sources.json must be either http: or https:
 */
/*
caches.open('background').then(function(cache_handler) {
	cache_handler.add('/json/sources.json')
})*/
/**
 *
 */
async function cache_built_src() {
	let start_loading_sources = new Date()
	let custom_src = await xµ.get_stored('custom_src', {})
	let built_src = await build_src(custom_src)  // should cache ressources
	console.log(`json/sources.json loading time : ${new Date() - start_loading_sources} ms`)
	return built_src
}
window.bg_get_built_src = cache_built_src

let dt_need_reload_src = new Date()
function is_need_reload_src(dt_src_got) {
	return dt_need_reload_src > dt_src_got
}
window.bg_is_need_reload_src = is_need_reload_src
function update_need_reload_src() {
	dt_need_reload_src = new Date()
}
window.bg_update_need_reload_src = update_need_reload_src

/*
 *
 */
async function init() {
	// let built_srcs = await cache_built_src()
	// resolve_search_hosts(Object.keys(built_srcs), built_srcs)
	announce_updates(await xµ.get_stored('version', undefined))
	let stored_sch_sea = init_table_sch_s(await xµ.get_storage())
	if(stored_sch_sea) create_alarm(stored_sch_sea)
	document.body.classList.add('javascript_loaded')
}
init()
