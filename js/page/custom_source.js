// SPDX-FileName: ./custom_source.js
// SPDX-FileCopyrightText: 2018-2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals CodeMirror */
import * as mµ from '/js/mp_utils.js'
import * as µ from '/js/BOM_utils.js'
import * as g from '/js/gettext_html_auto.js/gettext_html_auto.js'

mµ.set_theme()
let custom_src_codemirror
const dbg = false
/**
 * Load the custom sources in the HTML document.
 * @memberof load_sources
 * @param {Object} custom_src The stored customs sources
 */
function load_custom_src(custom_src){
	dbg && console.log('custom_src', custom_src)
	if (typeof(custom_src) === 'object' && Object.keys(custom_src).length) {
		µ.id('custom_sources').textContent = JSON.stringify(custom_src, null, '\t')
	} else {
		µ.id('custom_sources').textContent = µ.id('default_custom_sources').textContent
	}
	custom_src_codemirror = CodeMirror.fromTextArea(
		µ.id('custom_sources'), {
			mode: 'application/json',
			lineNumbers: true,
			gutters: ['CodeMirror-lint-markers'],
			lint: true,
			screenReaderLabel: 'CodeMirror',
			indentWithTabs: true,
			extraKeys: {Tab: false} // avoids keyboard trap preserving 'tab-key' navigation
		}
	)
	const STATUS_CURRENT_LINE = µ.id('ln_nb')
	const STATUS_CURRENT_COL = µ.id('col_nb')
	custom_src_codemirror.on('cursorActivity', () => {
		const cursor = custom_src_codemirror.getCursor()
		STATUS_CURRENT_LINE.textContent = cursor.line + 1
		STATUS_CURRENT_COL.textContent = cursor.ch + 1
	})
}
µ.id('save_custom_sources').addEventListener('click', async (evt) => {
	evt.preventDefault()
	await custom_src_codemirror.save()
	const custom_src_value = µ.id('custom_sources').value
	dbg && console.log('custom_src_value', custom_src_value)
	let custom_obj
	try {
		custom_obj = JSON.parse(custom_src_value)
	} catch (exc) {
		alert(`Error parsing user custom source (${exc}).`)
		return
	}
	await mµ.to_storage('custom_src', custom_obj)
	mµ.update_need_reload_src()
})
µ.id('reset_custom_sources').addEventListener('click', async () => {
	await mµ.to_storage('custom_src', {})
	mµ.update_need_reload_src()
})
/**
 * Management of locally user-defined custom sources
 * @namespace new_update_sources
 */
async function init () {
	const userLang = await mµ.get_wanted_locale()
	const QUERYSTRING = new URL(window.location).searchParams
	if (QUERYSTRING.get('xgettext'))
		await g.xgettext_html()
	/*const mp_i18n = */await g.gettext_html_auto(userLang)
	let provided_sources_text = await fetch('/json/sources.json')
	provided_sources_text = await provided_sources_text.text()
	µ.id('provided_sources').textContent = provided_sources_text
	load_custom_src(await mµ.get_custom_src())
	document.body.classList.add('javascript_loaded')
}
init()
