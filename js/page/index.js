// SPDX-FileName: ./index.js
// SPDX-FileCopyrightText: 2017-2022 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals List, Choices, browser */
//
import * as mµ 		from '../mp_utils.js'

mµ.set_theme() // exceptional priority because it causes full screen background flash (FFF/000)

if (typeof(browser) !== 'undefined')
	not_just_frontend()  // exceptionnal priority also

import * as _ 		from '../js_utils.js'
import * as µ 		from '../BOM_utils.js'
import * as DOMµ 	from '../DOM_parsererror_utils.js'
import * as sµ 		from '../source_utils.js'
import * as st 		from '../source_testing.js'
import * as ss 		from '../scheduled_searchs.js'
import * as g 		from '../gettext_html_auto.js/gettext_html_auto.js'
import * as csv		from '../deps/csv.min.js'

function not_just_frontend() {
	mµ.get_child_mode().then((child_mode) => {
		if (!child_mode) {
			µ.id('cog_settings').classList.toggle('display_none')
			µ.$('.add_src').classList.toggle('display_none')
			µ.id('mp_autosearch_permalink').classList.toggle('display_none')
		}
	})
	µ.id('load_headlines').classList.toggle('display_none')
	µ.id('perm_line').classList.toggle('display_none')
}
const HEADLINE_TITLE_SIZE = 140
const META_FINDINGS_PAGE_SIZE = 15
const EXCERPT_SIZE = 350
const dynamic_css = µ.createStyleSheet()
const CSS_CHECKBOX_RULE_INDEX = dynamic_css.insertRule('.f_selection {display:none}', 0)
const IMG_PLACEHOLDER = '/img/photo_loading_placeholder.png'
const QUERYSTRING = new URL(window.location).searchParams
const TEST_SRC = QUERYSTRING.get('test_sources')
const INTER_FETCH_DELAY = 251  // ms ; 2001 -> no improvment
const SERIAL_QUERIES_DBG = false
// const DBG = true
let dt_source_objs = new Date()
let source_objs
let MAX_RES_BY_SRC
let	HEADLINE_PAGE_SIZE
let userLang
let LANG_NAME
let COUNTRY_NAME
let mp_i18n
let ALT_LOAD_TXT
let IS_LOAD_IMG
let manifest
let source_keys = []
let current_source_selection = []
let current_source_nb = 0
let rest_search_source = [] // Marin
let fetch_abort_controller
let resolved_search_hosts = {}
let mp_req = {
	findingList: new List('findings', {
		item: 'finding-item',
		valueNames: ['f_h1', 'f_txt', 'f_source', 'f_dt', 'f_by',
			{ name: 'f_source_title', attr: 'title' },
			// { name: 'mp_data_source', attr: 'mp-data-src'},
			{ name: 'f_by_title', attr: 'title' },
			{ name: 'f_ISO_dt', attr: 'datetime' },
			{ name: 'f_h1_title', attr: 'title' },
			{ name: 'f_dt_title', attr: 'title' },
			{ name: 'f_img_src', attr: 'src'},
			{ name: 'f_img_alt', attr: 'alt'},
			{ name: 'f_img_title', attr: 'title'},
			{ name: 'mp_data_img', attr: 'mp-data-img'},
			{ name: 'mp_data_alt', attr: 'mp-data-alt'},
			{ name: 'mp_data_title', attr: 'mp-data-title'},
			{ name: 'mp_data_key', attr: 'mp-data-key'},
			{ name: 'f_url', attr: 'href' },
			{ name: 'f_icon', attr: 'src' },
		],
		page: 10,
		pagination: {
			outerWindow: 2,
			innerWindow: 3,
			item: '<li><a class="page" href="#findings"></a></li>'
		},
		// fuzzySearch: { distance: 1501 }, // Unused. 1000 would search through 400 char.
	}),
	metaFindingList: new List('meta-findings', { // List of source related info for a query
		item: 'meta-findings-item',
		valueNames: [
			{ name: 'mf_icon', attr: 'src' },
			'mf_name',
			{ name: 'mf_title', attr: 'title' },
			{ name: 'mf_ext_link', attr: 'href' },
			// { name: 'mf_remove_source', attr: 'data-mf_remove_source' },
			{ name: 'mf_res_nb', attr: 'data-mf_res_nb' },
			{ name: 'mf_report', attr: 'data-mf_report_source'},
			'mf_locale_res_nb',
		],
		page: META_FINDINGS_PAGE_SIZE,
		pagination: {
			outerWindow: 10,
			innerWindow: 10,
			item: '<li><a class="page" href="#meta-findings"></a></li>'
		}
	}),
	running_query_countdown: 0,
	final_result_displayed: 0,
	query_result_timeout: null,
	query_final_result_timeout: null,
	query_start_date: null,
	query_abort_search_timeout : null
}
function update_nb_src() {
	current_source_nb = current_source_selection.length
	µ.id('cur_tag_sel_nb').textContent = current_source_nb
	mµ.get_nb_needed_host_perm(current_source_selection, source_objs).then((obj) => {
		let span = µ.id('cur_tag_perm_nb')
		span.textContent = obj.not_perm
		span.title = obj.names.toString()
	})
	let btn = µ.id('mp_submit')
	if (current_source_nb === 0) {
		btn.disabled = true
		btn.style.cursor = 'not-allowed'
		btn.title = mp_i18n.gettext('Select at least one source to search in.')
	} else if (btn.disabled) {
		btn.disabled = false
		btn.style.cursor = ''
	}
}
/**
 * Display the image stored in mp-data-img.
 * @param {click} e Click event
 */
function load_result_image (e) {
	let attrs = e.target.attributes
	let mp_data_src = attrs['mp-data-img'].textContent
	let mp_data_alt = attrs['mp-data-alt'] ? attrs['mp-data-alt'].textContent : ''
	let mp_data_title = attrs['mp-data-title'] ? attrs['mp-data-title'].textContent : ''
	for (let img of µ.$$(`img[mp-data-img='${mp_data_src}']`)) {
		img.src = mp_data_src
		img.alt = mp_data_alt
		img.title = mp_data_title
	}
}
function img_not_load(l_fmp) {
	l_fmp.f_img_src = IMG_PLACEHOLDER
	l_fmp.f_img_alt = ALT_LOAD_TXT
}
function img_lookup(l_fmp, f_img, f, n, i, f_nb) {
	if (n.type !== 'XML') {
		let is_r_img = _.is_str(n.r_img)
		if (is_r_img || _.is_str(n.r_img_xpath))
			if (is_r_img)
				try { f_img = f.querySelector(n.r_img) }
				catch (exc) { error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
					exc, sµ.error_status.ERROR, 'test_src', {'article': i})) }
			else // (_.is_str(n.r_img_xpath))
				try { f_img = µ.evaluateXPath(f, n.r_img_xpath) }
				catch (exc) { error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
					exc, sµ.error_status.ERROR, 'test_src', {'article': i})) }
	}
	try {
		if (_.is(f_img)) {
			if (f_img === '' && n.type === 'XML') { // extract image from f_txt
				let f_txt = l_fmp.f_txt
				let f_txt_dom = DOMµ.DOM_parser.parseFromString(
					µ.HTML_decode_entities(f_txt), 'text/html')
				// f_img = f_txt_dom.querySelector('img')
				f_img = f_txt_dom.getElementsByTagName('img')[0] // it's not faster with Firefox 90.a1
				// it's slower in Chromium Version 90.0.4430.85 (Build officiel) Arch Linux (64 bits)
				// a benchmark shows it 2x faster in Firefox 96
				l_fmp.f_txt = f_txt_dom.body.textContent // removes image from f_txt
			}
			if (f_img) {
				l_fmp.f_img_src = µ.urlify(f_img.attributes.src.value, n.domain_part)
				l_fmp.f_img_alt = DOMµ.strip_HTML_tags(f_img.alt) || ''
				l_fmp.f_img_title = DOMµ.strip_HTML_tags(f_img.title) || l_fmp.f_img_alt || ''
			}
		}
		if (_.is(n.r_img_src) || _.is(n.r_img_src_xpath)) {
			l_fmp.f_img_src = mµ.$_('r_img_src', f, n, f_nb)
			if (_.is(l_fmp.f_img_src))
				l_fmp.f_img_src = µ.urlify(l_fmp.f_img_src, n.domain_part)
		}
		if (_.is(n.r_img_alt))
			l_fmp.f_img_alt = mµ.$_('r_img_alt', f, n, f_nb)
		if (_.is(n.r_img_title))
			l_fmp.f_img_title = mµ.$_('r_img_title', f, n, f_nb)
		if (!l_fmp.f_img_title)
			l_fmp.f_img_title = l_fmp.f_img_alt
	} catch (exc) {
		error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
			exc, sµ.error_status.WARNING, 'test_src', {'article': i}))
	}
	l_fmp.mp_data_img = l_fmp.f_img_src
	l_fmp.mp_data_alt = l_fmp.f_img_alt
	l_fmp.mp_data_title = l_fmp.f_img_title
	if (!IS_LOAD_IMG && l_fmp.f_img_src)
		img_not_load(l_fmp)
	return l_fmp
}
µ.id('mp_query').value = new URL(window.location).searchParams.get('q')
µ.id('mp_query').addEventListener('keypress', function(evt) {
	if (!evt) evt = window.event
	let keyCode = evt.keyCode || evt.which
	if (keyCode === 13) {	// search on pressing Enter key
		µ.id('mp_submit').click()
		return false	// returning false will prevent the event from bubbling up.
	}
})
function toggle_sentence_search_tags (a, b) {
	const sel_mul_tech = tag_select_multiple.tech
	const len_before = sel_mul_tech.getValue(true)
	sel_mul_tech.removeActiveItemsByValue(a)
	const len_after = sel_mul_tech.getValue(true)
	if (len_before > len_after) {
		sel_mul_tech.setChoiceByValue(b)
		on_tag_sel()
	}
}
async function tick_query_duration() {
	let d = (new Date() - mp_req.query_start_date) / 1000
	µ.id('mp_running_duration').textContent = d > 10 ? Math.floor(d) : d
	mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
}
let error = []
function alert_perm() {
	alert(mp_i18n.gettext(µ.id('need_perm_txt').textContent))
	if (µ.id('tags_handle').textContent === '+')
		µ.id('tags_handle').click()
	µ.id('cur_tag_perm_nb').classList.add('draw_attention')
}
function search_source_timer (src_key, delay) {
	setTimeout(function () {search_source(src_key)}, delay)
}
async function search_source (i) {
	let n = source_objs[i]
	// DBG && console.log('search_source source definition', i, n)
	let src_query_start_date = new Date()
	let raw_rep
	try {
		raw_rep = await fetch(sµ.format_search_url(
			n.search_url, mp_req.search_terms, MAX_RES_BY_SRC), {
			signal: fetch_abort_controller.signal,
			method: n.method || 'GET',
			headers: { 'Content-Type': n.search_ctype || 'application/x-www-form-urlencoded'},
			body: n.body ? sµ.format_search_url(
				n.body, mp_req.search_terms, MAX_RES_BY_SRC, n.search_ctype) : null
		})
	} catch (exc) {
		error.push(sµ.create_error(n, sµ.error_code.NETWORK_ERROR, n.tags.key,
			exc, sµ.error_status.ERROR, 'test_src', {}, 1))
		update_search_query_countdown(i)
		return
	}
	try {
		if (!raw_rep.ok) {
			error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
				`fetch not ok : ${raw_rep.status}`, sµ.error_status.ERROR, 'test_src', {}, 1))
			update_search_query_countdown(i)
			return
		}
		let rep
		let c_type = raw_rep.headers.get('content-type')
		if (c_type.includes('application/json') || (n.type === 'JSON')) {
			try {
				if(n.jsonp_to_json_re) {
					let raw_txt = _.drop_escaped_quotes(_.triw(await raw_rep.text()))
					rep = JSON.parse(
						_.regextract(n.jsonp_to_json_re[0], raw_txt, n.jsonp_to_json_re[1])
					)
				} else {
					rep = await raw_rep.json()
				}
			} catch (exc) {
				error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
					exc, sµ.error_status.ERROR, 'test_src', {}, 'log'))
				update_search_query_countdown(i)
				return
			}
		} else {  // non JSON sources
			if (n.tags.charset) {
				raw_rep = await raw_rep.arrayBuffer()
				let text_decoder = new TextDecoder(n.tags.charset, {fatal: true})
				raw_rep = text_decoder.decode(raw_rep)
			} else {
				raw_rep = await raw_rep.text()
			}
			rep = mµ.DOM_parse_text_response(raw_rep, c_type)
			if (!rep) {
				error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
					`dom parser error ${rep}`, sµ.error_status.ERROR, 'test_src', {}, 'log'))
				update_search_query_countdown(i)
				return
			}
		}
		// if (!n.domain_part || n.extends) n.domain_part = _.domain_part(i)
		if (!n.favicon_url && !('JSON' === n.type))
			n.favicon_url = µ.get_favicon_URL(rep, n.domain_part)
		let fdgs = []
		if ('JSON' === n.type && !_.is(n.json_to_html)) {
			fdgs = _.is(n.results) ? mµ.$_('results', rep, n, 0, false) : rep
		} else {
			if (_.is(n.json_to_html)) {
				n = _.deep_copy(n)
				n.type = 'JSON'
				rep = mµ.$_('json_to_html', rep, n, 0, false)
				delete n.type
				rep = mµ.DOM_parse_text_response(rep, 'text/html')
			}
			if (_.is(n.results))
				try {
					fdgs = rep.querySelectorAll(n.results)
				} catch (exc) {
					error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
						exc, sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
				}
			else if (_.is(n.results_xpath))
				try {
					fdgs = µ.evaluateXPath(rep, n.results_xpath)
				} catch (exc) {
					error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
						exc, sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
				}
			else {
				fdgs = rep.querySelectorAll('item') // we need fdgs to auto-detect RSS variant
				if (!fdgs.length)
					fdgs = rep.querySelectorAll('entry')  // it's ATOM flavour fdgs
				if (!fdgs.length)
					error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
						'no source definition for "results"',
						sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
			}
		}
		let fdgs_nb = fdgs ? fdgs.length : 0
		// DBG && console.log('fdgs', fdgs_nb, fdgs)
		let max_fdgs = MAX_RES_BY_SRC  //!\ fail things when undef
		// DBG && console.log('MAX_REs_BY_SRC', MAX_RES_BY_SRC)
		fdgs_nb = Math.min(fdgs_nb, max_fdgs)
		if (0 === fdgs_nb || isNaN(fdgs_nb)) {
			update_search_query_countdown(i)
			error.push(sµ.create_error(n, sµ.error_code.NO_RESULT, n.tags.key,
				'No result', sµ.error_status.ERROR, 'test_src', {}, 'log'))
			if (TEST_SRC)  // keep empty sources in the metaFindings to allow reports
				return
		}
		let res_nb_str = ''
		if (n.type !== 'XML') {
			try { res_nb_str = mµ.$_('res_nb', rep, n)}
			catch (exc) {
				error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
					exc, sµ.error_status.ERROR, 'test_src', {}, 'log'))
			}
		} else {
			n = sµ.set_RSS_variant(fdgs, n)
		}
		let res_nb = Number(_.do_int(res_nb_str))
		if (isNaN(res_nb) || 0 === res_nb) {  // some sources don't have res_nb at all
			res_nb = fdgs_nb
		}
		let f_h1='', f_url='', f_by='', f_dt=new Date(0), f_nb=0, f_img=''
		let dup_elt, dup_val
		for (let f of fdgs) {
			if (max_fdgs-- === 0) break
			f_nb = MAX_RES_BY_SRC - max_fdgs
			f_h1 = ''; f_url=''; f_by=''; f_img=''; f_dt=new Date(0)
			let l_fmp ={'f_img_src':'','f_img_alt':'','f_img_title':'','mp_data_img':'',
				'mp_data_alt':'','mp_data_title':'','f_txt':''}
			try {
				f_h1 = mµ.$_('r_h1', f, n, f_nb)
				if (!f_h1) throw 'missing this f_h1'
				f_dt = mµ.$_('r_dt', f, n, f_nb, false)
				if (!f_dt) throw `No f_dt after lookup (mµ.$_) f_h1 ${f_h1} %%%`
				f_dt = mµ.parse_dt_str(f_dt, n.tags.tz, f_nb, n)
				if(isNaN(f_dt)) throw 'No f_dt after parse_dt_str'
				f_url = µ.urlify(mµ.$_('r_url', f, n, f_nb), n.domain_part)
				if (!f_url) throw 'missing this f_url'
			} catch (exc) {
				error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
					f_nb + ' ' + exc, sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
				res_nb -= 1
				continue
			}
			try {
				l_fmp.f_txt = mµ.$_('r_txt', f, n, f_nb)
				l_fmp = img_lookup(l_fmp, f_img, f, n, i, f_nb)
				f_by = mµ.$_('r_by', f, n, f_nb) || n.tags.name
				let f_text = TEST_SRC ? _.shorten(l_fmp.f_txt, EXCERPT_SIZE) :
					_.bolden(l_fmp.f_txt, mp_req.search_terms)
				if (await mµ.get_undup_results() &&	(dup_elt = get_dup(f_h1)).length) {
					dup_elt = dup_elt[0]
					dup_val = dup_elt.values()
					dup_val.f_source += dup_val.f_source.slice(-1) === '…' ? '' : '…'
					dup_val.f_source_title += `, ${n.tags.name}`
					dup_elt.values(dup_val)
				} else {
					mp_req.findingList.add({f_h1:f_h1, f_url:f_url, f_by:f_by,
						f_txt: f_text,
						f_h1_title:	f_h1,
						f_by_title:	f_by,
						f_dt: f_dt.toISOString().slice(0, -14),
						f_epoc:	f_dt.valueOf(),
						f_ISO_dt: f_dt.toISOString(),
						f_dt_title: f_dt.toLocaleString(userLang),
						f_icon:	n.favicon_url,
						f_img_src: l_fmp.f_img_src,
						f_img_alt: l_fmp.f_img_alt,
						f_img_title: l_fmp.f_img_title,
						mp_data_img: l_fmp.mp_data_img,
						mp_data_alt: l_fmp.mp_data_alt,
						mp_data_title: l_fmp.mp_data_title,
						f_source: n.tags.name,
						f_source_title: n.tags.name,
						mp_data_key: i
					})
				}
			} catch (exc) {
				error.push(sµ.create_error(n, sµ.error_code.ERROR, n.tags.key,
					f_nb + ' ' + exc, sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
			}
		}
		if (TEST_SRC)
			st.no_err(n, n.tags.key, 'No error', 'test_src', opener.document)
		else
			mp_req.metaFindingList.add({
				mf_icon: n.favicon_url,
				mf_name: n.tags.name,
				mf_title: mf_title(n.tags.name),
				mf_ext_link: sµ.format_search_url(n.search_url_web || n.search_url,
					mp_req.search_terms, MAX_RES_BY_SRC),
				// mf_remove_source: n.tags.name,
				mf_res_nb: res_nb,
				mf_locale_res_nb: res_nb.toLocaleString(userLang),
			})
		update_search_query_countdown(i)
		console.log(i, new Date() - src_query_start_date, 'ms',
			mp_req.running_query_countdown + 1, '/', current_source_nb, ':', res_nb, 'res')
	} catch (exc) {
		let err = exc.name == 'AbortError' ? sµ.error_code.NETWORK_ERROR : sµ.error_code.ERROR
		error.push(sµ.create_error(n, err, n.tags.key, exc,sµ.error_status.ERROR, 'test_src',
			{'article': i}, 'log'))
		update_search_query_countdown(i)
	}
}
µ.id('mp_submit').addEventListener('click', async () => {
	if (current_source_nb === 0) return
	if (µ.id('mp_query').value === '') return
	µ.id('mp_submit').disabled = true
	µ.id('mp_submit').style.cursor = 'not-allowed'
	µ.id('search_stats').style.display='none'
	µ.id('donation_reminder').style.display='none'
	µ.id('favicon').href = '/img/favicon-metapress-v2.png'
	mp_req.metaFindingList.search()
	if (typeof(browser) === 'undefined') {
		mp_req.query_start_date = ndt()
		const token = `${ndt().toISOString()}_${_.uuidv4()}`.replace(/:/g, '-')
		const params = gen_permalink().searchParams
		µ.id('waiting_gif').classList.remove('display_none')
		document.body.classList.add('waiting')
		const rep = await mµ.try_fetch(
			`../scripts/py/backend.cgi?backend=1&token=${token}&${params}`)
		µ.id('waiting_gif').classList.add('display_none')
		document.body.classList.remove('waiting')
		µ.id('mp_submit').disabled = false
		µ.id('mp_submit').style.cursor = 'normal'
		if (rep.ok)
			sub_import_JSON(await rep.json())
		return
	}
	µ.id('mp_stop').style.display = 'inline'
	µ.id('list_src').style.display = 'none'
	let elt_sto = µ.id('mp_stop')
	if (elt_sto.disabled) {
		elt_sto.disabled = false
		elt_sto.textContent = 'Cancel'
		elt_sto.style.opacity = 1
	}
	mp_req.search_terms = µ.id('mp_query').value
	if (mp_req.running_query_countdown !== 0 || '' === mp_req.search_terms)
		return // to prevent 2nd search during a running one
	mp_req.running_query_countdown = 1 // to prevent 2nd search during permission request
	try {
		if (!await mµ.request_sources_perm(current_source_selection, source_objs)) {
			alert_perm()
			mp_req.running_query_countdown = 0
			return
		}
		let val = await mµ.get_nb_needed_host_perm(current_source_selection, source_objs)
		µ.id('cur_tag_perm_nb').textContent = val.not_perm
	} catch (exc) {
		console.warn('Ignored exception : ', exc)
	}
	mp_req.metaFindingList.search()
	document.body.classList.add('waiting')
	mp_req.running_query_countdown = current_source_nb
	if (!TEST_SRC){
		mp_req.query_start_date = new Date()
		mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
		clear_results()
	}
	rest_search_source = [...current_source_selection]
	fetch_abort_controller = new AbortController()
	fetch_abort_controller.signal.onabort = () => {
		if (TEST_SRC) return
		µ.id('mp_running_stats').style.display = 'none'
		µ.id('mp_cancelling').style.display = 'block'
		elt_sto.disabled = true
		elt_sto.textContent = mp_i18n.gettext('Cancelling…')
		elt_sto.style.opacity = 0.75
	}
	let time = parseInt(await mµ.get_search_timeout(), 10)
	if (time > 0) {
		if (TEST_SRC)
			time = 3 * time
		clearTimeout(mp_req.query_abort_search_timeout)
		mp_req.query_abort_search_timeout = setTimeout(fetch_abort_controller.abort, 1000 * time)
	}
	let next_req_delay
	// let c = 0
	_.shuffle_array(current_source_selection)
	let domain_ip = null
	let last_fetch_date = null
	let now_dt = null
	let last_seen_IPs = {}
	let src_host = ''
	let first_ip
	// for (let src_key of current_source_selection.slice(0, 499)) {
	for (let src_key of current_source_selection) {
		SERIAL_QUERIES_DBG && console.log('will search_source', src_key)
		src_host = new URL(src_key).host
		domain_ip = resolved_search_hosts[src_host]
		if (!domain_ip) {
			let dns_record
			dns_record = await mµ.dns_resolve(src_host)
			if (dns_record.addresses) {
				first_ip = dns_record.addresses[0]
				resolved_search_hosts[src_host] = first_ip
				domain_ip = first_ip
			}
		}
		// console.log(src_key, domain_ip)
		if (domain_ip) {
			now_dt = new Date().getTime()
			last_fetch_date = last_seen_IPs[domain_ip]
			let should_wait = last_fetch_date && last_fetch_date > (now_dt - INTER_FETCH_DELAY)
			let next_fetch_date = last_fetch_date + INTER_FETCH_DELAY
			next_req_delay = should_wait ? next_fetch_date - now_dt : 0
			should_wait && console.log(src_key, 'will wait', next_req_delay, 'ms')
			last_seen_IPs[domain_ip] = next_fetch_date || now_dt
		} else {
			next_req_delay += INTER_FETCH_DELAY / 10
		}
		if (SERIAL_QUERIES_DBG) {
			await search_source(src_key)
		} else
			search_source_timer(src_key, next_req_delay)
	}
})
function get_dup(f_h1) {
	return mp_req.findingList.get('f_h1', f_h1)
}
function update_search_query_countdown(current_s) { // Marin
	let tooltip_text = µ.id('mp_tooltip_text')
	let btn_src = µ.id('mp_running_btn_src')
	let btn_src_text = µ.id('mp_running_btn_src_text')
	rest_search_source = rest_search_source.filter(s => s !== current_s)
	clearTimeout(mp_req.query_result_timeout)
	search_query_countdown()
	if (!TEST_SRC)
		if(mp_req.running_query_countdown === 0) {
			clearTimeout(mp_req.query_abort_search_timeout)
			btn_src.style.display = 'none'
			btn_src.textContent = '+'
			tooltip_text.style.display = 'none'
			btn_src_text.style.display = 'none'
			tooltip_text.innerHTML = ''
			btn_src_text.innerHTML = ''
			display_ongoing_results()
		} else if (mp_req.running_query_countdown < 30) {
			let t = `<h4>${mp_i18n.gettext('Waiting for…')}</h4>`
			btn_src.style.display = 'inline'
			tooltip_text.style.display = 'none'
			for (let s of rest_search_source) {
				let n = source_objs[s]
				t += '<p class="tooltip_text_family">'
				+`<img class="f_icon" src=${n.favicon || '.'} width=24>`
				+`&nbsp;<b>${n.tags.name}</b> : ${s}<p>`
			}
			if (btn_src_text.style.display !== 'none')
				btn_src_text.innerHTML = t
			tooltip_text.innerHTML = t
		}
}
function search_query_countdown() {
	mp_req.running_query_countdown = rest_search_source.length
	clearTimeout(mp_req.query_result_timeout)
	mp_req.query_result_timeout = setTimeout(() => {
		display_ongoing_results()
	}, 2000)
}
function filter_one_src(mf_name_elt) {
	if (mf_name_elt === null) return
	mp_req.findingList.search(mf_name_elt.textContent, ['f_source_title'], listjs_custom_search)
	bold_clicked_a(mf_name_elt, 'mf_name')
	mf_date_slicing()
	filter_last(µ.id('mf_all_res'), null)
}
function listjs_custom_search(str, cols) {  // seems ListJS applies str = str.toLowerCase()
	// let str_re = new RegExp(str, 'i')  // it's escaping itself !
	const val_name = cols[0]
	str = str.replace(/\\/g, '')
	for (let i of mp_req.findingList.items)
		// i.found = str_re.test(i.values()[val_name])
		i.found = i.values()[val_name].toLowerCase().includes(str)
}
function display_ongoing_results() {
	if (mp_req.final_result_displayed) return
	µ.id('mp_query_countdown').textContent = mp_req.running_query_countdown
	µ.id('mp_running_src_fetched').textContent = current_source_nb
	if(_.is(fetch_abort_controller) && !fetch_abort_controller.signal.aborted)
		if (!TEST_SRC) {
			µ.id('mp_running_stats').style.display = 'block'
			µ.$('title').textContent = µ.id('ongoing_tab_title').textContent
		}
	if (mp_req.findingList.size() > 0) {
		mp_req.findingList.sort('f_epoc', {order: 'desc'})
		mp_req.metaFindingList.show(0, mp_req.metaFindingList.size())
		for (let a of document.getElementsByClassName('mf_name')) {
			a.onclick = (evt) => filter_one_src(evt.target)
			a.parentNode.onclick = (evt) => filter_one_src(evt.target.querySelector('.mf_name'))
		}
		for (let a of document.getElementsByClassName('mf_remove_source')) {
			a.onclick = evt => {
				document.body.classList.add('waiting')
				setTimeout(function () {
					const mf_name_elt = evt.target.parentNode.querySelector('.mf_name')
					mf_name_elt.onclick({target: mf_name_elt}) // filter only the elements to delete
					const cur_page_size = mp_req.findingList.page
					mp_req.findingList.show(0, mp_req.findingList.size()) // display all the elts to del
					for (let i of mp_req.findingList.visibleItems)
						mp_req.findingList.remove('f_url', i.values().f_url)
					mp_req.metaFindingList.remove('mf_name', mf_name_elt.textContent)
					µ.id('mp_query_meta_total').textContent =
						get_mf_res_nb().toLocaleString(userLang)
					mp_req.findingList.show(0, cur_page_size)
					µ.id('mf_total').click()
					document.body.classList.remove('waiting')
				}, 0)
			}
		}
		for (let a of document.getElementsByClassName('mf_report')) {
			a.onclick = evt => {
				let pop = evt.target.nextElementSibling
				pop.style.display = 'block'
				const name = evt.target.parentNode.firstElementChild.textContent.trim()
				a.disabled = true
				pop.querySelector('.mf_name').textContent = name
				pop.querySelector('.cancel_report').onclick = () => {
					pop.style.display = 'none'
					a.disabled = false
				}
				pop.querySelector('.send_report').onclick = async () => {
					pop.style.display = 'none'
					let comment = pop.querySelector('.input_report').value || 'none'
					let pb = pop.querySelector('.select_prob').value
					let url = new URL(`https://www.meta-press.es/fbk/${manifest.version}/${pb}/${name}`)
					await mµ.request_one_host_perm('https://www.meta-press.es') // keep www. cause redir
					console.info('The 2 next XHR are 404 on purpose, I\'ll get the feedback from logs')
					mµ.try_fetch(url)
					let setting = ''
					if (pop.querySelector('.permit_send').checked)
						for (const i of gen_permalink(true).searchParams) setting += `&${i[0]}=${i[1]}`
					mµ.try_fetch(`${url}?date=${new Date().toISOString()}&txt=${comment}${setting}`)
				}
			}
		}
		mp_req.metaFindingList.show(0, META_FINDINGS_PAGE_SIZE)
		mp_req.metaFindingList.sort('mf_res_nb', {order: 'desc'})
		µ.id('mf_total').onclick = evt => {
			mp_req.findingList.search()
			bold_clicked_a(evt.target, 'mf_name')
			mf_date_slicing()
			filter_last(µ.id('mf_all_res'), null)
		}
		mf_date_slicing()
		for (let sbm of document.getElementsByClassName('sidebar-module'))
			sbm.style.display='block'
		mp_req.findingList.update()
	}
	if ((! mp_req.final_result_displayed) && (mp_req.running_query_countdown === 0))
		display_final_results()
}
let mf_search_cb_timeout
function mf_search_cb (evt) {
	clearTimeout(mf_search_cb_timeout)
	mf_search_cb_timeout = setTimeout(() => {
		mp_req.metaFindingList.search(_.triw(evt.target.value), ['mf_name'])
	}, evt.keyCode === 13 ? 0 : 500)
}
function get_mf_res_nb () {
	let a = 0
	for (let r of mp_req.metaFindingList.items) a += r.values().mf_res_nb
	return a
}
function result_attach_event () {
	for (let a of µ.$$(`[src="${IMG_PLACEHOLDER}"]`)) {
		a.removeEventListener('click', load_result_image)
		a.addEventListener('click', load_result_image, {once: true})
	}
	for (let a of document.getElementsByClassName('f_txt'))
		if (µ.isOverflown(a)) a.style.resize = 'vertical'
}
async function display_final_results() {
	if (TEST_SRC) {
		const pool_node = opener.document.getElementById('mp_display_result_test')
		const pool = parseInt(pool_node.dataset.pool) + 1
		pool_node.dataset.pool = pool
		st.add_errors(error, opener.document)
		opener.document.getElementById('update_stats').click()
		self.close()
	} else {  // so: if (!TEST_SRC)
		clearTimeout(mp_req.repeat_timeout_ongoing)
		clearTimeout(mp_req.query_result_timeout)
		mp_req.final_result_displayed = 1
		location.href = '#'  // ensure href is not already #before_search_stats, else it wont move
		location.href = '#before_search_stats'
		µ.id('mp_running_stats').style.display = 'none'
		µ.id('mp_cancelling').style.display = 'none'
		µ.id('search_stats').style.display = 'block'
		µ.id('date_range').style.display = 'flex'
		let l = mp_req.findingList.length
		let i = mp_req.findingList.items
		if (l > 0) {
			µ.id('date_filter_sta').min = i[l - 1]._values.f_dt
			µ.id('date_filter_sto').max = l > 1 ? i[1]._values.f_dt : i[0]
		}
		if (_.rnd(1) > 7)
			µ.id('donation_reminder').style.display = 'block'
		clearTimeout(mp_req.duration_ticker)
		if (0 === mp_req.findingList.size())
			µ.id('no_results').style.display = 'block'
		else
			µ.id('mp_page_sizes').style.display = 'block'
		if(!mp_req.findingList.handlers.updated.includes(result_attach_event)) {
			mp_req.findingList.on('updated', result_attach_event)
			mp_req.findingList.update()
		}
		µ.id('mp_query_meta_total').textContent =
			get_mf_res_nb().toLocaleString(userLang)
		µ.id('mp_query_meta_local_total').textContent =
			mp_req.findingList.size()
		let mp_end_date = ndt()
		let req_duration = (mp_end_date - mp_req.query_start_date) / 1000
		µ.id('mp_duration').textContent = req_duration
		µ.id('mp_duration').title = mp_end_date.toLocaleString(userLang)
		µ.id('mp_query_search_terms').textContent = mp_req.search_terms
		µ.id('mp_src_nb').textContent =
			mp_req.metaFindingList.items.filter(i => i.values().mf_res_nb > 0).length
		µ.id('mp_src_fetched').textContent = current_source_nb
		µ.id('mp_submit').disabled = false
		µ.id('mp_submit').style.cursor = 'pointer'
		µ.id('mp_stop').style.display = 'none'
		document.getElementsByTagName('title')[0].textContent = µ.id('tab_title').textContent
		document.body.classList.remove('waiting')
		let id_sch_s = QUERYSTRING.get('id_sch_s')
		if (req_duration > 8 && !id_sch_s)
			mµ.notify_user(µ.id('tab_title').textContent, _.trim(
				µ.id('search_stats').textContent).replace(' 🔗', ''))
		if (id_sch_s)
			add_elt_url(id_sch_s)
		if (await mµ.get_keep_host_perm() === '0')
			mµ.drop_host_perm()
		if (mp_req.metaFindingList.size() > 30) {  // search source in meta finding
			µ.id('mf_meta_search').style.display = 'flex'
			µ.id('mp_meta_search').addEventListener('keyup', mf_search_cb)
			µ.id('mp_clear_search').onclick = () => {
				µ.id('mp_meta_search').value = ''
				mp_req.metaFindingList.search()
			}
		}
		µ.id('mp_search_permalink').href = µ.rm_href_anchor(gen_permalink(1))
		if (QUERYSTRING.get('backend')) {
			await export_JSON(QUERYSTRING.get('token'))
		}
	}
}
function clear_results() {
	mp_req.final_result_displayed = 0
	µ.id('no_results').style.display = 'none'
	µ.id('mp_page_sizes').style.display = 'none'
	µ.id('mp_query_meta_total').textContent = '…'
	µ.id('mp_query_meta_local_total').textContent = '…'
	µ.id('mp_running_stats').style.display = 'none'
	µ.id('mp_query_countdown').textContent = '…'
	µ.id('mp_running_src_fetched').textContent = '…'
	µ.id('mp_duration').textContent = '…'
	µ.id('mp_query_search_terms').textContent = '…'
	µ.id('mp_src_nb').textContent = '…'
	µ.id('mp_src_fetched').textContent = '…'
	µ.id('mp_duration').textContent = '…'
	µ.id('mf_meta_search').style.display='none'
	µ.id('clear_date').click()
	µ.id('mp_clear_filter').click()
	if ('block' === µ.id('mp_sel_all').style.display)
		µ.id('mp_sel_mod').click()
	for (const sbm of document.getElementsByClassName('sidebar-module'))
		sbm.style.display='none'
	mp_req.findingList.clear()
	mp_req.metaFindingList.clear()
}
µ.id('mp_autosearch_permalink').addEventListener('click', () => {
	let sch_sea = gen_permalink(false)
	sch_sea = sch_sea.toString() // Chrome doesn't support URL in browser.storage elt
	mµ.to_storage('new_sch_sea', sch_sea)
})
µ.id('mp_stop').addEventListener('click', () => {
	fetch_abort_controller.abort()
})
/**
 * Generate an URL who caontain all the sear settings.
 * @param {boolean} is_submit if true add autosubmit
 */
function gen_permalink(is_submit=false) {
	let permalink = new URL(window.location)
	permalink.searchParams.set('q', µ.id('mp_query').value)
	for (let t of Object.keys(tag_select_multiple)) {
		permalink.searchParams.delete(t)
		for (let v of tag_select_multiple[t].getValue(true))
			permalink.searchParams.append(t, v)
	}
	if (del_src_sel.length > 0) {
		permalink.searchParams.delete('rm_src_sel')
		for (const i of del_src_sel)
			permalink.searchParams.append('rm_src_sel', i)
	}
	if (add_src_sel.length > 0) {
		permalink.searchParams.delete('add_src_sel')
		for (const i of add_src_sel)
			permalink.searchParams.append('add_src_sel', i)
	}
	if (is_submit)
		permalink.searchParams.set('submit', 1)
	else
		permalink.searchParams.delete('submit')
	return permalink
}
for (let pg_size of [5, 10, 20, 50])
	µ.id(`mp_headlines_page_${pg_size}`).addEventListener('click', () => {
		headlineList.show(0, pg_size)
	})
µ.id('mp_headlines_page_all').addEventListener('click', () => {
	let pg_size = headlineList.size()
	headlineList.show(0, pg_size)
})
for (let p of [10, 20, 50, 100])
	µ.id(`list_src_page_${p}`).addEventListener('click', () => display_panel.show(0, p))
µ.id('list_src_page_all').addEventListener('click', () => {
	display_panel.show(0, display_panel.size())})
for (let p of [10, 20, 50])
	µ.id(`mp_page_${p}`).addEventListener('click', () => mp_req.findingList.show(0, p))
µ.id('mp_page_all').addEventListener('click', () => {
	mp_req.findingList.show(0, mp_req.findingList.size())})
µ.id('mp_import_RSS').addEventListener( 'click', e => import_XML(e, 'RSS'))
// µ.id('mp_import_ATOM').addEventListener('click', e => import_XML(e,'ATOM'))
µ.id('mp_import_JSON').addEventListener('click', import_JSON)
µ.id('mp_import_CSV').addEventListener( 'click', import_CSV)
function import_CSV() {
	mp_req.query_start_date = ndt()
	µ.read_user_file(function (evt) {
		document.body.classList.add('waiting')
		setTimeout(function () {
			try {
				let input_csv = evt.target.result
				input_csv = csv.parse(input_csv)
				clear_results()
				let mf = {}
				input_csv.splice(0, 1)
				for (let i of input_csv) {
					let r_dt = new Date(i[0])
					let f_source = i[1]
					let f_source_key = i[2]
					let img = {
						f_img_src: i[8],
						f_img_alt: i[9],
						f_img_title: i[10],
						mp_data_img: i[8],
						mp_data_alt: i[9],
						mp_data_title: i[10],
					}
					if (!IS_LOAD_IMG)
						img_not_load(img)
					mp_req.findingList.add({
						f_h1: i[5],
						f_h1_title: i[5],
						f_url: i[4],
						f_icon: i[3],
						f_img_src: img.f_img_src,
						f_img_alt: img.f_img_alt,
						f_img_title: img.f_img_title,
						mp_data_img: img.mp_data_img,
						mp_data_alt: img.mp_data_alt,
						mp_data_title: img.mp_data_title,
						mp_data_key: i[2],
						f_txt: i[6],
						f_by:  i[7] || i[1],
						f_by_title: i[7] || i[1],
						f_dt: r_dt.toISOString().slice(0, -14),
						f_dt_title: r_dt.toLocaleString(userLang),
						f_epoc: r_dt.valueOf(),
						f_ISO_dt: r_dt.toISOString(),
						f_source: f_source,
						f_source_title: f_source
					})
					let n = source_objs[f_source_key]
					let url = !_.is(n) ? f_source_key : sµ.format_search_url(
						n.search_url_web || n.search_url, mp_req.search_terms, MAX_RES_BY_SRC)
					if (!_.is(mf[f_source])) {  // initialization
						mf[f_source] = {
							mf_icon: i[3],
							mf_name: f_source,
							mf_title: mf_title(f_source),
							mf_ext_link: url,
							// mf_remove_source: f_source,
							mf_res_nb: 0
						}
					}
					mf[f_source].mf_res_nb += 1
					mf[f_source].mf_locale_res_nb = mf[f_source].mf_res_nb.toLocaleString(userLang)
				}
				for (let v of Object.values(mf)) {
					mp_req.metaFindingList.add(v)
				}
				display_ongoing_results()
			} catch (exec) {
				console.error('CSV loading failure', exec)
			}
			document.body.classList.remove('waiting')
		}, 0)
	})
}
function import_JSON() {
	mp_req.query_start_date = ndt()
	µ.read_user_file((evt) => sub_import_JSON(JSON.parse(evt.target.result)))
}
function sub_import_JSON (input_json) {
	document.body.classList.add('waiting')
	setTimeout(function () {
		try {
			tags_from_JSON(input_json.filters)
			clear_results()
			for (let f of input_json.findings) {
				if (IS_LOAD_IMG) {
					f.f_img_src = f.mp_data_img
					f.f_img_alt = f.mp_data_alt
					f.f_img_title = f.mp_data_title
				} else if (f.mp_data_img)
					img_not_load(f)
			}
			mp_req.findingList.add(input_json.findings)
			mp_req.metaFindingList.add(input_json.meta_findings)
			mp_req.search_terms = input_json.search_terms
			µ.id('mp_query').value = input_json.search_terms
			display_ongoing_results()
		} catch (exc) {
			console.error('JSON loading failure', exc)
		} finally {
			document.body.classList.remove('waiting')
		}
	}, 0)
}
function import_XML(evt, flux_type) {
	if (µ.id('tags_row').style.display === 'block')
		µ.id('tags_handle').click()
	mp_req.query_start_date = ndt()
	µ.read_user_file(function (evt) {
		document.body.classList.add('waiting')
		setTimeout(function () {
			try {
				let input_xml = mµ.DOM_parse_text_response(evt.target.result, 'application/xml')
				clear_results()
				let r_src, n, r_dt, r_by, f_source, f_icon, item_obj, f_h1, f_url, l_fmp
				let mf = {}
				let p = sµ.XML_SRC[flux_type]  // parser info
				let xml_results = input_xml.querySelectorAll(p.results)
				let detected_flux_type = sµ.find_RSS_variant(xml_results, {tags:{name:'import_XML'}})
				// console.log('detected_flux_type', detected_flux_type)
				p = sµ.XML_SRC[detected_flux_type]
				tags_from_XML(input_xml.querySelectorAll(p.filters), flux_type)
				mp_req.search_terms = input_xml.getElementsByTagName('title')[0].textContent.replace(
					'Meta-Press.es : ', '')
				µ.id('mp_query').value = mp_req.search_terms
				let is_mp_RSS = false
				let generator = input_xml.getElementsByTagName('generator')[0]
				if (_.is(generator)) {
					generator = generator.textContent
					if (_.is_str(generator))
						is_mp_RSS = generator.startsWith('Meta-Press.es')
				}
				for (let item of input_xml.querySelectorAll(p.results)) {
					l_fmp = {
						'f_img_src': '', 'f_img_alt': '', 'f_img_title': '', 'mp_data_img': '',
						'mp_data_alt': '', 'mp_data_title': '', 'f_txt': '', 'mp_data_key': ''
					}
					p.tags = {key: ''}
					if (is_mp_RSS) {	// it's our format
						if ('RSS' === flux_type)
							l_fmp.mp_data_key = µ.evaluateXPath(item, './dc:identifier').textContent
						else
							l_fmp.mp_data_key = item.querySelector('contributor > uri').textContent
						n = source_objs[l_fmp.mp_data_key]
						if (n)
							p.tags.key = n.tags.key
					}
					f_url = mµ.$_('r_url', item, p)
					r_src = _.domain_part(f_url)
					if (!p.tags.key)
						n = source_objs[r_src]
					l_fmp.f_txt = mµ.$_('r_txt', item, p)
					if (_.is(p.r_img_src) || _.is(p.r_img_src_xpath)) {
						l_fmp.f_img_src = mµ.$_('r_img_src', item, p)
						l_fmp.mp_data_img = l_fmp.f_img_src
					}
					if (_.is(p.r_img_alt) || _.is(p.r_img_alt_xpath)) {
						l_fmp.f_img_alt = mµ.$_('r_img_alt', item, p)
						l_fmp.mp_data_alt = l_fmp.f_img_alt
					}
					if (_.is(p.r_img_title) || _.is(p.r_img_title_xpath)) {
						l_fmp.f_img_title = mµ.$_('r_img_title', item, p)
						l_fmp.mp_data_title = l_fmp.f_img_title
					}
					f_h1 = mµ.$_('r_h1', item, p)
					r_dt = new Date(mµ.$_('r_dt', item, p))
					r_by = mµ.$_('r_by', item, p)
					f_icon = n && n.favicon_url || '/img/Feed-icon.svg'
					f_source = n && n.tags.name || _.domain_name(f_url) || ''
					if (!IS_LOAD_IMG)
						img_not_load(l_fmp)
					item_obj = {
						f_h1: f_h1,
						f_h1_title: f_h1,
						f_url: f_url,
						f_icon: f_icon,
						f_img_src: l_fmp.f_img_src,
						f_img_alt: l_fmp.f_img_alt,
						f_img_title: l_fmp.f_img_title,
						mp_data_img: l_fmp.mp_data_img,
						mp_data_alt: l_fmp.mp_data_alt,
						mp_data_title: l_fmp.mp_data_title,
						mp_data_key: l_fmp.mp_data_key,
						f_txt: l_fmp.f_txt,
						f_by: r_by || f_source,
						f_by_title: r_by,
						f_dt: r_dt.toISOString().slice(0, -14),
						f_dt_title: r_dt.toLocaleString(userLang),
						f_epoc: r_dt.valueOf(),
						f_ISO_dt: r_dt.toISOString(),
						f_source: f_source,
						f_source_title: f_source
					}
					mp_req.findingList.add(item_obj)
					if (!_.is(mf[f_source])) {  // initialization
						mf[f_source] = {
							mf_icon: f_icon,
							mf_name: f_source,
							mf_title: mf_title(f_source),
							mf_ext_link: n && sµ.format_search_url(n.search_url_web || n.search_url,
								mp_req.search_terms, MAX_RES_BY_SRC) || '',
							// mf_remove_source: f_source,
							mf_res_nb: 0
						}
					}
					mf[f_source].mf_res_nb += 1
					mf[f_source].mf_locale_res_nb = mf[f_source].mf_res_nb.toLocaleString(userLang)
				}
				for (let v of Object.values(mf)) {
					mp_req.metaFindingList.add(v)
				}
				display_ongoing_results()
			} catch (exc) {
				console.error(flux_type, 'loading failure', exc)
			}
			document.body.classList.remove('waiting')
		}, 0)
	})
}
function mf_title(src_name) {
	return `${mp_i18n.gettext('Filter results of')} '${src_name}' ${mp_i18n.gettext('only')}`
}
µ.id('mp_sel_mod').addEventListener('click', () => {
	let cur_sel_all = µ.id('mp_sel_all')
	let cur_unsel_all = µ.id('mp_unsel_all')
	let cur_sel_inverse = µ.id('mp_sel_inverse')
	cur_sel_all.style.display = cur_sel_all.style.display === 'inline' ? 'none' : 'inline'
	cur_unsel_all.style.display = cur_unsel_all.style.display === 'inline' ? 'none' : 'inline'
	cur_sel_inverse.style.display = cur_sel_inverse.style.display === 'inline' ?
		'none' : 'inline'
	let cur_sel_mod =dynamic_css.rules[CSS_CHECKBOX_RULE_INDEX].cssText.match(/(block|none)/)[0]
	let next_sel_mod = cur_sel_mod === 'none' ? 'block' : 'none'
	document.getElementsByClassName('mp_export_choices')[0].style.display = next_sel_mod
	dynamic_css.deleteRule(CSS_CHECKBOX_RULE_INDEX)
	dynamic_css.insertRule(
		`.f_selection {display:${next_sel_mod}!important}`, CSS_CHECKBOX_RULE_INDEX)
})
µ.id('mp_sel_all').addEventListener('click', () => {
	for (let item of mp_req.findingList.visibleItems) {
		let check = item.elm.getElementsByClassName('f_selection')[0]
		check.checked = true
	}
})
µ.id('mp_unsel_all').addEventListener('click', () => {
	for (let item of mp_req.findingList.visibleItems) {
		let check = item.elm.getElementsByClassName('f_selection')[0]
		check.checked = false
	}
})
µ.id('mp_sel_inverse').addEventListener('click', () => {
	for (let item of mp_req.findingList.visibleItems) {
		let check = item.elm.getElementsByClassName('f_selection')[0]
		check.checked = check.checked === false
	}
})
/**
 * Export the curent source selection json file.
 */
function export_JSON(token) {
	let cur_sel_items = []
	for (let i of mp_req.findingList.matchingItems)
		cur_sel_items.push(i.values())
	let json_str = `{
		"filters": ${JSON.stringify(tags_to_JSON(), null, '\t')},
		"findings": ${JSON.stringify(cur_sel_items, null, '\t')},
		"meta_findings": ${JSON.stringify(mp_req.metaFindingList.toJSON(), null, '\t')},
		"search_terms": ${JSON.stringify(µ.id('mp_query').value)}
	}`
	if (token && _.is_str(token))
		µ.id('backend_export_content').textContent = json_str
	else
		µ.upload_file_to_user(mµ.export_filename(manifest.version, 'json'), json_str)
}
/**
 * Export the curent source selection csv file.
 */
function export_CSV() {
	document.body.classList.add('waiting')
	let arr = [['ISO_date', 'Source', 'Source_key', 'Favicon', 'URL', 'title', 'txt',
		'by', 'img_src', 'img_alt', 'img_title']]
	// for (let t in Object.keys(tag_select_multiple)) csv_file += `'${t}', `
	// csv_file += ''q_excluded', 'q_included', 'q_search'\n'
	// let tags_1st_line = false
	for (const i of mp_req.findingList.matchingItems) {
		let v = i.values()
		let img = ''
		let alt = ''
		let title = ''
		if (v.mp_data_img) {
			img = v.mp_data_img || ''
			alt = v.mp_data_alt || ''
			title = v.mp_data_title || ''
		}
		arr.push([v.f_ISO_dt, v.f_source, v.mp_data_key, v.f_icon, v.f_url, v.f_h1, v.f_txt,
			v.f_by, img, alt, title])
		/*if (!tags_1st_line) {
			for (const v of Object.values(tag_select_multiple))
				csv_file += `'${µ.preg_quote_file(v.getValue(true).toString())}', `
			csv_file += `'${µ.preg_quote_file(del_src_sel.toString() || '')}', `
			csv_file += `'${µ.preg_quote_file(add_src_sel.toString() || '')}', `
			csv_file += `'${µ.preg_quote_file(mp_req.search_terms)}', `
			tags_1st_line = true
		}*/
	}
	document.body.classList.remove('waiting')
	µ.upload_file_to_user(mµ.export_filename(manifest.version, 'csv'), csv.stringify(arr))
}
function export_RSS() {  // https://validator.w3.org/feed/#validate_by_input
	document.body.classList.add('waiting')
	let flux_items = '', v = {}
	for (let i of mp_req.findingList.matchingItems) {
		v = i.values()
		flux_items += `<item>
			<title>${_.encode_XML(title_line(v))}</title>
			<description>${_.encode_XML(v.f_txt)}</description>
			<media:thumbnail xmlns:media="http://search.yahoo.com/mrss/" url="${v.mp_data_img}"/>
			<link>${_.encode_XML(encodeURI(v.f_url))}</link>
			<pubDate>${new Date(v.f_ISO_dt).toUTCString()}</pubDate>
			<dc:creator>${_.encode_XML(v.f_by)}</dc:creator>
			<guid>${_.encode_XML(encodeURI(v.f_url))}</guid>
			<dc:identifier>${v.mp_data_key}</dc:identifier>
		</item>`
	}
	let flux = `<?xml version='1.0' encoding='UTF-8'?>
	<rss version='2.0' xmlns:dc='http://purl.org/dc/elements/1.1/'>
		<channel>
			<title>Meta-Press.es : ${µ.id('mp_query').value}</title>
			<description>Selection RSS 2.0</description>
			<link>https://www.meta-press.es</link>
			<lastBuildDate>${ndt().toUTCString()}</lastBuildDate>
			<language>${tag_select_multiple.lang.getValue(true) || userLang}</language>
			<generator>Meta-Press.es (v${manifest.version})</generator>
			<docs>http://www.rssboard.org/rss-specification</docs>
			${tags_to_XML('RSS')}
			${flux_items}
		</channel>
	</rss>`
	document.body.classList.remove('waiting')
	µ.upload_file_to_user(mµ.export_filename(manifest.version, 'rss'), flux)
}
/*function export_ATOM() {  // https://validator.w3.org/feed/#validate_by_input
	document.body.classList.add('waiting')  // https://tools.ietf.org/html/rfc4287
	let flux_items = '', v = {}, img=''  // no clean way to embed media in results
	for (let i of mp_req.findingList.matchingItems) {
		v = i.values()
		if (v.f_img_src || v.mp_data_img)
			img = mµ.img_tag(v.mp_data_img, v.mp_data_alt, v.mp_data_title)
		else
			img = ''
		flux_items += `<entry>
			<title>${_.encode_XML(title_line(v))}</title>
			<summary type='html'>${_.encode_XML(img)}${_.encode_XML(v.f_txt)}</summary>
			<published>${v.f_ISO_dt}</published>
			<updated>${v.f_ISO_dt}</updated>
			<link href='${_.encode_XML(encodeURI(v.f_url))}'/>
			<author><name>${_.encode_XML(v.f_by)}</name></author>
			<contributor>
				<name>${_.encode_XML(v.f_source_title)}</name>
				<uri>${_.encode_XML(v.mp_data_key)}</uri>
			</contributor>
			<id>${_.encode_XML(encodeURI(v.f_url))}</id>
		</entry>`
	}
	let flux = `<?xml version='1.0' encoding='UTF-8'?>
	<feed xmlns='http://www.w3.org/2005/Atom'>
		<title>Meta-Press.es : ${µ.id('mp_query').value}</title>
		<subtitle>Decentralize press search engine</subtitle>
		<link href='https://www.meta-press.es'/>
		<generator uri='https://www.meta-press.es'>Meta-Press.es (v${manifest.version})</generator>
		<updated>${ndt().toISOString()}</updated>
		<author><name>Meta-Press.es</name></author>
		<id>https://www.meta-press.es/</id>
		${tags_to_XML('ATOM')}
		${flux_items}
	</feed>`
	document.body.classList.remove('waiting')
	µ.upload_file_to_user(mµ.export_filename(manifest.version, 'atom'), flux)
}*/
// select export
µ.id('mp_export_sel_CSV').addEventListener('click', () => {
	mp_req.findingList.filter(i =>
		i.elm && i.elm.getElementsByClassName('f_selection')[0].checked)
	export_CSV()
	mp_req.findingList.filter()
})
µ.id('mp_export_sel_JSON').addEventListener('click', () => {
	mp_req.findingList.filter(i =>
		i.elm && i.elm.getElementsByClassName('f_selection')[0].checked)
	export_JSON()
	mp_req.findingList.filter()
})
/*µ.id('mp_export_sel_ATOM').addEventListener('click', () => {
	mp_req.findingList.filter(i =>
		i.elm && i.elm.getElementsByClassName('f_selection')[0].checked)
	export_ATOM()
	mp_req.findingList.filter()
})*/
µ.id('mp_export_sel_RSS').addEventListener('click', () => {
	mp_req.findingList.filter(i =>
		i.elm && i.elm.getElementsByClassName('f_selection')[0].checked)
	export_RSS()
	mp_req.findingList.filter()
})
// export all
µ.id('mp_export_search_RSS').addEventListener( 'click', export_RSS)
// µ.id('mp_export_search_ATOM').addEventListener('click', export_ATOM)
µ.id('mp_export_search_JSON').addEventListener('click', export_JSON)
µ.id('mp_export_search_CSV').addEventListener( 'click', export_CSV)
function title_line(v) {
	if (!_.is_str(v.f_h1)) return v.f_source
	let src_prefix = `[${v.f_source}]`
	return v.f_h1.indexOf(src_prefix) !== -1 ? `${src_prefix} ${v.f_h1}` : `${v.f_h1}`
}
function ndt() { return new Date() }
const now = ndt()
const date_filters = [
	function mf_last_day(r)  {return r.values().f_epoc > ndt().setDate(    now.getDate()-1)},
	function mf_last_2d(r)	 {return r.values().f_epoc > ndt().setDate(    now.getDate()-2)},
	function mf_last_3d(r)	 {return r.values().f_epoc > ndt().setDate(    now.getDate()-3)},
	function mf_last_week(r) {return r.values().f_epoc > ndt().setDate(    now.getDate()-7)},
	function mf_last_2w(r)	 {return r.values().f_epoc > ndt().setDate(    now.getDate()-14)},
	function mf_last_month(r){return r.values().f_epoc > ndt().setMonth(   now.getMonth()-1)},
	function mf_last_2m(r)   {return r.values().f_epoc > ndt().setMonth(   now.getMonth()-2)},
	function mf_last_6m(r)   {return r.values().f_epoc > ndt().setMonth(   now.getMonth()-6)},
	function mf_last_1y(r)   {return r.values().f_epoc > ndt().setFullYear(now.getFullYear()-1)},
	function mf_last_2y(r)   {return r.values().f_epoc > ndt().setFullYear(now.getFullYear()-2)},
	function mf_last_5y(r)   {return r.values().f_epoc > ndt().setFullYear(now.getFullYear()-5)},
	function mf_last_dk(r)   {return r.values().f_epoc > ndt().setFullYear(now.getFullYear()-10)}
]
for (let flt of date_filters)
	µ.id(flt.name).addEventListener('click',
		evt => filter_last(evt.target, flt))
µ.id('mf_all_res').addEventListener('click',
	evt => filter_last(evt.target, null))
function mf_date_slicing() {
	let prev_nb = 0
	let total_nb = set_filter_nb(null, 'mf_all_res', 0, 0)
	for (let flt of date_filters)
		prev_nb = set_filter_nb(flt, flt.name, prev_nb, total_nb)
	mp_req.findingList.filter()
}
function set_filter_nb(fct, id, prev_nb, total_nb) {
	mp_req.findingList.filter()
	if(fct) mp_req.findingList.filter(fct)
	let nb = mp_req.findingList.matchingItems.length
	if (nb > 0 && nb > prev_nb + (total_nb * 0.06)) {
		let p = Math.floor(nb * 40 / mp_req.findingList.size())
		µ.id(id+'_nb').textContent =
			`${nb.toLocaleString(userLang)} ${'.'.repeat(p)}`
		µ.id(id).parentNode.style.display='flex'
	} else {
		µ.id(id).parentNode.style.display='none'
	}
	return nb
}
/**
 * Filter the result in the starting date.
 * @param {*} target
 * @param {Function} fct the filter function
 */
function filter_last(target, fct) {
	mp_req.findingList.filter()
	if(fct) mp_req.findingList.filter(fct)
	bold_clicked_a(target, 'mf_dt_lbl')
}
/**
 * Add the given class to the target and remove the other user of this class.
 * @param {Node} target target
 * @param {string} a_class class to add
 */
function bold_clicked_a(target, a_class) {
	for (let i of document.getElementsByClassName(a_class))
		i.classList.remove('bold')
	target.classList.add('bold')
}
µ.id('mp_filter').addEventListener('keyup', searchInResCallback)
µ.id('mp_clear_filter').addEventListener('click', () => {
	µ.id('mp_filter').value = ''
	searchInResCallback({target: {value: ''}})
})
let searchInResCallback_timeout
function searchInResCallback (evt) {
	clearTimeout(searchInResCallback_timeout)
	searchInResCallback_timeout = setTimeout(() => {
		if (mp_req.findingList.searched)
			µ.id('mf_total').click()
		if (mp_req.findingList.filtered)
			filter_last(µ.id('mf_all_res'), null)
		let val = evt.target.value
		if (val)
			mp_req.findingList.search(val, ['f_h1', 'f_source', 'f_dt', 'f_by',	'f_txt'])
		else
			mp_req.findingList.search()
		mf_date_slicing()
	}, evt.keyCode === 13 ? 0 : 500)
}
/* * * Headlines * * */
const headlineList = new List('headlines', {
	item: 'headline-item',
	valueNames: [
		'h_title',
		'h_body',
		{ name: 'h_url',	attr: 'href' },
		{ name: 'h_icon',	attr: 'src' },
		{ name: 'h_tip',	attr: 'title' },
		{ name: 'h_dt',		attr: 'mp-data-dt' },
		{ name: 'h_img',	attr: 'src' },
		{ name: 'h_img_alt',attr: 'alt' },
		{ name: 'h_img_title',attr: 'title' },
	],
	page: HEADLINE_PAGE_SIZE,
	pagination: {
		outerWindow: 10,
		innerWindow: 10,
		item: '<li><a class="page" href="#headlines"></a></li>'
	}
})
let headline_item = 1
let headline_timeout, headline_fadeout_timeout
function rotate_headlines_timeout() {
	headline_timeout = setTimeout(() => {
		if (headlineList.size() > HEADLINE_PAGE_SIZE) {
			headline_item = (headline_item + HEADLINE_PAGE_SIZE) % headlineList.size()
			headlineList.show(headline_item, HEADLINE_PAGE_SIZE)
			rotate_headlines()
		}
	}, 10000)
}
function rotate_headlines() {
	clearTimeout(headline_fadeout_timeout)
	rotate_headlines_timeout()
	fadeout_pagination_dot('headlines', 'rgba(0, 208, 192, 1)', 1)
}
function fadeout_pagination_dot(id, bg_color, dot_opacity) {
	headline_fadeout_timeout = setTimeout(() => {
		let dot = µ.$(`#${id} ul.pagination > li.active a.page`)
		dot_opacity -= 0.1
		if (dot)
			dot.style.backgroundColor = bg_color.replace(/, 1\)/, `, ${dot_opacity})`)
		fadeout_pagination_dot(id, bg_color, dot_opacity)
	}, 1000)
}
µ.id('headlines').addEventListener('mouseover', () => {
	clearTimeout(headline_timeout)
	clearTimeout(headline_fadeout_timeout)
})
µ.id('headlines').addEventListener('mouseout', async () => {
	rotate_headlines()
})
let set_hdl = new Set()
µ.id('load_headlines').addEventListener('click', async () => load_headlines())
µ.id('reload_headlines').addEventListener('click', async () => load_headlines())
µ.id('remove_headlines').addEventListener('click', async () => {
	headlineList.clear()
	µ.id('headlines_wrapper').style.display = 'none'
})
function load_internal_news() {
	const mp_hls = µ.$$('.mp_hl')
	for (let h of mp_hls)
		headlineList.add({
			h_title: mp_i18n.gettext(h.textContent),
			h_url: h.href,
			h_icon: '/img/favicon-metapress-v2.png',
			h_tip: mp_i18n.gettext(DOMµ.strip_HTML_tags(h.title)),
			h_img: '.',
			h_img_alt: ' ',
		})
	headlineList.show(0, mp_hls.length)
	for (let i=mp_hls.length-1; i--;)  // the last link should keep its target="_blank"
		// numbering starts at 1 for nth-of-type
		µ.$(`#headlines > ul >li:nth-of-type(${i+1}) a`).target = ''
	µ.$('#headlines > ul >li:nth-of-type(1) a').addEventListener('click',
		async () => load_headlines())
}
async function load_headlines() {
	set_hdl.clear()
	let headline_host = mµ.get_current_news_hosts(current_source_selection, source_objs)
	try {
		if (! await mµ.request_permissions(headline_host)) {
			return alert_perm()
		}
	} catch (exc) {
		// alert_perm()
		console.warn('Ignored exception', exc)
	}
	document.body.classList.add('waiting')
	µ.id('headlines_wrapper').style.display = 'block'
	headlineList.clear()
	load_internal_news()
	µ.id('mp_headlines_page_sizes').style.display = 'block'
	µ.id('load_headlines').style.display = 'none'
	let i = 0
	for (let k of current_source_selection.slice(0, await mµ.get_max_news_loading())) {
		if(await mµ.check_host_perm (current_source_selection, source_objs)) {
			const n = source_objs[k]
			if (!n) {
				console.warn('Why is this source missing ?', k, n)
				continue
			}
			if (!n.news_rss_url) {
				console.warn(k, 'missing news_rss_url', n)
				continue
			}
			await µ.sleep(INTER_FETCH_DELAY / 10 * (i++) / 2)
			const raw_rep = await mµ.try_fetch(n.news_rss_url)
			if (!raw_rep.ok) {
				console.warn(`${n.news_rss_url} status ${raw_rep.status}`)
				continue
			}
			let text_rep
			if (n.tags.charset) {
				text_rep = await raw_rep.arrayBuffer()
				let text_decoder = new TextDecoder(n.tags.charset, {fatal: true})
				text_rep = text_decoder.decode(text_rep)
			} else {
				text_rep = await raw_rep.text()
			}
			const r = mµ.DOM_parse_text_response(text_rep, raw_rep.headers.get('content-type'))
			try {
				let items = r.querySelectorAll('item')
				if (!items.length)
					items = r.querySelectorAll('entry')
				const RSS = sµ.XML_SRC[sµ.find_RSS_variant(items, n)]
				// console.log('RSS variant', sµ.find_RSS_variant(items, n))
				const n_RSS = sµ.extend_source(RSS, {tags: n.tags,
					domain_part: n.domain_part,
					type: 'XML'
				})
				for (let h of Array.from(items).slice(0, await mµ.get_max_news_by_src())) {
					const h_title = mµ.$_('r_h1', h, n_RSS, i)
					const h_body = mµ.$_('r_txt', h, n_RSS, i, true)
					const h_tibo = `${h_title} ${h_body}`
					// console.log('h_dt', mµ.$_('r_dt', h, n_RSS, i, false))
					const h_dt = new Date(mµ.$_('r_dt', h, n_RSS, i, false))
					let h_img = img_lookup({}, '', h, n_RSS, k, i)
					if (!h_img.f_img_src) { // for The Conversation
						h_img = img_lookup({f_txt: mµ.$_('r_txt', h, n_RSS, i, false)},
							'', h, n_RSS, k, 0)
					}
					// console.log(h_img)
					if(!set_hdl.has(h_title)) {
						set_hdl.add(h_title)
						headlineList.add({
							h_title:_.shorten(h_tibo, HEADLINE_TITLE_SIZE),
							h_body: h_body,
							h_url:	µ.urlify(mµ.$_('r_url', h, n_RSS, i), n.domain_part),
							h_icon: n.favicon_url,
							h_tip: `${n.tags.name} (${h_dt.toLocaleString(userLang)}) ${h_title}`,
							h_dt: h_dt.toISOString(),
							h_img: h_img.f_img_src || '',
							h_img_alt: h_img.f_img_alt || h_title,
							h_img_title: h_img.f_img_title || h_title,
							mp_data_img: h_img.f_img_src || '',
							mp_data_alt: h_img.f_img_alt || h_title,
							mp_data_title: h_img.f_img_title || h_title,
							mp_data_key: n.domain_part,
						})
					}
				}
			} catch (exc) {
				console.error('header for', k, exc)
			}
			headlineList.sort('h_dt', {order: 'desc'})
		}
	}
	headlineList.show(0, HEADLINE_PAGE_SIZE)
	document.body.classList.remove('waiting')
}
/* * * Tags * * */
const select_multiple_opt = {
	removeItemButton: true,
	resetScrollPosition: false,
	placeholder: true,
	placeholderValue: '*',
	duplicateItemsAllowed: false,
	searchResultLimit: 10,
	allowHTML: true
}
const tag_select_multiple = {
	'tech': '',
	'lang': '',
	'country': '',
	'themes': '',
	'src_type': '',
	'res_type': ''
}
let add_src_sel = []
let del_src_sel = []
let nb_tags = {}
async function populate_tags(source_selection) {
	for (let tag_name of Object.keys(tag_select_multiple)) {
		nb_tags[tag_name] = {}
	}
	for (let k of source_selection) {
		let n = source_objs[k]
		if (!_.is(n)) return
		for (let tag_name of Object.keys(tag_select_multiple)) {
			let tag_val = n.tags[tag_name]
			if (_.is_str(tag_val)) {
				nb_tags[tag_name][tag_val] = 1 + nb_tags[tag_name][tag_val] || 1
			} else if (_.is_obj(tag_val))
				for (let tag_val_itm of tag_val)
					nb_tags[tag_name][tag_val_itm] = 1 + nb_tags[tag_name][tag_val_itm] || 1
			else
				console.warn(`Unknown tag type ${tag_name} ${tag_val} for ${k}`)
		}
	}
	for (let tag_name of Object.keys(tag_select_multiple)) {
		let s = []
		for (let t of Object.keys(nb_tags[tag_name]).sort()) {
			if(tag_name === 'lang')
				s.push({value: t, label:
					`${LANG_NAME.of(t)}<small> <i>[${t}]</i> <i>(${nb_tags[tag_name][t]})</i></small>`})
			else if (tag_name === 'country')
				s.push({value: t, label:`${COUNTRY_NAME.of(t)}<small> <i>[${t}]</i> <i>(${
					nb_tags[tag_name][t]})</i></small>`})
			else
				s.push({value: t, label:
					`${mp_i18n.gettext(t)}<small> <i>(${nb_tags[tag_name][t]})</i></small>`})
		}
		if(tag_select_multiple[tag_name] !== '')
			tag_select_multiple[tag_name].destroy()
		if (tag_name == 'tech')
			s.push({value: 'cherry-pick_sources', label: 'cherry-pick'})
		tag_select_multiple[tag_name] = new Choices(µ.id(`tags_${tag_name}`),
			Object.assign({choices: s}, select_multiple_opt))
	}
}
async function dns_prefetch(current_source_selection) {
	let domain_to_prefetch = new Set()
	let all_search_urls = sµ.get_all_search_urls(current_source_selection, source_objs)
	for (let i of all_search_urls)
		domain_to_prefetch.add(new URL(i).origin)
	for (let i of domain_to_prefetch)
		append_prefetch_link(i)
	for (let src_key of current_source_selection)
		await mµ.dns_resolve(new URL(src_key).host)
}
let already_add = []
function append_prefetch_link(url) {
	if (already_add.includes(url)) return
	let l = document.createElement('link')
	l.rel='dns-prefetch'
	l.href= url
	l.crossorigin = 'anonymous'
	document.head.appendChild(l)
	already_add.push(url)
}
function try_store_tags () {
	if (!QUERYSTRING.get('id_sch_s'))
		mµ.to_storage('filters', tags_to_JSON())
}
async function on_tag_sel(opt={}) {
	try_store_tags()
	let new_src_sel = {}
	for (const tag_name of Object.keys(tag_select_multiple)) {
		new_src_sel[tag_name] = {}
		for (let selected_tag of tag_select_multiple[tag_name].getValue(true)) {
			if (tag_name !== 'tech') { // inner accumulation
				for (let k of source_keys)
					if (Array.isArray(source_objs[k].tags[tag_name])) {
						if (source_objs[k].tags[tag_name].includes(selected_tag))
							new_src_sel[tag_name][k] = ''
					} else {
						if (source_objs[k].tags[tag_name] === selected_tag)
							new_src_sel[tag_name][k] = ''
					}
			} else { // inner intersection
				let cur_tag_keys = []
				for (let k of source_keys)
					if (source_objs[k].tags[tag_name].includes(selected_tag))
						cur_tag_keys.push(k)
				if (Object.keys(new_src_sel.tech).length === 0) {
					for (let c of cur_tag_keys)
						new_src_sel.tech[c] = ''
					continue
				} else {
					let prec_tag_keys = Object.keys(new_src_sel[tag_name])
					new_src_sel[tag_name] = {}
					for (let k of prec_tag_keys.filter(e => cur_tag_keys.includes(e)))
						new_src_sel[tag_name][k] = ''
				}
			}
			if (tag_select_multiple[tag_name].getValue(true).length > 0 &&
				Object.keys(new_src_sel[tag_name]).length === 0 &&
				add_src_sel.length === 0
			) {
				empty_sel(opt)
				return
			}
		}
	}
	let new_src_sel_keys = []
	if (!tag_select_multiple.tech.getValue(true).includes('cherry-pick_sources')) {
		new_src_sel_keys = source_keys
		let tag_sel_keys = []
		for (let tag_name of Object.keys(tag_select_multiple)) { // get the smallest ensemble
			if (new_src_sel_keys.length === 0) {
				break
			} else {
				tag_sel_keys = Object.keys(new_src_sel[tag_name])
				if (tag_sel_keys.length > 0)
					new_src_sel_keys = new_src_sel_keys.filter(v => tag_sel_keys.includes(v))
			}
		}
	}
	if (new_src_sel_keys.length > del_src_sel.length || add_src_sel.length > 0) {
		for (const i of del_src_sel) _.array_remove_once(new_src_sel_keys, i)
		for (const i of add_src_sel) _.array_add_once(new_src_sel_keys, i)
		current_source_selection = new_src_sel_keys
		current_source_nb = current_source_selection.length
		try_store_tags()
		if (await mµ.get_live_search_reload() && µ.id('mp_query').value) {
			µ.id('mp_submit').click()
		}
		if ((await mµ.get_news_loading()) &&
			headlineList.size() === 0 &&
			!QUERYSTRING.get('submit')
		) {
			load_headlines()
		} else if(await mµ.get_live_news_reload() && headlineList.size()) {
			let h_display = µ.id('mp_headlines_page_sizes').style.display
			if(h_display !== 'none' && (opt && opt.target && opt.target.id !== 'tags_tech'))
				µ.id('reload_headlines').click()
		}
	} else {
		empty_sel(opt)
	}
	update_nb_src()
	display_show_src()
}
function empty_sel(opt) {
	alert(mp_i18n.gettext(µ.id('empty_sel_txt').textContent))
	if (opt && _.is(opt.parentNode)) {
		tag_select_multiple[opt.parentNode.id.replace('tags_', '')].setValue(opt.value)
	} else {
		set_default_tags()
	}
	on_tag_sel()
}
function tags_to_JSON() {
	let tags = {}
	for (let t of Object.keys(tag_select_multiple)) {
		tags[t] = tag_select_multiple[t].getValue(true)
	}
	tags.del_src_sel = del_src_sel
	tags.add_src_sel = add_src_sel
	return tags
}
function tags_to_XML(XML_type) {
	let tags = '', tag_val
	for (let tag_name of Object.keys(tag_select_multiple)) {
		tag_val = tag_select_multiple[tag_name].getValue(true)
		switch(typeof(tag_val)) {
		case 'string': tags += XML_tag_in_cat(XML_type, tag_name, tag_val) ;break
		case 'object': for (let v of tag_val) tags += XML_tag_in_cat(XML_type, tag_name, v) ;break
		}
	}
	for (const v of del_src_sel) tags += XML_tag_in_cat(XML_type, 'del_src_sel', v)
	for (const v of add_src_sel) tags += XML_tag_in_cat(XML_type, 'add_src_sel', v)
	return tags
}
function XML_tag_in_cat(XML_type, tag_name, tag_value) {
	tag_name = _.encode_XML(tag_name)
	tag_value = _.encode_XML(tag_value)
	if ('ATOM' === XML_type) return `<category term='${tag_value}' label='mp__${tag_name}' />\n`
	else return `<category>mp__${tag_name}__${tag_value}</category>\n`
}
function tags_from_XML(tag_nodes, xml_type) {
	let tag_values = {}
	for (let t of Object.keys(tag_select_multiple)) tag_values[t] = []
	tag_values.add_src_sel = []
	tag_values.del_src_sel = []
	for (let c of tag_nodes)
		if ('ATOM' === xml_type) {
			let c_name = c.attributes.label.value
			try { tag_values[c_name.split('__')[1]].push(`${c.attributes.term.value}`) }
			catch (exc) { console.warn(`Bad ATOM filter name '${c_name}' (${exc}).`) }
		} else {
			let c_split = c.textContent.split('__')
			if (c_split.length > 2) {
				try { tag_values[c_split[1]].push(`${c_split[2]}`) }
				catch (exc) { console.warn(`Bad RSS filter name '${c.textContent}' (${exc}).`) }
			}
		}
	tags_from_JSON(tag_values)
	on_tag_sel()
}
function tags_from_JSON(tag_values) {
	let v, s
	for (let tag_name of Object.keys(tag_values)) {
		s = tag_select_multiple[tag_name]
		v = tag_values[tag_name]
		if (!s) continue  // we want to avoid : s === ""
		s.passedElement.element.removeEventListener('change', on_tag_sel)
		s.removeActiveItems()
		s.setChoiceByValue(v)
		s.passedElement.element.addEventListener('change', on_tag_sel)
	}
	if (tag_values.add_src_sel)
		for (let t of tag_values.add_src_sel)
			if (source_keys.includes(t))
				add_src_sel = tag_values.add_src_sel
	if (tag_values.del_src_sel)
		for (let t of tag_values.del_src_sel)
			if (source_keys.includes(t))
				del_src_sel = tag_values.del_src_sel
	on_tag_sel()
}
function is_virgin_tags() {
	for (let k of Object.keys(tag_select_multiple))
		if (tag_select_multiple[k].setChoiceByValue(k).getValue(true).length > 0) return false
	if(QUERYSTRING.get('id_sch_s')) return false
	if(add_src_sel.length || del_src_sel.length) return false
	return true
}
function is_advanced_search() {
	let r = JSON.stringify(get_default_tags()) !== JSON.stringify(tags_to_JSON())
	return r || del_src_sel.length || add_src_sel.lenth
}
µ.id('reset_tag_sel').addEventListener('click', () => {
	set_default_tags()
	on_tag_sel()
})
function set_default_tags() {
	if (QUERYSTRING.get('keep_empty_tags'))
		return console.info('Tags kept empty as per keep_empty_tags querystring variable')
	console.info('Meta-Press.es : loading default tags')
	tags_from_JSON(get_default_tags())
}
/**
 * Create an object with the default search tags.
 * @returns {Object} the tags
 */
function get_default_tags() {
	return {
		'tech': ['HTTPS', 'fast', 'one word'],
		'lang': [userLang.slice(0, 2).toLowerCase()],
		'country': [],
		'themes': [],
		'src_type': ['Press'],
		'res_type': [],
		'del_src_sel': [],
		'add_src_sel': []
	}
}
/**
 * Set the tags search in browser data in the page.
 * @param {Object} stored_tags the tags in memory
 */
function init_tags(stored_tags) {
	tags_from_JSON(stored_tags)
	if (is_virgin_tags()) set_default_tags()
	if (is_advanced_search() &&
		getComputedStyle(µ.id('tags_row')).display === 'none')
		µ.id('tags_handle').click()
	µ.id('mp_src_total').textContent = source_keys.length
	µ.id('mp_src_countries').textContent = tag_select_multiple.country._presetChoices.length
	µ.id('mp_src_langs').textContent = tag_select_multiple.lang._presetChoices.length
}
/* * * end tags * * */
function set_query_placeholder() {
	const val = [
		'La Quadrature du Net', 				'Wikipedia', 		'Repair Café', 	'Chelsea Manning',
		'5 centimeters per second', 		'Tor Browser', 	'Fairphone', 		'Frances Haugen',
		'Internet ou Minitel 2.0', 			'AsciiDoctor', 	'Yellow vests', 'Pliocene Exile',
		'Le Prince de Machiavel', 			'Nausicaä', 		'OpenStreetMap','Grimoire-Command.es',
		'Pensées pour moi-même', 				'F-Droid.org', 	'Leto Atreides','Le Petit Prince',
		'La princesse de Clèves', 			'Nuit Debout', 	'Reporterre', 	'Pablo Servigne',
		'Another World by Éric Chahi', 	'Delta.Chat', 	'OpenContacts',
	]
	µ.id('mp_query').placeholder =
		mp_i18n.gettext('Search terms, ex.') +` : ${val[_.pick_between(0, val.length)]}`
}
µ.id('tags_handle').addEventListener('click', () => {
	µ.id('tags_row').classList.toggle('display_none')
	µ.id('tags_2nd_row').classList.toggle('display_none')
	µ.id('tags_handle').textContent = µ.id('tags_handle').textContent === '+' ? '-' : '+'
	if (µ.id('tags_handle').textContent === '+') µ.id('list_src').style.display = 'none'
	else if (del_src_sel.length || add_src_sel.length) {
		µ.id('list_src').style.display = 'block'
		µ.id(del_src_sel.length ? 'v_del' : 'v_add').click()
	}
})
µ.id('mp_running_btn_src').addEventListener('click', (evt) => {
	let btn_src_text = µ.id('mp_running_btn_src_text')
	evt = evt.target
	evt.textContent = evt.textContent === '+' ? '-' : '+'
	btn_src_text.classList.toggle('display_none')
	for(let c of btn_src_text.children){
		c.style.display = btn_src_text.style.display
	}
})
function is_tag_in_querystring(QUERYSTRING) {
	if (QUERYSTRING.get('id_sch_s')) return true
	for (let t of Object.keys(tag_select_multiple))
		if (QUERYSTRING.get(t)) return true
	if (QUERYSTRING.get('rm_src_sel') || QUERYSTRING.get('add_src_sel')) return true
	return false
}
/**
 * Reset tag_select_multiple, remove all the search settings.
 */
function get_empty_json_tags() {
	let json_obj = {}
	for (let t of Object.keys(tag_select_multiple))
		json_obj[t] = []
	return json_obj
}
function querystring_to_json(sp) {  // a generic version is a bad idea security-wise
	// inputs must be sanitized
	let json_obj = get_empty_json_tags()
	for (const [key, value] of sp) {
		if (_.is(json_obj[key]))
			json_obj[key].push(value)
		if (key === 'rm_src_sel')
			src_list_del(value)
		if (key === 'add_src_sel')
			src_list_add(value)
	}
	return json_obj
}
function manage_mp_input_focus() {
	for (let y of µ.id$('.choices__list--dropdown > .choices__list > .choices__item')) {
		y.addEventListener('click', () => { µ.id('#mp_query').focus() }, true) // is not called
	}
}
let id_elt, last_elt
async function add_elt_url(id) {
	id_elt = id.split('__')[1]
	let list_elt = mp_req.findingList.items
	let not_in_future = false
	let i = 0
	while(!not_in_future && list_elt.length) {
		let dt_elt = new Date(list_elt[i]._values.f_epoc)
		not_in_future = new Date() > dt_elt
		if(not_in_future) {
			last_elt = dt_elt.toUTCString()
		} else { i++ }
	}
	let store = await mµ.get_storage()
	await ss.init_table_sch_s(store)
	let sch_sea = store[`sch_sea__${id_elt}`]
	let local_url = gen_permalink(false)
	let new_url = new URL(local_url.origin + local_url.pathname + sch_sea)
	let sto_r = new_url.searchParams.get('last_res')
	const [list_p, params] = await mµ.set_text_params(new_url, mp_i18n)
	let body_txt = `${mp_i18n.gettext(' results for "')}` + list_p.q + '"\n' + params
	local_url.searchParams.delete('last_res')
	local_url.searchParams.sort()
	new_url.searchParams.delete('last_res')
	new_url.searchParams.delete('submit')
	new_url.searchParams.sort()
	if(local_url.search === new_url.search) { // if true we continue
		new_url.searchParams.set('last_res', last_elt)
		sch_sea = new_url.search
		let nb_new_res = count_new_res(sto_r) // sto_r can be null
		if(nb_new_res > 0) {
			µ.id('favicon').href = '/img/favicon-metapress-active.png'
			if (nb_new_res >= 10) { nb_new_res = '+' + nb_new_res }
			body_txt = nb_new_res + mp_i18n.gettext(' new ') + body_txt
			let notif_txt = mp_i18n.gettext('New result(s) for scheduled search')
			mµ.notify_user(`${notif_txt} ${id_elt}`, body_txt)
		}
		await mµ.to_storage(`sch_sea__${id_elt}`, sch_sea)
	}
}
function count_new_res(sto_r) {
	if (!sto_r) return 0
	let sto_r_dt = new Date(sto_r)
	let results = mp_req.findingList.matchingItems
	let last = null
	let count = 0
	for(let r of results) {
		// r.elm.classList.remove('first_new_result', 'new_result', 'last_new_result')
		if (!r.elm) break
		let dt_r = r.elm.querySelector('time').dateTime
		if(sto_r_dt < new Date(dt_r)) {
			last = r.elm
			last.classList.add('new_result')
			if (!count) last.classList.add('first_new_result')
			count += 1
		}
	}
	if(last) last.classList.add('last_new_result')
	return count
}
// date filter
let start = µ.id('date_filter_sta')
let stop = µ.id('date_filter_sto')
stop.disabled = true
µ.id('clear_date').onclick = evt => {
	start.value = ''
	stop.value = ''
	stop.min = ''
	stop.disabled = true
	filter_last(evt.target, null)
}
start.value = ''
stop.value = ''
start.onchange = evt => {
	if (new Date(evt.target.value)) {  // eslint-disable-line no-constant-condition
		µ.id('date_filter_sto').min = evt.target.value
		filter_last(evt.target, null)
		// reset if Start empty
		stop.disabled = evt.target.value === ''
		if (stop.disabled)
			stop.value = ''
		if (evt.target.value !== '')
			filter_date_range(evt)
	}
}
stop.onchange = evt => {
	if (new Date(evt.target.value))  // eslint-disable-line no-constant-condition
		filter_date_range(evt)
}
/**
 * Applies a date range filter with #date_filter_sta #date_filter_sto.
 * @param {Event} evt The fired event
 */
function filter_date_range(evt) {
	filter_last(evt.target, (r) => {
		let start = µ.id('date_filter_sta').value
		let stop = µ.id('date_filter_sto').value
		if (stop  === '') stop = new Date().toISOString()
		if (start === '') start = new Date().toISOString()
		if (start > stop) start = stop
		let date_stop = new Date(stop)
		date_stop.setHours('23')
		date_stop.setMinutes('59')
		date_stop.setSeconds('59')
		return r.values().f_epoc >= new Date(start) &&
			r.values().f_epoc <= new Date(date_stop)
	})
}
// end date filter
/**
 * Refresh the Meta-Press sources: source_objs, current_source_nb
 * and also refresh the ChoiceJS selection.
 * @param {string} src_name The name of the soure to get
 * @param {boolean} build_src If we need to rebuild the sources before import
 * @param {boolean} init If we are in the intended initial call
 * @param {boolean} reload If we need to reload the sources
 */
async function update_source_objs(src_name='', built_src, init=true, reload=false) {
	dt_source_objs = new Date()
	source_objs = await sµ.get_prov_src(src_name, built_src)
	if (await mµ.get_child_mode()) {
		let kids_src_objs = {}
		for (const [key, val] of Object.entries(source_objs))
			if (val.tags.tech.includes('for kids'))
				kids_src_objs[key] = val
		source_objs = kids_src_objs
	}
	source_keys = Object.keys(source_objs)
	current_source_selection = _.deep_copy(source_keys)
	current_source_nb = source_keys.length
	if (TEST_SRC)
		return
	if (typeof (mp_i18n) !== 'undefined' && typeof(browser) !== 'undefined') {
		let old = await mµ.get_filters()
		if(reload) {
			// let t_id = await browser.tabs.getCurrent().then(e => e.id)
			let t_id = (await browser.tabs.getCurrent()).id
			old = (await browser.storage.local.get(`filters_${t_id}`))[`filters_${t_id}`]
			browser.storage.local.remove(`filters_${t_id}`)
		} else if (!init && is_tag_in_querystring(QUERYSTRING)) {
			old = querystring_to_json(QUERYSTRING)
		}
		await populate_tags(current_source_selection)
		init_tags(old)
		µ.id('cur_tag_perm_nb').onclick = async () => {
			µ.id('cur_tag_perm_nb').classList.remove('draw_attention')
			await mµ.request_sources_perm(current_source_selection, source_objs)
			update_nb_src()
		}
		display_show_init()
		update_nb_src()
	} else {
		await populate_tags(current_source_selection)
		let tags = {}
		if(!init && is_tag_in_querystring(QUERYSTRING))
			tags = querystring_to_json(QUERYSTRING)
		init_tags(tags)
		display_show_init()
	}
	// document.dispatchEvent(new Event('tags_src_load'))
}
/**
 * Import a JSON containing the custom source
 * https://stackoverflow.com/questions/16215771/how-to-open-select-file-dialog-via-js#answer-40971885
 * @param {Event} evt event fired
 */
/* async function import_custom_src() {
	let input = document.createElement('input')
	input.type = 'file'
	input.accept = '.json'
	input.click()
	input.onchange = async () => {
		let file = input.files.item(0)
		let txt = await file.text()
		try {
			let new_src = JSON.parse(txt)
			let old = await mµ.get_custom_src()
			let merge_src = Object.assign(old, new_src)
			mµ.set_custom_src()
			mµ.update_need_reload_src()
			update_source_objs('', Object.assign(source_objs, merge_src), false, false)
		} catch (error) {
			window.alert('File not readable (bad format or corrupted file)')
		}
	}
} */
/**
 * Export the user custom source(sf) in JSON file.
 */
/* function export_custom_src() {
		µ.upload_file_to_user(`${_.ndt_human_readable()}_custom_sources.json`,
			await mµ.get_custom_src())
}*/
// µ.id('import_c_src').onclick = import_custom_src
// µ.id('export_c_src').onclick = export_custom_src
/**
 * Initialises the tab div and the interaction with its button.
 * @param {string} id_manager The id of the div which contains the tab
 * @param {Function} treatment The function called when tab changes, the tab id is passed in
 * parameter
 */
async function init_tabs(id_manager, treatment) {
	let manager = document.getElementById(id_manager)
	for (const i of manager.children) {
		i.addEventListener('click', evt => {
			if (evt.target.classList.contains('select')) return
			for (const j of manager.children)
				j.classList.remove('select')
			evt.target.classList.add('select')
			treatment()
		})
	}
}
let display_panel
µ.id('mp_edit_v_src_search').onclick = () => {
	let page_size = display_panel.page
	display_panel.page = source_keys.length
	display_panel.update()
	let command_bar = µ.id('mp_v_sel')
	command_bar.style.display = 'block' !== command_bar.style.display ? 'block' : 'none'
	display_panel.page = page_size
	display_panel.update()
}
init_tabs('tab_v_src', () => {
	display_show_src()
	µ.id('mp_clear_v_src_search').click()
})  // source list tab selector
µ.id('view_select_src').onclick = () => {
	let section = µ.id('list_src')
	if (section.style.display != 'block') {
		section.style.display = 'block'
		display_show_src()
	} else section.style.display = 'none'
}
µ.id('close_list_src').onclick = () => {
	µ.id('list_src').style.display = 'none'
}
/**
 * Fills the List.js display src panel with the source.
 * @async
 * @function
 */
async function display_show_init() {  // fill
	display_panel.clear()
	let to_add = []
	for (const [key, n] of Object.entries(source_objs)) {
		let symbol = ''
		if (n.overwrite)
			symbol = '&#10071;'
		if (n.custom)
			symbol += '&#128190;'
		let favicon_url = n.favicon_url
		let name = n.tags.name
		let country = n.tags.country
		let lang = n.tags.lang
		let a = n.tags.src_type
		a = _.is_str(a) ? mp_i18n.gettext(a) : a.map(x => mp_i18n.gettext(x)).join(', ')
		let b = n.tags.res_type
		b = _.is_str(b) ? mp_i18n.gettext(b) : b.map(x => mp_i18n.gettext(x)).join(', ')
		to_add.push({
			f_icon: favicon_url,
			f_icon_title: name,
			black: symbol,
			src_list_nom: name,
			src_list_lang: lang,
			src_list_lang_name: LANG_NAME.of(lang),
			src_list_country: country,
			src_list_type: a,
			src_list_result_type: b,
			src_list_url : key,
			src_list_edit: _.domain_part(document.baseURI) + `/html/new_source.html?name=${key}`,
			src_list_del: key,
			src_list_add: key,
			src_list_only: key
		})
	}
	display_panel.add(to_add, () => {
		display_panel.sort('src_list_nom', { order: 'asc' })
		display_show_src()
	})
}
function src_list_del (src_key) {
	if (source_keys.includes(src_key))
		_.array_add_once(del_src_sel, src_key)
	_.array_remove_once(current_source_selection, src_key)
	_.array_remove_once(add_src_sel, src_key)
}
function src_list_add(src_key) {
	_.array_remove_once(del_src_sel, src_key)
	if (source_keys.includes(src_key)) {
		_.array_add_once(current_source_selection, src_key)
		_.array_add_once(add_src_sel, src_key)
	}
}
function update_display_panel() {
	for (const i of display_panel.visibleItems) {
		let li = i.elm
		li.querySelector('.src_list_del').onclick = evt => {
			let key = evt.target.dataset.src_url
			const current_tab = µ.$('#tab_v_src .select').id
			if (current_tab === 'v_del')
				_.array_remove_once(del_src_sel, key)
			else
				src_list_del(key)
			on_tag_sel()
		}
		li.querySelector('.src_list_add').onclick = evt => {
			let key = evt.target.dataset.src_url
			src_list_add(key)
			on_tag_sel()
		}
		li.querySelector('.src_list_only').onclick = evt => {
			let key = evt.target.dataset.src_url
			_.array_remove_once(del_src_sel, key)
			current_source_selection = []
			add_src_sel = []
			_.array_add_once(current_source_selection, key)
			_.array_add_once(add_src_sel, key)
			tag_select_multiple.tech.setChoiceByValue('cherry-pick_sources')
			on_tag_sel()
		}
		let add = li.querySelector('.src_list_add')  // display the corresponding button
		let del = li.querySelector('.src_list_del')
		let key = del.dataset.src_url
		const current_tab = µ.$('#tab_v_src .select').id
		del.style.display = current_tab === 'v_del' ? 'block' : 'none'
		add.style.display = 'block'
		if (current_source_selection.includes(key)) {
			li.classList.add('selected')
			add.style.display = 'none'
			del.style.display = 'block'
		} else li.classList.remove('selected')
		if (['v_select', 'v_all'].includes(current_tab)) {
			µ.id('mp_v_src_del_page').style.display = 'none'
			µ.id('mp_v_src_del_all').style.display = 'none'
		} else {
			µ.id('mp_v_src_del_page').style.display = 'inline-block'
			µ.id('mp_v_src_del_all').style.display = 'inline-block'
		}

	}
}
/**
 * Refresh the filter used when the sources to display change.
 */
function display_show_src() {
	if (!_.is(display_panel)) return
	display_panel.filter()
	let current_tab = µ.$('#tab_v_src .select').id
	if (current_tab !== 'v_all') {
		let current_content
		switch (current_tab) {
		case 'v_select': current_content = current_source_selection ;break
		case 'v_add': current_content = add_src_sel ;break
		case 'v_del': current_content = del_src_sel ;break
		}
		display_panel.filter(item => current_content.includes(item.values().src_list_url))
	}
}
µ.id('mp_v_src_del_all').onclick = () => {
	let current_tab = µ.$('#tab_v_src .select').id
	if (current_tab === 'v_del')
		del_src_sel = []
	on_tag_sel()
}
µ.id('mp_v_src_del_page').onclick = () => {
	let current_tab = µ.$('#tab_v_src .select').id
	if (current_tab === 'v_del')
		for (const i of display_panel.visibleItems)
			_.array_remove_once(del_src_sel,
				i.elm.querySelector('.src_list_del').dataset.src_url)
	else
		for (const i of display_panel.visibleItems)
			src_list_del(i.elm.querySelector('.src_list_del').dataset.src_url)
	on_tag_sel()
}
µ.id('mp_v_src_sel_all').onclick = () => {
	for (const src_key of source_keys)
		src_list_add(src_key)
	on_tag_sel()
}
µ.id('mp_v_src_sel_page').onclick = () => {
	for (const i of display_panel.visibleItems)  // .src_list_del always exists
		src_list_add(i.elm.querySelector('.src_list_del').dataset.src_url)
	on_tag_sel()
}
let search_in_src_callback_timeout  // search in list
µ.id('mp_v_src_search').onkeyup = search_v_src
µ.id('mp_v_src_search').onclick = search_v_src
/**
 * Search in the display src panel.
 * @param {Event} evt the event fired
 */
function search_v_src(evt) {
	clearTimeout(search_in_src_callback_timeout)
	search_in_src_callback_timeout = setTimeout(() => {
		// bug in listjs 2.3.1 with dots in keywords to we replace them with spaces
		display_panel.search(_.triw(evt.target.value).replace('.', ' '), [
			'src_list_nom',
			'src_list_lang',
			'src_list_country',
			'src_list_type',
			'src_list_result_type',
			'src_list_only'
		])
	}, evt.keyCode === 13 ? 0 : 500)
}
µ.id('mp_clear_v_src_search').onclick = () => {  // Clear the source list search bar
	µ.id('mp_v_src_search').value = ''
	display_panel.search()
}
µ.id('mp_clear_query').onclick = () => {
	µ.id('mp_query').value = ''
}
async function init() {
	MAX_RES_BY_SRC = await mµ.get_max_res_by_src()
	if (TEST_SRC) {
		let src_names = QUERYSTRING.getAll('name')
		await update_source_objs(src_names, await mµ.get_built_src(), false)
	} else {
		HEADLINE_PAGE_SIZE = await mµ.get_news_page_size()
		userLang = await mµ.get_wanted_locale()
		mp_i18n = await g.gettext_html_auto(userLang) // must be before populate_tags
		µ.set_HTML_lang(userLang)
		LANG_NAME = new Intl.DisplayNames([userLang], {type: 'language'})
		COUNTRY_NAME = new Intl.DisplayNames([userLang], {type: 'region'})
		ALT_LOAD_TXT = mp_i18n.gettext('Click to load image')
		IS_LOAD_IMG = await mµ.get_load_photos()
		manifest = await mµ.get_manifest()
		display_panel = new List('list_src_t', {
			item: 'src_item_tpl',
			valueNames: [
				{ name: 'f_icon', attr: 'src' },
				{ name: 'f_icon_title', attr: 'title' },
				'src_list_nom',
				'src_list_country',
				'src_list_lang',
				'src_list_lang_name',
				'src_list_type',
				'src_list_result_type',
				'black',
				{ name: 'src_list_url', attr: 'href' },
				{ name: 'src_list_edit', attr: 'href' },
				{ name: 'src_list_del', attr: 'data-src_url' },
				{ name: 'src_list_add', attr: 'data-src_url' },
				{ name: 'src_list_only', attr: 'data-src_url' }
			],
			page: 20,
			pagination: {
				outerWindow: 3,
				innerWindow: 3,
				item: '<li><a class="page" href="#list_src_t"></a></li>'
			},
		})
		let ld_src_dt_start = new Date()
		await update_source_objs('', await mµ.get_built_src(), false) // Calls populate_tags
		console.info('Meta-Press.es : loading source', new Date() - ld_src_dt_start, 'ms')
		if (QUERYSTRING.get('xgettext'))
			await g.xgettext_html()  // is after populate_tags (must be called from english)
		document.onvisibilitychange = async() =>	{  // Update sources if tab becomes visible again
			if (document.visibilityState !== 'visible') return
			if (!await mµ.is_need_reload_src(dt_source_objs)) return
			update_source_objs('', mµ.get_built_src(), false, true)
			// let tab_id = await browser.tabs.getCurrent().then(e => e.id)
			let tab_id = (await browser.tabs.getCurrent()).id
			mµ.to_storage(`filters_${tab_id}`, tags_to_JSON())
		}
		display_panel.on('updated', update_display_panel)
		if(await mµ.get_sentence_search())
			µ.id('mp_query').addEventListener('input', () => {
				let query = µ.id('mp_query').value
				let mw_query = new RegExp('[^ ]\\s\\s*[^ ]')
				if(mw_query.test(query))
					toggle_sentence_search_tags('one word', 'many words')
				else
					toggle_sentence_search_tags ('many words', 'one word')
			})
		const do_manage_mp_input_focus = false
		if (do_manage_mp_input_focus)  // removed to allow kbd nav downward
			manage_mp_input_focus()  // must be after populate_tags
		if(QUERYSTRING.get('id_sch_s') && !QUERYSTRING.get('submit')) {
			µ.id('add_auto_s_text').textContent = mp_i18n.gettext('save modification')
			µ.id('add_auto_s_new').style.display = 'none'
			µ.id('add_auto_s_save').style.display = 'inline-block'
		}
		set_query_placeholder()
		µ.id('mp_version').textContent = 'v' + manifest.version
		µ.dropdown_alive()
		// xµ.check_current_storage_health()  // was only needed for sync storage
		// sµ.update_favicon_rss(source_objs)
	}
	if (QUERYSTRING.get('submit'))
		µ.id('mp_submit').click()
	else {
		// resolved_search_hosts = await mµ.resolved_search_hosts()
		dns_prefetch(current_source_selection)
	}
	document.body.classList.add('javascript_loaded')
}
init()
