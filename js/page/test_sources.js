// name		: test_sources.js
// SPDX-FileCopyrightText: 2021-2022 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals List */
import * as _			from '../js_utils.js'
import * as µ			from '../BOM_utils.js'
import * as mµ		from '../mp_utils.js'
import * as sµ		from '../source_utils.js'
import * as st		 from '../source_testing.js'

const DEFAULT_SEARCH_TERMS_TAB_NB = 4
// const POPUP_MAXIMUM = µ.id('mp_display_result_test').dataset.pool
const INTER_POPUP_DELAY = 1000
const DEFAULT_SEARCH_TERMS = {
	// 'one word': ['Esperanto', 'Biden', 'alternatiba', 'Quadrature du Net'],
	'one word': ['europe'],
	// 'many words': ['Quadrature du net', 'Watergate', 'Gilets Jaunes', 'Jaunes Gilets'],
	'many words': ['europe'],
	'approx': ['europe']
}
const RES_FRAMES = [ 'test_integrity', 'test_src']
let date_before_test_src, date_after_test_src
let source_objs

/**
 * Returns the default search terms depending on tech tags : "many words", "one word", "approx"…
 * @param {Array} the tech tags of a source
 * @returns {Array} the terms
 */
function get_default_terms(src_tech_tags) {
	if(src_tech_tags.includes('many words')) {
		return DEFAULT_SEARCH_TERMS['many words']
	} else if (src_tech_tags.includes('one word')) {
		return DEFAULT_SEARCH_TERMS['one word']
	} else {
		return DEFAULT_SEARCH_TERMS.approx
	}
}
function reset_all_src_for_query() {
	let all_src_for_query = {}
	for(const v of Object.values(DEFAULT_SEARCH_TERMS))
		for(let j of v)
			if(!Object.keys(all_src_for_query).includes(j.toLowerCase()))
				all_src_for_query[j.toLowerCase()] = {not_rdy: [], rdy: []}
	return all_src_for_query
}
/**
 * Test (integrity, unity and JSON diff) for the given sources.
 * @param {Object} source_objs The sources
 */
function before_test_sources(source_objs) {
	let elt = µ.id('mp_display_result_test')
	date_before_test_src = new Date().getTime()
	mp_test.testList.clear()
	let all_src_for_query = reset_all_src_for_query()
	for(let [tested_src_k, tested_src_v] of Object.entries(source_objs)) {
		let src_tested_name = tested_src_v.tags.name
		let src_tested_favicon = tested_src_v.favicon_url
		get_all_search_terms(tested_src_k, tested_src_v, all_src_for_query)
		mp_test.testList.add({
			f_h1: src_tested_name,
			src_notes: tested_src_v.tags.notes || '',
			src_id: `report_${tested_src_k}`,
			f_icon: src_tested_favicon,
			f_name: tested_src_k,
			src_key: tested_src_k,
			name_src: src_tested_name
		})
		let src_test_btn = µ.id(`btn_${tested_src_k}`)
		if(!src_test_btn) {
			src_test_btn = document.createElement('button')
			src_test_btn.id = `btn_${tested_src_k}`
			src_test_btn.title = src_tested_name
			src_test_btn.addEventListener('click', () => {
				µ.id('mp_filter_report').value = src_tested_name
				µ.id('mp_filter_report').dispatchEvent(new Event('input'))
			})
			elt.append(src_test_btn)
		}
		src_test_btn.classList.add('btn_test', 'default')
		src_test_btn.textContent = ' '
		let report_div = µ.id(`report_${tested_src_k}`)
		report_div.getElementsByClassName('btn_retest')[0].onclick = () => {
			for(let i of µ.$$(`[id='report_${tested_src_k}'] [data-query]`))
				open_query_tab([tested_src_k], i.dataset.query)
		}
		for (let i of RES_FRAMES) {
			report_div.getElementsByClassName(`${i}_btn`)[0].onclick = () => {
				report_div.getElementsByClassName(`${i}`)[0].classList.toggle('display_none')
				report_div.getElementsByClassName(`${i}_btn`)[0].getElementsByClassName(
					'fld_ico')[0].classList.toggle('fld_ico_hide')
			}
		}
	}
	test_sources(all_src_for_query, source_objs)
}
function test_stats() {
	µ.id('test_volume').textContent = µ.$$('.btn_test').length
	µ.id('mp_test_results').style.display = 'inline'
	// test_stats_ticker()
}
export function test_stats_ticker() {  // not used anywhere else
	update_test_stats()
	if(µ.$$('button.default').length > 0)
		setTimeout(test_stats_ticker, INTER_POPUP_DELAY)
}
function update_test_stats() {
	µ.id('mp_clear_filter_report').click()
	for (let i of RES_FRAMES) {
		let warning = µ.$$(`.${i} .warn`).length
		let error = µ.$$(`.${i} .error`).length
		let txt = ''
		if(i === 'test_src'){
			let success = µ.$$(`.${i} a.success`).length
			let net_err = µ.$$(`.${i} a.network_error`).length
			txt += `OK ${success}, net err ${net_err}, `
		}
		txt += `warn ${warning}, err ${error}`
		µ.id(i).textContent = txt
	}
	date_after_test_src = new Date().getTime()
	µ.id('test_duration').textContent = (date_after_test_src - date_before_test_src) / 1000
}
/**
 *
 */ /*
async function retest_sources() {
	µ.id('mp_test_results').style.display = 'none'
	mp_test.testList.filter()
	let all_src_for_query = reset_all_src_for_query()
	let err_src = µ.$$('.test_src[class*=error_], .test_src.default_, .test_src.warn_')
	// also matches .test_src.no_result_error_
	for(let err_src_tested of err_src) {
		let src_key_to_retest = err_src_tested.dataset.src_key
		get_all_search_terms(src_key_to_retest, source_objs[src_key_to_retest], all_src_for_query)
		st.edit_style_class(err_src_tested, 'default')
		err_src_tested.textContent = ''
		let btn_mat = µ.id(`btn_${src_key_to_retest}`)
		btn_mat.textContent = 'O'
		st.edit_style_class(btn_mat, 'default')
		for (let i of RES_FRAMES)
			st.edit_style_class(µ.$(`[id='report_${src_key_to_retest}'] .${i}_btn`), 'default')
	}
	sµ.clear_reserved_names()
	test_sources(all_src_for_query, source_objs)
	test_stats()
}*/
function test_sources(all_src_for_query, source_objs){
	µ.id('test_queries').textContent = Object.keys(all_src_for_query).length
	for(const v of Object.values(all_src_for_query)) {
		let clone_all = _.deep_copy(v.not_rdy)
		let cur_src
		for(let tested_src of clone_all) {
			cur_src = source_objs[tested_src]
			sµ.test_src_integrity(source_objs, tested_src, (ready_to_test) => {
				let src_tags_tech = cur_src.tags.tech
				let terms = cur_src.positive_test_search_term || get_default_terms(src_tags_tech)
				for (const i of ready_to_test[1])
					st.add_err(cur_src, 0, i.text, i.src_key, i.level, 'test_integrity')
				for(let query of terms){
					query = query.toLowerCase()
					_.array_remove_once(all_src_for_query[query].not_rdy, tested_src)
					if (ready_to_test[0])
						all_src_for_query[query].rdy.push(tested_src)
					if(all_src_for_query[query].not_rdy.length === 0) {
						const src_for_query = all_src_for_query[query].rdy
						const src_for_query_nb = src_for_query.length
						const slice_size = src_for_query_nb / DEFAULT_SEARCH_TERMS_TAB_NB
						let slice_end = slice_size
						if (src_for_query_nb > 400) {
							for (let s = 0; s < src_for_query_nb - (slice_size / 2); s += slice_size) {
								open_query_tab(src_for_query.slice(s, slice_end), query)
								slice_end += slice_size
							}
						} else {
							open_query_tab(all_src_for_query[query].rdy, query)
						}
					}
				}
			})
		}
	}
}
function open_query_tab(tested_srcs, tested_src_query) {
	let local_url = new URL(window.location)
	let elt = µ.id('mp_display_result_test')
	let pool = parseInt(elt.dataset.pool)
	if(pool > 0) {
		elt.dataset.pool = pool - 1
		let new_url = new URL(local_url.origin + '/html/index.html')
		new_url.searchParams.set('q', tested_src_query)
		for(let src_key of tested_srcs){
			let a = µ.$(`[id='report_${src_key}'] .test_src`)
			st.add_result_test_to_query(source_objs[src_key], a, '', tested_src_query, 'O', 'default')
			// new_url.searchParams.set('tech', 'cherry-pick_sources')
			// new_url.searchParams.append('add_src_sel', src_key)
			new_url.searchParams.append('name', source_objs[src_key].tags.name)
		}
		new_url.searchParams.set('submit', 1)
		new_url.searchParams.delete('launch_test')
		new_url.searchParams.set('test_sources', 1)
		window.open(new_url.toString())
	} else {
		setTimeout(function () {open_query_tab(tested_srcs, tested_src_query)}, INTER_POPUP_DELAY)
	}
}
const mp_test = {
	testList: new List('mp_display_test', {
		item: 'test-item',
		valueNames: ['f_h1','src_notes',
			{ name: 'src_id', attr: 'id' },
			{ name: 'f_icon', attr: 'src' },
			{ name: 'f_name', attr: 'title'},
			{ name: 'src_key', attr: 'data-src_key'},
			{ name: 'name_src', attr: 'data-name_src'},
			{ name: 'toggle_fct', attr:'script'}
		]
	})
}
µ.id('mp_search_filter_report').addEventListener('click', () => filter_src_test(true))
µ.id('mp_filter_report').addEventListener('input', () => filter_src_test(false))
µ.id('mp_clear_filter_report').addEventListener('click', () => {
	µ.id('mp_filter_report').value = ''
	mp_test.testList.filter()
})
function filter_src_test(strict_equal) {
	let val = µ.id('mp_filter_report').value
	val = _.triw(val).toLowerCase()
	if(strict_equal && val !== '')
		mp_test.testList.filter(i => i.values().f_h1.toLowerCase() === val)
	else {
		mp_test.testList.filter(i => i.values().f_h1.toLowerCase().includes(val))
	}
}
function get_all_search_terms (src_key, src, all_src_for_query) {
	const src_positive_sch_terms = src.positive_test_search_term
	const src_tags_tech = src.tags.tech
	let src_tested_query
	if (_.is(src_positive_sch_terms)) {
		if (Array.isArray(src_positive_sch_terms)) {
			src_tested_query = src_positive_sch_terms
			for (let v of src_tested_query)
				if (!all_src_for_query[v.toLowerCase()])
					all_src_for_query[v.toLowerCase()] = {not_rdy: [], rdy: []}
		} else console.warn(`${src_key} definition of positive_test_search_terms should be a list`)
	} else src_tested_query = get_default_terms(src_tags_tech)
	for (let v of Object.values(src_tested_query))
		all_src_for_query[v.toLowerCase()].not_rdy.push(src_key)
}
async function init() {
	await mµ.set_theme()
	source_objs = await mµ.get_built_src()
	const src_keys = Object.keys(source_objs)
	µ.id('mp_start_test').addEventListener('click', async(evt) => {
		evt.target.style.display = 'none'
		µ.id('mp_display_report_test').style.display = 'inline-block'
		if(await mµ.request_sources_perm(src_keys, source_objs)) {
			sµ.clear_reserved_names()
			before_test_sources(source_objs)
			test_stats()
		}
	})
	const NB_TESTS = 10
	µ.id('mp_start_test_nb').addEventListener('click', async(evt) => {
		µ.id('mp_display_report_test').style.display = 'inline-block'
		let src_test_nb = 0
		let need_test = Math.min(src_test_nb + NB_TESTS, src_keys.length)
		let src_test = {}
		for (let i = src_test_nb; i < need_test; i++) {
			src_test[src_keys[i]] = source_objs[src_keys[i]]
		}
		evt.target.style.display = 'none'
		if(await mµ.request_sources_perm(src_keys, source_objs)) {
			before_test_sources(src_test)
			test_stats()
		}
		evt.target.style.display = 'inline-block'
		src_test_nb += NB_TESTS
		if (src_test_nb >= src_keys.length) {
			sµ.clear_reserved_names()
			src_test_nb = 0
		}
	})
	µ.id('update_stats').addEventListener('click', () => {
		update_test_stats()
	})
	document.body.classList.add('javascript_loaded')
}
init()
