// SPDX-FileName: ./BOM_utils.js
// SPDX-FileCopyrightText: 2017-2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

export const	$ = (str) => document.querySelector(str)
export const $$ = (str) => document.querySelectorAll(str)
export const id = (str) => document.getElementById(str)
/**
 * Redirects console printing to an alert displayed on demand
 * Used in mobile debugging
 */
export function alert_console() {
	let log = ''
	function make_mp_print (name, nat_obj) {	// eslint-disable-line no-inner-declarations
		return function () {
			log += `${name} : `
			nat_obj.apply(console, arguments)
			for (let i=0; i < arguments.length; i++)
				log += ' '+ String(arguments[i])
			log += '\n'
		}
	}
	console.info = make_mp_print('Info', console.info)
	console.log = make_mp_print('Log', console.log)
	console.warn = make_mp_print('Warning', console.warn)
	console.error = make_mp_print('Error', console.error)
	console.trace = make_mp_print('Trace', console.trace)
	let btn = id('mp_dev')
	btn.style.display = 'block'
	btn.addEventListener('click', () => alert(log))
}
/**
 * Decode the HTML entities from the given string to their UTF8 counterparts
 * More info : https://stackoverflow.com/questions/3700326/decode-amp-back-to-in-javascript
 * @example htmlDecode("&lt;img src='myimage.jpg'&gt;") returns "<img src='myimage.jpg'>"
 * @param {string} s The string to decode
 * @returns {string} The decoded string
 */
export function HTML_decode_entities (s) {
	let elt = document.createElement('textarea')
	elt.innerHTML = s
	return elt.value || ''
}
/**
 * Encode the given XML string with UTF8 entities
 * More info : https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
 * @param {string} s String to encode
 * @example XML_encode_UTF8 (" « ") returns " &#171; "
 * @returns {string} The encoded string
 */
export function XML_encode_UTF8 (s) {
	return s.replace(/[\u00A0-\u9999]/g, (i) => `&#${i.charCodeAt(0)};`)
}
/**
 * Encode the given HTML string with UTF8 entities
 * Same as XML_encode_UTF8 but with '&' and '"' encoded also
 * @param {string} s The string to encode
 * @returns {string} The encoded s
 */
export function HTML_encode_UTF8 (s) {
	return XML_encode_UTF8(s).replace(/[&"]/g, (i) => `&#${i.charCodeAt(0)};`)
}
// https://stackoverflow.com/questions/50840168/how-to-detect-if-the-os-is-in-dark-mode-in-browsers
/**
 * Check if the browser is in dark mode.
 * @returns {boolean} dark mode
 */
export function isDarkMode() {
	return window.matchMedia('(prefers-color-scheme: dark)').matches
}
/**
 * Upload the data in a filename and submit to the user.
 * @param {string} file_name The filename
 * @param {string} str The data to upload
 */
export function upload_file_to_user(file_name, str) {
	let a = document.createElement('a')
	a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(str)}`
	a.download=file_name
	a.click()
}
/**
 * Return all the months name of the given locale in the given format
 * From https://stackoverflow.com/a/70869819
 * @param {object} with properties : locale (fr, en, ja…) ; format (short, long, numeric…)
 * @returns {Array} an array containing the 12 months names
 */
export function get_months(locale = navigator.language, format = 'long') {
	const locale_format = new Intl.DateTimeFormat(locale, {month: format}).format
	return [...Array(12).keys()].map((m) => locale_format(new Date(2021, m)))
}
/**
 * Lower case, normalize and remove diacritics from the given string
 * https://stackoverflow.com/a/51874002
 */
function norm_str (str) {
	return str.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f.]/g, '')
}
/**
 *
 */
function string_index_in_array(str, arr) {
	return arr.findIndex(a => norm_str(a).startsWith(norm_str(str)))
	// return arr.findIndex(a => norm_str(a).search(norm_str(str)) > 0)
}
/**
 * Return the month number of the given month name from the given language
 * @param {string} a locale (fr, en, ja…)
 * @param {string} a month name in the given language (février, january, 5月)
 * @returns {number} the corresponding number (2, 1, 5) or the given month_name if not matched
 */
export function lang_month_nb(lang, month_name) {
	let a = string_index_in_array(month_name, get_months(lang)) + 1  // months start at 0 in JS
	if (a > 0) return a  // nl maart is abbr-ed in mrt. so :
	a = string_index_in_array(month_name, get_months(lang, 'short')) + 1
	if (a > 0) return a
	return month_name
}
/**
 * Count the number of browser supported locales as per ISO 639-2 2-letters codes
 * 2022-05-12 : 218 in Firefox 102.0a1
 */
export function locales() {
	return supported_ISO_639_2_language_locales()
}
export function supported_ISO_639_2_language_locales() {
	let a = new Set()
	let b = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
		'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let b_ = ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
		'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let r
	for (let c of b)
		for (let d of b)
			for (let e of b_) {
				r = Intl.DateTimeFormat.supportedLocalesOf([c+d+e])
				r.length && a.add(r[0])
			}
	return Array.from(a).sort()
}
function supported_xx_xx_country_locales() {	// eslint-disable-line no-unused-vars
	let a = new Set()
	let b = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
		'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let r
	for (let c of b)
		for (let d of b)
			for (let e of b)
				for (let f of b) {
					r = Intl.DateTimeFormat.supportedLocalesOf([`${c+d}-${e+f}`])
					r.length && a.add(r[0])
				}
	return Array.from(a).sort()
}
export function known_countries_and_code(lang) {
	let a = {}
	let b = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
		'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let b_ = ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
		'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let k
	let x = new Intl.DisplayNames([lang], {type: 'region'})
	for (let c of b)
		for (let d of b)
			for (let e of b_) {
				k = c+d+e
				try {
					a[k] = x.of([k])
				} catch (err) {
					continue
				}
			}
	// return Array.from(a).sort()
	return a
}
// const valid_HTTP_protocols = ['https:', 'http:', 'data:', 'file:']
/**
 * Tests if a given string is a valid URL
 * @param {string} str The string to test
 * @returns {boolean} URL validity
 */
/*export function is_valid_HTTP_URL(str) {	// inspired from https://stackoverflow.com/a/43467144
	let url
	try { url = new URL(str) } catch (_) { return false }
	return valid_HTTP_protocols.includes(url.protocol)
}*/  // match on http://a
export function is_valid_HTTP_URL(str) {
	return valid_HTTP_URL_RE.test(str)
}
/**
 * Tranform the link in a url link for domain part.
 * @param {string} link The link to transform
 * @param {string} domain_part The doamin part of the destination
 * @returns {string} The working url
 */
export function urlify(link, domain_part) { // return URL from any href (even relative links)
	let url
	if (!link || link.startsWith('http')) // and we are very happy to test implicitely many cases
		return link
	else if (link.startsWith('file:') || link.startsWith('data:'))	// remove MP auto-added path
		url = link.replace(document.URL.split('/').slice(0,-1).join('/'), domain_part)
	else if (link.startsWith('//'))
		url = (domain_part.split('/'))[0] + link
	else
		url = domain_part + (link.startsWith('/') ? '' : '/') + link
	return is_valid_HTTP_URL(url) ? url : null
}
/**
 * String to define a regular expression test the validity of a URL
 * https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
 */
const valid_HTTP_URL_str = 'https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{2,7}\\b([-a-zA-Z0-9()@:%_\\+.~#?&\\/\\/=]*)' // eslint-disable-line max-len
/**
 * Regular expression to test the validity of a URL
 */
const valid_HTTP_URL_RE = new RegExp(valid_HTTP_URL_str)
/**
 * Find the favicon with the given HTML fragment and domain.
 * @param {Node} html_fragment The working area
 * @param {string} domain_part The domain of the website
 * @returns The favicon url
 */
export function get_favicon_URL(HTML_fragment, domain_part) { // favicon may be implicit
	let fav_node = HTML_fragment.querySelector('link[rel~="icon"]')
	return urlify(fav_node && fav_node.attributes.href.value || '/favicon.ico', domain_part)
}
/*
 *
 */
export function get_HTML_lang(HTML_fragment) {  // lang could be find in
	// or attributed <html … lang= -> split(/[-_]/)
	// <meta http-equiv="Content-language" content="fr">
	// <meta property="og:locale" content="fr_FR">
	let lang = HTML_fragment.querySelector('html').lang
	if (!lang) {
		let node = HTML_fragment.querySelector('meta[http-equiv="Content-Language"]') ||
			HTML_fragment.querySelector('meta[property="og:locale"]')
		lang = node.content || ''
	}
	let lang_split = lang.split(/[_-]/)
	return [lang_split[0], lang_split[1]]
}
/**
 * Find the RSS feed of a given HTML fragment and domain.
 * @param {Node} html_fragment The working area
 * @param {string} domain_part The domain of the website
 * @returns The favicon url
 */
export function get_RSS_URL(html_fragment, domain_part) { // favicon may be implicit
	let rss = html_fragment.querySelector('link[rel~="alternate"][type="application/rss+xml"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	rss = html_fragment.querySelector('a[data-smarttag-name="rss"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	rss = html_fragment.querySelector('a[aria-label="RSS"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	rss = html_fragment.querySelector('a[href*="feed"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	rss = html_fragment.querySelector('a[href*="rss"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	for (let a of html_fragment.querySelectorAll('a')) {
		if (a.textContent.includes('feed') ||
			a.textContent.includes('rss') ||
			a.textContent.includes('RSS')
		)
			return urlify(rss.attributes.href.value, domain_part)
	}
	if (!rss)
		console.log('RSS not found', domain_part, html_fragment)
	return rss && urlify(rss.attributes.href.value, domain_part) || ''
}
/**
 * An arbitrary french internationalisation number format. What's important is to use the same
 * anywhere. Meta-Press.es has been created by a french guy.
 * @type {Intl.NumberFormat}
 */
const intlNum = Intl.NumberFormat('fr', {
	minimumIntegerDigits: 4,
	useGrouping: 0,
	signDisplay: 'always'})
/**
 * Returns the given date transformed regarding to the navigator timezone.
 * If nav_to_tz is true the timezone offset is added, else it is removed
 * else returns the date from the given timezone
 * @param {string} dt_str The date
 * @param {string} tz The timezone
 * @param {boolean} nav_to_tz The direction of the conversion
 * @returns {Date} the date converted
 */
export function timezoned_date (dt_str, tz='UTC', nav_to_tz=false, dbg=false) {
	const dt = dt_str ? new Date(dt_str) : new Date()
	dbg && console.log('dt_str', dt_str, 'dt', dt, 'tz', tz)
	if (isNaN(dt)) return NaN
	if (tz === 'UTC' || tz === 'GMT') return dt
	const dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000)
	const dt_UTC	= new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000)
	const parsed_tz = parseInt(tz)
	let int_offset
	if (typeof(parsed_tz) === 'number' && !isNaN(parsed_tz)) {
		int_offset = parsed_tz
	} else {
		const dt_repr = dt_UTC.toLocaleTimeString('fr', {timeZoneName: 'short', timeZone: tz})
		dbg && console.log('dt_repr', dt_repr)
		int_offset = parseInt((dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.') * 100))
	}
	dbg && console.log('dt_orig', dt_orig, 'dt_UTC', dt_UTC)
	let date_tz
	if (nav_to_tz) {
		date_tz = dt_UTC
		int_offset = -int_offset
	} else {
		date_tz = dt_orig
	}
	dbg && console.log('date_tz', date_tz, 'int_offset', int_offset)
	const tz_offset = intlNum.format(int_offset)
	dbg && console.log('tz_offset', tz_offset)
	return new Date(date_tz.toISOString().replace(/\.\d{3}Z/, tz_offset))
}
/**
 * Return the content type without 'rss+' or 'atom+' part
 * @function
 * @param {string} str string to clean (supposed to be an HTTP content type)
 * @returns {string}
 */
export function clean_c_type(str) { return str.split(';')[0].replace(/(rss|atom)\+/, '') }
/**
 * await for this function to wait a given number of milliseconds.
 * @function
 * @param {number} duration The sleep duration in milliseconds
 * @returns {Promise}
 */
export async function sleep(duration) {
	return new Promise(resolve => setTimeout(resolve, duration))
}
/**
 * Return a namespace URL from a given prefix. Contains all the namespaces we found yet.
 * Extended version of : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_using_XPath_in_JavaScript#Implementing_a_User_Defined_Namespace_Resolver
 * @param {string} prefix The prefix
 * @returns {string} the corresponding URL of the intended prefix norm
 */
function NS_resolver(prefix) {
	let NS = {
		'xhtml' : 'http://www.w3.org/1999/xhtml',
		'mathml': 'http://www.w3.org/1998/Math/MathML',
		'content':'http://purl.org/rss/1.0/modules/content/',
		'wfw': 'http://wellformedweb.org/CommentAPI/',
		'dc': 'http://purl.org/dc/elements/1.1/',
		'atom': 'http://www.w3.org/2005/Atom',
		'sy': 'http://purl.org/rss/1.0/modules/syndication/',
		'slash': 'http://purl.org/rss/1.0/modules/slash/',
		'media': 'http://search.yahoo.com/mrss/',
		'image': 'http://www.google.com/schemas/sitemap-image/1.1'
	}
	return NS[prefix] || null
}
// Evaluate an XPath expression aExpression against a given DOM node
// or Document object (aNode), returning the results as an array
// thanks wanderingstan at morethanwarm dot mail dot com for the
// initial work.
/**
 * The XPath evaluator used by {@link evaluateXPath}.
 * @constant
 * @type {XPathEvaluator}
 */
const xpe = new XPathEvaluator()
/* let no_namespace = false
 if (no_namespace) {
	NS_resolver = xpe.createNSResolver(aNode.ownerDocument == null ?
		aNode.documentElement : aNode.ownerDocument.documentElement)
} else {
*/
/**
 * Search the XPath expression path in a node (querySelectorAll for XPath).
 * @param {Node} node The node working area
 * @param {string} XPath_expression The xpath expression
 * @returns {Array} All the found matchs
 */
export function evaluateXPath(node, XPath_expression) {
	let result = xpe.evaluate(XPath_expression, node, NS_resolver, 0, null)
	let found = [], res
	while (res = result.iterateNext()) found.push(res)
	if (found.length === 1) found = found[0]
	return found
}
/**
 * Extract an HTML tag attribute attr from a string str supposed to contain HTML.
 * @param {string} attr Attribute to extract
 * @param {string} str The string to search in
 * @returns {string} The value of the 1st attribute
 */
/*export function extract_val_attr (attr, str) {
	let s = regextract(` ${attr}=["'](.*?)["']`, str)
	return (s === str ? '' : s)
}*/
/**
 * The string to build a regular expression to extract hour:minutes from a date.
 * @type {string}
 */
export const HH_MM_STR = ' (\\d\\d?:\\d\\d)'
/**
 * The regular expression allowing to extract hour:minutes from a date.
 * @type {RegExp}
 */
export const HH_MM_RE = new RegExp(HH_MM_STR, 'i')
/**
 * The regex to extract a string with just hour:minutes.
 * @type {RegExp}
 */
export const HH_MM_ONLY_RE = new RegExp(`^${HH_MM_STR}$`, 'i')
/**
 * The string to build a regular expression to extract -month-day in a date.
 * @type {string}
 */
export const MM_DD_ONLY_STR = '^-\\d{1,2}-\\d{1,2}'
/**
 * The regular expression to extract -month-day in a date.
 * @type {RegExp}
 */
export const MM_DD_ONLY_RE = new RegExp(MM_DD_ONLY_STR, 'i')
/**
 * Get the selected value in the given HTML select object
 * @param {Object} sel The HTML select object
 * @returns The current selected value
 */
export function get_HTML_select_value(sel) { return sel.options[sel.selectedIndex].value }
/**
 * Set the language in the HTML document.
 * @param {string} lg The language
 */
export function set_HTML_lang(lg) { document.body.parentElement.lang = lg }
//export const intl_lang_name = (lg) => new Intl.DisplayNames([lg], {type: 'language'}).of(lg)
/**
 * Remove an anchor from a given URL.
 * @example  rm_href_anchor('olivier.fr#tree') return 'olivier.fr'
 * @param {URL} permalink The link
 * @returns The link without the anchor
 */
export function rm_href_anchor(permalink) { return permalink.href.split('#')[0] }
/**
 * Check if the given HTML element is in overflow status
 * @param {Node} elt The HTML element to check
 * @returns {boolean} overflow ?
 */
export function isOverflown(elt) {
	return elt.scrollHeight > elt.clientHeight || elt.scrollWidth > elt.clientWidth
}
/**
 * Creates a style link with href or a style node.
 * https://stackoverflow.com/a/524798
 * @param {string} href The stylesheet to reach if undefined create style
 * @returns {StyleSheet} The created or loaded stylesheet
 */
export function createStyleSheet(href) {
	let elt
	if(typeof href !== 'undefined') {
		elt = document.createElement('link')
		elt.type = 'text/css'
		elt.rel = 'stylesheet'
		elt.href = href
	} else elt = document.createElement('style')
	// elt['generated'] = true
	document.getElementsByTagName('head')[0].appendChild(elt)
	let sheet = document.styleSheets[document.styleSheets.length - 1]
	// if(typeof sheet.addRule === 'undefined') // we're not targeting browsers without addRule()
	//	sheet.addRule = addRule
	if(typeof sheet.removeRule === 'undefined')
		sheet.removeRule = sheet.deleteRule
	return sheet
}
/**
 * Demand a file from the user and pass it to a hoop treatment function
 * @param {Function} treatment_hook Given function to act on the file content
 */
export function read_user_file(treatment_hook) {
	let input_elt = document.createElement('input')
	input_elt.type = 'file'
	input_elt.click()
	input_elt.addEventListener('change', () => {
		let file = input_elt.files[0]
		if (file) {
			let reader = new FileReader()
			reader.readAsText(file, 'UTF-8')
			reader.onload = treatment_hook
			reader.onerror = () => console.error('error importing user file')
		}
	}, false)
}
/**
 * Activate dropdown buttons constructs (.drop_btn / .drop_down_div)
 */
export function dropdown_alive() {
	let menus = document.querySelectorAll('.drop_btn')
	for (const i of menus) {
		i.onclick = evt => {
			let elt = evt.target
			elt.textContent = elt.classList.contains('drop_active') ? ' + ' : ' - '
			elt.classList.toggle('drop_active')
			elt.parentNode.querySelector('.drop_down_div').classList.toggle('drop_display')
		}
	}
}
