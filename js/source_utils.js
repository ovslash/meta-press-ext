// SPDX-FileName: ./source_utils.js
// SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import * as _ from './js_utils.js'
import * as µ from './BOM_utils.js'
import * as mµ from './mp_utils.js'

/**
 * @module source_utils
 */
/**
 * Source definitions loading / building / reloading…
 * @namespace source_loading
 */
/**
 * Source definition for various XML-based formats (lik RSS or ATOM).
 * @memberof source_loading
 * @type {Object}
 */
export const XML_SRC = {
	'RSS': {	// filters: where we store the source filters in exported XML files
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		// 'r_img': 'NOT MANAGED',
		'r_by_xpath': './dc:creator',
		'docs': 'https://www.w3.org/TR/selectors-3/#attrnmsp'
	},
	'RSS_content:encoded': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt_xpath': './content:encoded',
		'r_by_xpath': './dc:creator'
	},
	'RSS_dc:date': {	// basta.media
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt_xpath': './dc:date',
		'r_txt': 'description'
	},
	'RSS_enclosure': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		'r_by_xpath': './dc:creator',
		'r_img_src': 'enclosure',
		'r_img_src_attr': 'url'
	},
	'RSS_media:content': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		'r_by_xpath': './dc:creator',
		'r_img_src_xpath': './media:content/@url',
		'r_img_title_xpath': './edia:content/media:title'
	},
	'RSS_image:image': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		'r_by_xpath': './dc:creator',
		'r_img_src_xpath': './image:image/image:loc',
		'r_img_title_xpath': './image:image/image:title',
		'r_img_alt_xpath': './image:image/image:caption'
	},
	'RSS_media:thumbnail': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		'r_by_xpath': './dc:creator',
		'r_img_src_xpath': './media:thumbnail',
		'r_img_src_attr': 'url'
	},
	'ATOM': {
		'filters': 'category',
		'results': 'entry',
		'r_h1': 'title',
		'r_url': 'link',
		'r_url_attr': 'href',
		'r_dt': 'published',
		'r_txt': 'summary',
		'r_by': 'author'
	},
	'ATOM_content': {
		'filters': 'category',
		'results': 'entry',
		'r_h1': 'title',
		'r_url': 'link',
		'r_url_attr': 'href',
		'r_dt': 'published',
		'r_txt': 'content',
		'r_by': 'author'
	}
}
export function find_RSS_variant(items, n) { /* eslint-disable dot-notation */
	let j = 0
	let src_def = {}
	const light_n = {tags: n.tags}
	const silent = true
	const do_txt = true
	for (let i of items) {
		src_def = extend_source(XML_SRC['RSS_image:image'], light_n)
		if (mµ.$_('r_img_title',i, src_def, j, do_txt, silent)) return 'RSS_image:image'
		src_def = extend_source(XML_SRC['RSS_enclosure'], light_n)
		if (mµ.$_('r_img_src',	i, src_def, j, do_txt, silent)) return 'RSS_enclosure'
		src_def = extend_source(XML_SRC['RSS_media:thumbnail'], light_n)
		if (mµ.$_('r_img_src',	i, src_def, j, do_txt, silent)) return 'RSS_media:thumbnail'
		src_def = extend_source(XML_SRC['RSS_media:content'], light_n)
		if (mµ.$_('r_img_src',	i, src_def, j, do_txt, silent)) return 'RSS_media:content'
		if (i.innerHTML.includes('img') && i.innerHTML.includes('src'))
			console.warn('XML from ', n.tags.name, 'might contain undetected img', i)
		src_def = extend_source(XML_SRC['RSS_dc:date'], light_n)
		if (mµ.$_('r_dt',				i, src_def, j, do_txt, silent)) return 'RSS_dc:date'
		src_def = extend_source(XML_SRC['RSS_content:encoded'], light_n)
		if (mµ.$_('r_txt',			i, src_def, j, do_txt, silent)) return 'RSS_content:encoded'
		src_def = extend_source(XML_SRC['ATOM_content'], light_n)
		if (mµ.$_('r_txt',			i, src_def, j, do_txt, silent)) return 'ATOM_content'
		src_def = extend_source(XML_SRC['ATOM'], light_n)
		if (mµ.$_('r_txt',			i, src_def, j, do_txt, silent)) return 'ATOM'
		j += 1
	} /* eslint-enable dot-notation */
	return 'RSS'
}
export function set_RSS_variant(items, n) {
	n.xml_type = find_RSS_variant(items, n)
	// console.log('RSS_varian', n.xml_type)
	return extend_source(XML_SRC[n.xml_type], n)
}
/**
 * Returns the JSON object defining the provided sources before any modifications
 * @memberof source_loading
 * @returns {Object} The raw JSON object of provided sources
 */
export async function get_raw_src() {
	let src = await fetch('../json/sources.json')
	return await src.json()
}
/**
 * Extends a source from another given source.
 * @memberof source_loading
 * @param {Object} n The source that inherits its definition from another source
 * @param {Object} extended_src The mother source
 * @returns {Object} The extended source n
 */
export function extend_source(n, extended_src) {
	let new_src = Object.assign({}, extended_src, n)
	new_src.tags = Object.assign({}, extended_src.tags, n.tags)
	return new_src
}

export async function import_custom_src(built_src, custom_src) {
	try {
		for (const i of Object.keys(custom_src)) {
			if (built_src[i]) custom_src[i].overwrite = true
			custom_src[i].custom = true
		}
		built_src = Object.assign(built_src, custom_src)
	} catch (exc) { alert(`Error parsing user custom source (${exc}).`) }
	return built_src
}
/**
 * Loads and build the sources object from the JSON definitions (provided and locally defined)
 * The building process actually extends the sources marked as so or XML types, removes the
 * broken-tagged ones, and adds the computed tags (such as HTTP/HTTPS, Illustrated…).
 * @memberof source_loading
 * @returns {Object} The sources object
 */
export async function build_src(custom_src) {
	let built_src = await get_raw_src()
	if (custom_src)
		import_custom_src(built_src, custom_src)
	// for (let [k, n] of Object.entries(built_src)) {	// we need to remove entries while looping
	for (let k of Object.keys(built_src)) {
		let n = built_src[k]
		let n_tech = n.tags.tech
		if (n_tech && n_tech.includes('broken')) {
			delete built_src[k]
			continue
		}
		// if (!k.startsWith('http')) continue	// was used to avoid false RSS and ATOM
		if (_.is(n.extends)) { // Manage sources to extend
			let extended_src = built_src[n.extends]
			if (_.is(extended_src))
				n = extend_source(n, extended_src)
			else {
				console.error(`${k} extends a broken or missing source ${n.extends}.`)
				delete built_src[k]
				continue
			}
		}
		n.tags.key = k
		n_tech = n.tags.tech
		if (!n.domain_part || n.extends) n.domain_part = _.domain_part(k)
		if (n.search_url.startsWith('https')) // Dynamically add the HTTPS src tag
			_.array_add_once(n_tech, 'HTTPS')
		let src_type = n.tags.src_type
		if (n.r_img || n.r_img_src) // Dynamically add the Illustrated src tag
			_.array_add_once(src_type, 'Illustrated')
		built_src[k] = n
	}
	return built_src
}
/*
 *
 */
export function get_all_search_urls(current_source_selection, source_objs) {
	let all_search_urls = []
	for (let i of current_source_selection) {
		let n = source_objs[i]
		if (!_.is(n)) return console.error('Unknown source', i)
		if (n.search_url)
			all_search_urls.push(n.search_url)
		if (n.redir_url)
			if (Array.isArray(n.redir_url))
				all_search_urls.concat(n.redir_url)
			else
				all_search_urls.push(n.redir_url)
	}
	return all_search_urls
}
/**
 * Creates and returns a copy of the source objects or just the src_name part of sources
 * @memberof source_loading
 * @param {string} src_names The name of the sources to return
 * @returns {Object} The sources object
 */
export function get_prov_src(src_names, built_src) {
	if(src_names === '') return built_src
	let prv = {}
	// for (let t of Object.keys(built_src)) {
	//	prv[t] = built_src[t]
	// }
	if(_.is_str(src_names)) {
		src_names = [src_names]
	}
	if(Array.isArray(src_names)) {
		for(let src_name of src_names) {
			for (let t of Object.keys(built_src)) {
				if(built_src[t].tags.name === src_name) {
					prv[t] = built_src[t]
					//let ext = prv[t].extends
					//if(ext) {prv[ext] = provided_sources[ext]}
					break
				}
			}
		}
	}
	return prv
}
/**
 * @constant The enumeration of the error types.
 */
export const error_code = {
	ERROR: '0',						// not recognized error
	INVALID_URL: 'url_0',			// regex url
	NETWORK_ERROR: 'url_1',			// network
	NO_RESULT: 'result_0',			// no result
	BAD_VALUE: 'value_0',			// malformed value
	INVALID_VALUE: 'value_1',			// invalid, no referenced value
	FATHER_NOT_FOUND: 'value_2',	// father (extends) not found
	DEPRECATED_ATTR: 'attr_0',		// attribute deprecated
	NOT_FOUND_ATTR: 'attr_1',		// attribute not found
	CONFLICTS_ATTR: 'attr_2',		// conflict with attributes
	NAME_ALREADY_TAKEN: 'attr_3', // src name is taken
	SUPERFLUOUS_ATTR: 'attr_4',  // for unknown attributes
	RSS_POSSIBLE: 'rss_0',			// If rss is possibly found
}
/**
 * @constant The enumeration of the error status.
 */
export const error_status = {
	ERROR: 'error',
	WARNING: 'warn',
	INFO: 'info',
	LOG: 'log'
}
/**
 * Creates an error object fitted for the add_error function of the source testing framework
 * @param {string} error_code The code of the error
 * @param {string} src_name The name of the source
 * @param {string} text Error description
 * @param {string} level the error level : warn, error
 * @param {Object} other The other attribute data to add to object
 */
export function create_error(src_def, error_code, src_key, text, level, dest, other={}, log) {
	if(log) console[level](src_key, text)
	let trace =  new Error()
	let n_error = {
		'src_def': src_def,
		'code': error_code,
		'src_key': src_key,
		'text': text,
		'level': level,
		'dest': dest,
		'stack_trace': trace.stack
	}
	// console[level](src_name, text)
	Object.assign(n_error, other)
	return n_error
}
/**
 * The boolean describes if a source is correct by testing its attributes
 * {@link test_attribute}.
 * @memberof test
 */
// let def_attr_is_complete = true
/**
 * The list of the all error found by {@link test_attribute}.
 * @memberof test
 */
let error_attr = []
/**
 * Tests if a source URL is correct and reachable.
 * @memberof test
 * @param {Object} src - the src to test (for get the name)
 * @param {string} url - URL to testget_current_source_search_hosts
 * @param {string} attr_name - name, key of the attribute
 * @param {Object} status - object who contain code in key and true in value
 * {200: true,300: true}
 * @returns none
 */
export function test_url(src, url, attr_name, status) {
	if (µ.is(src[attr_name]) && src[attr_name] !== '') {
		let requete = new XMLHttpRequest()
		requete.open('GET', url, false)
		try {
			requete.send()
			if (!status[requete.status] && requete.status !== 304) {
				// def_attr_is_complete = true
				error_attr.push(
					create_error(
						src,
						error_code.NETWORK_ERROR,
						src.tags.key,
						`${attr_name} ${url} is not correctly receive ${requete.status} code`,
						'warn',
						'test_integrity',
						{ test: 'url', 'url': url, 'status': status.toString() }
					)
				)
			}
			return 200
		} catch (exec) {
			console.error(exec)
			// def_attr_is_complete = true
			error_attr.push(
				create_error(
					src,
					error_code.ERROR,
					src.tags.key,
					`${attr_name} is not correctly receive... ${requete.status} code`,
					'warn',
					'test_integrity',
					{ test: 'url', 'url': url, 'status': status.toString() }
				)
			)
		}
	}
	return
}
export const SRC_DEF_TESTS = {
	'base': [
		'favicon_url',
		'search_url',
		'tags',
		'tags.lang',
		'tags.country',
		'tags.tech',
		'tags.themes',
		'tags.src_type',
		'tags.res_type'],
	'rss': {},
	'format_order': [
		'extends',
		'favicon_url',
		'news_rss_url',
		'news_rss_decode',
		'type',
		'jsonp_to_json',	// -> jsonp_to_json_re
		'json_to_html',
		'xml_type',
		'search_url',
		'search_url_web',
		'method',
		'search_ctype',
		'body',
		'redir_url',
		'domain_part',
		'res_nb',
		'results',
		'r_h1',
		'r_url',
		'r_dt',
		'r_dt_fmt_',
		'r_txt',
		'r_img',
		'r_img_src',
		'r_img_alt',
		'r_img_title',
		'r_by',
		'what_to_fix',
		'tags',
		'tags.name',
		'tags.lang',
		'tags.country',
		'tags.date_locale',
		'tags.themes',
		'tags.tech',
		'tags.src_type',
		'tags.res_type',
		'tags.tz',
		'tags.charset',
		'tags.notes',
		'tags.what_to_fix',
		'tags.warn_mask',
		'tags.key',
		'positive_test_search_term'],
	'rss_url': [
		'/search/{}/feed?orderby=post_date&order=desc',
		'/search/{}/feed2?orderby=post_date&order=desc',
		'/search.rss?q={}',
		'/search/{}/feed.xml',
		'?s={}&feed=rss2'
	],
	'extends': {
		'test': ['inSource']
	},
	'favicon_url': {
		'test': ['url'],
		'url_code': 200
	},
	'type': {
		'test': ['oneofvalues'],
		'test_oneofvalues': ['JSON', 'XML']
	},
	'news_rss_url': {
		'test': ['url'],
		'url_code': 200
	},
	'search_url': {
		'test': ['re'],
		'test_re': '{}',
		'test_level': 'warn'
	},
	'search_url_web': {
		'test': ['re'],
		'test_re': '{}',
		'test_conflict': ['body'],
		'test_level': 'warn'
	},
	'method': {
		'test': ['oneofvalues'],
		'test_oneofvalues': ['POST']
	},
	'body': {
		'test': ['re'],
		'test_re': '{}'
	},
	'redir_url': {
		'test': ['url'],
		'url_code': 200
	},
	'domain_part': {
		'test': ['url'],
		'url_code': 200
	},
	'results': {
		'conflicts': ['r_h1_xpath']
	},
	'r_h1': {
		'conflicts': ['r_h1_xpath']
	},
	'r_url': {
		'conflicts': ['r_url_xpath']
	},
	'r_dt': {
		'conflicts': ['r_dt_xpath']
	},
	'r_txt': {
		'conflicts': ['r_txt_xpath']
	},
	'r_by': {
		'conflicts': ['r_by_xpath']
	},
	'tags': {},
	'tags.lang': {
		'test': ['oneofvalues'],
		'test_oneofvalues': integrity_locales()
	},
	'tags.country': {
		'test': ['oneofvalues'],
		'test_oneofvalues': Object.keys(µ.known_countries_and_code('en')) || []
	},
	'tags.themes': {},
	'tags.tech': {},
	'tags.src_type': {},
	'tags.res_type': {
		'test': ['oneofvalues'],
		'test_oneofvalues': ['audio', 'image', 'text', 'video'],
		'val_can_be_array': true
	},
	'tags.tz': {
		'test': ['oneofvalues'],
		'test_oneofvalues': Intl.supportedValuesOf && Intl.supportedValuesOf('timeZone') || []
	}
}
/**
 *
 */
function integrity_locales() {
	let l = µ.locales()
	l = l && l.concat([
		'oc',  // https://iso639-3.sil.org/code/oci / oc Occitan
		'tet', // https://iso639-3.sil.org/code/tet Tetum
		'tl',  // https://iso639-3.sil.org/code/tgl / tl Tagalog
		'sh',  // https://iso639-3.sil.org/code/hbs / sh Serbo-Croatian
		'xnz', // https://iso639-3.sil.org/code/xnz Kenzi
		'aym', // https://iso639-3.sil.org/code/aym Aymara
		'arz', // https://iso639-3.sil.org/code/arz Egyptian Arabic
		'scn', // https://iso639-3.sil.org/code/scn Sicilian
		'ckb', // https://iso639-3.sil.org/code/ckb Central Kurdish
	]) || []
	return l
}
/**
 * Tests the regular expression for the attr_name of src.
 * @memberof test
 * @param {Object} src_def_tests - The object who contain the description of the format
 * (test_integrity.json)
 * @param {Object} src - The object describing the source
 * @param {string} attr_name - The name (key) of the attribute
 * @param {string} regex - The regex to test the attribute name
 * @returns {Boolean} If the regex match
 */
function test_re(src, attr_name, regex) {
	// let attr = SRC_DEF_TESTS[attr_name]
	let error_level = 'error'
	if (!regex.test(src[attr_name])) {
		/*if (attr.test_level) {
			def_attr_is_complete = true
			error_level = attr.test_level
		}*/
		error_attr.push(
			create_error(
				src,
				error_code.INVALID_VALUE,
				src.tags.key,
				`${src[attr_name]} is an invalid ${attr_name} (${regex})`,
				error_level,
				'test_integrity',
				{ test: 'regex',	'regex': regex.toString(), data: src[attr_name]	}
			)
		)
		return false
	}
	return true
}
/**
 * Tests if the attribute is well formed with the given file-described format.
 * @memberof test
 * @param {Object} sources - The sources.json build object
 * @param {Object} src_def_tests - The test_integrity.json build object
 * @param {Object} src - The source object to test
 * @param {string} attr_name - The name, key of attribute to test
 * @param {boolean} slow - Test with request
 * @param {string} dependent - The string who describe the dependecies of an attributes
 * @returns {Array} Who contain in first a Boolean the validity of the attribute, and the error
 * array [[error, name, level], ...]
 */
export function test_attribute(sources, src, attr_name, slow, dependent='none') {
	let error_attr = []
	let def_attr_is_complete = true
	let local_src = src
	let attr = SRC_DEF_TESTS[attr_name]
	let has_type = false
	let regex = null
	let error_level = 'error'
	let test = true
	if (attr_name.startsWith('tags.')) {
		attr_name = attr_name.replace('tags.', '')
		local_src = src.tags
	}
	if (!_.is(local_src[attr_name])) {
		let is_found_in_conflicts = false
		if (attr.conflicts)
			for (let i of attr.conflicts)
				if (local_src[i])
					is_found_in_conflicts = true
		if (!is_found_in_conflicts) {
			def_attr_is_complete = false
			error_attr.push(create_error(src, error_code.NOT_FOUND_ATTR, src.tags.key,
				`${attr_name} is needed (${dependent} dependencies)`,
				'error', 'test_integrity', {'test': 'search attribute alternative'}))
			return [def_attr_is_complete, error_attr]
		}
	} else if (attr.conflicts) {
		for (let i of attr.conflicts) {
			let conflict_lvl = src
			if (i.startsWith('tags.')) {
				i = i.replace('tags.', '')
				conflict_lvl = conflict_lvl.tags
			}
			if (conflict_lvl[i]) {
				def_attr_is_complete = false
				error_attr.push(create_error(src, error_code.CONFLICTS_ATTR, src.tags.key,
					`${i} is in conflict with attribute ${attr_name}`,
					'error', 'test_integrity', {'test': 'search conflicts'}))
				return [def_attr_is_complete, error_attr]
			}
		}
	}
	if (attr.test) {
		if (attr.test_conflict)
			for (let i of attr.test_conflict)
				if (src[i])
					test = false
		if (test)
			for (const type of attr.test)
				if (slow && type === 'url') {
					if (!test_re(local_src, attr_name, µ.valid_HTTP_URL_RE))
						return [def_attr_is_complete, error_attr]
					test_url(local_src, local_src[attr_name], attr_name, attr.url_code)
				} else if (type === 'oneofvalues') {
					has_type = true
					if (!Array.isArray(local_src[attr_name]))
						local_src[attr_name] = [local_src[attr_name]]
					for (let a of local_src[attr_name])
						has_type = has_type && attr.test_oneofvalues.includes(a)
					if (!has_type) {
						def_attr_is_complete = false
						if (attr.test_level) {
							def_attr_is_complete = true
							error_level = attr.test_level
						}
						error_attr.push(create_error(src, error_code.BAD_VALUE, src.tags.key,
							`${attr_name} source without correct value : ${local_src[attr_name]}`,
							error_level, 'test_integrity', {'test': 'oneofvalues', 'attr': attr_name}))
						return [def_attr_is_complete, error_attr]
					}
				} else if (type === 'values') {
					if (attr.test_values !== local_src[attr_name]) {
						def_attr_is_complete = false
						if (attr.test_level) {
							def_attr_is_complete = true
							error_level = attr.test_level
						}
						error_attr.push(create_error(
							src,
							error_code.BAD_VALUE,
							src.tags.key,
							`source without correct ${attr_name} : ${local_src.tags.name}`,
							error_level,
							'test_integrity',
							{'test': 'values', 'attr': attr_name}
						))
						return [def_attr_is_complete, error_attr]
					}
				} else if (type === 're' &&
					!(attr_name === 'search_url' && src.method && src.method === 'POST') &&
					!(attr_name === 'search_url_web' && src.type && src.type === 'JSON')
				) {
					regex = new RegExp(attr.test_re)
					if (test_re(local_src, attr_name, regex))
						return [def_attr_is_complete, error_attr]
				}	else if (type === 'inSource') {
					if (!sources[local_src[attr_name]]) {
						def_attr_is_complete = false
						if (attr.test_level) {
							def_attr_is_complete = true
							error_level = attr.test_level
						}
						error_attr.push(create_error(src, error_code.FATHER_NOT_FOUND, src.tags.key,
							`${attr_name} is found but the value is not in sources : ${local_src[attr_name]}`,
							error_level, 'test_integrity', {'test': 'inSource', 'attr': attr_name}))
						return [def_attr_is_complete, error_attr]
					}
				}
	}
	if (attr.dependencies)
		for (let j of attr.dependencies)
			test_attribute(sources, src, j, slow, attr_name)
	return [def_attr_is_complete, error_attr]
}

/**
 * Searches in the given source URL for RSS feeds
 * @param {string} src_url The url of the source
 * @param {string} src_name The name of the source
 * @param {Object} src_def_tests The test_integrity.json build Object
 * @returns {Array} Who contain the warn if an rss search exists
 */
export async function search_rss(src, src_url, src_name) {
	let error = []
	const header = new Headers()
	header.append('Content-Type', 'application/rss+xml')
	const init = {
		method: 'GET',
		headers: header,
		mode: 'cors',
		cache: 'default'
	}
	let res
	try {
		for (const i of SRC_DEF_TESTS.rss_url) {
			res = fetch(src_url + i, init).then(
				function (response) {
					if (response.ok) {
						let contentType = response.headers.get('content-type')
						if (contentType && contentType.indexOf('application/rss+xml') !== -1)
							error.push(create_error(src, error_code.RSS_POSSIBLE, src_url,
								`${src_url + i} can be xml type`,
								'warn', 'test_integrity', {'test': 'search rss'}))
					}
				})
			await res
		}
	} catch (exc) {
		console.log(exc, 'RSS', src_name)
	}
	return error
}
/**
 * Contains the already encountered source names.
 * @memberof test
 * @type {Object}
 */
let reserved_names = {}
/**
 * reserved_names content is dup and replace by an empty Object.
 */
export function clear_reserved_names() {
	reserved_names = {}
}
/**
 * Fills the reserved name list with the given source names
 * @param {Object} src A source object (intended to contain all the known sources)
 */
export function load_src_reserved_names(source_objs) {
	for (const [k, n] of Object.entries(source_objs))
		reserved_names[n.tags.name] = k
}
/**
 * Checks if the name has already been found in the tested sources
 * @param {string} name The name of the source to test
 * @returns {boolean}
 */
export function is_src_name_uniq(name) {
	return typeof reserved_names[name] === 'undefined'
}
/**
 * Tests if the given source is correct, given src_def_tests.
 * @memberof test
 * @param {Object} sources - The sources.json built object
 * @param {Object} src - The uniq source object to test
 * @param {Object} src_def_tests - The test_integrity.json built object
 * @param {Function} call_back - A callback function to run against the result
 * classic
 * @returns {Array} Boolean validity of the sources and error array
 * [True, [[error, name, level], ...]]
 */
const deprecated = ['r_dt_re', 'xml_type', 'h_title', 'headline_url']
const no_css = ['search_url_web']
const res_base = ['r_h1', 'r_url', 'r_dt']
const results = ['results']
export async function test_src_integrity(sources, src_key, call_back=undefined, slow=false) {
	// let start = new Date()
	let src = sources[src_key]
	let errors = []
	let def_src_is_complete = true
	let needed = SRC_DEF_TESTS.base
	let type = 'GLOBAL or '
	let known_src = {}
	for (let i of deprecated)
		if (src[i]) {
			def_src_is_complete = true
			errors.push([`${i} is deprecated`, src.tags.name,	'warn'])
		}
	if (src.type === 'XML'){
		type = type.concat('XML')
		needed = needed.concat(no_css)
	} else if (src.type === 'JSON') {
		type = type.concat('JSON')
		needed = needed.concat(no_css)
		needed = needed.concat(res_base)
		needed = needed.concat(results)
		needed.push('type')
	} else {
		type = type.concat('CSS')
		needed = needed.concat(res_base)
		needed = needed.concat(results)
		if (src.r_dt !== 'time' && src.rd_attr !== 'datetime')
			needed.push('tags.tz')
	}
	if (slow && src.type !== 'XML') {
		errors = errors.concat(await search_rss(src, src_key, src.tags.name))
	}
	if (src.method === 'POST') {
		type = type.concat(' POST')
		needed = needed.concat(['method', 'body'])
	}
	for (let i of needed)
		try {
			let [def_attr_is_complete, attr_errors] = test_attribute(sources, src, i, slow, type)
			def_src_is_complete = def_src_is_complete && def_attr_is_complete
			errors = errors.concat(attr_errors)
		} catch (exec) {
			console.error(i, exec)
		}
	for (let k of Object.keys(src))
		if (!SRC_DEF_TESTS.format_order.includes(k.replace(
			/_xpath$|_re$|_attr$|_fmt_\d+$|_tpl$|_com$/, '')))
			errors.push(create_error(src, error_code.SUPERFLUOUS_ATTR, src.tags.key,
				`${src.tags.name} unknown key ${k}`, 'warn', 'test_integrity')
			)
	if (!known_src[src.tags.name])
		known_src[src.tags.name] = src_key
	if (known_src[src.tags.name] !== src_key) {
		def_src_is_complete = false
		errors.push(create_error(src, error_code.NAME_ALREADY_TAKEN, src.tags.key,
			`${src.tags.name} is already used`, 'error', 'test_integrity')
		)
	}
	// console.log('tested in ', new Date() - start, ' i'm inevitable')
	if (typeof call_back === 'function')
		call_back([def_src_is_complete, errors])
	else
		return [def_src_is_complete, errors]
}
/**
 * Formats the sources.json in the given order. Furthermore r_dt_fmt describes all
 * the r_dt_fmt and all attributes except entries with _xpath, _attr, _re, _tpl, _com
 * dynamically added. The missing attributes are pushed at the end of the corresponding source.
 * @param {Object} raw_src - The sources.json unbuild Object
 * @param {Array} order - The order of the element in the sources
 * @returns {Object} - The well formated raw sources
 */
export function reformat_sources(raw_src, order) {
	let new_src = {}
	for (const i of Object.keys(raw_src))
		new_src[i] = reformat_source(raw_src[i], order)
	return new_src
}
export function reformat_source(raw_i, order) {  // new_i is a source definition
	let new_i = {}
	for (const attr of order)
		if (attr === 'tags')
			new_i[attr] = {}
		else if (attr.startsWith('tags.')) {
			let tag_name = attr.replace('tags.', '')
			new_i.tags[tag_name] = raw_i.tags[tag_name]
		} else if (attr === 'r_dt_fmt_') {
			for (const j of Object.keys(raw_i))
				if (j.startsWith('r_dt_fmt_'))
					new_i[j] = raw_i[j]
		} else
			for (let suff of ['', '_xpath', '_attr', '_re', '_tpl', '_com'])
				if (raw_i[`${attr}${suff}`])
					new_i[`${attr}${suff}`] = raw_i[`${attr}${suff}`]
	for (let j of Object.keys(raw_i))  // don't lose something unknown
		if (j !== 'tags')
			new_i[j] = raw_i[j]
	for (let j of Object.keys(raw_i.tags))
		try {
			if (!new_i.tags[j])
				new_i.tags[j] = raw_i.tags[j]
		} catch (exc) {
			console.warn(raw_i.tags.name, `new_src.tags[${j}] not found`)
		}
	return new_i
}
/**
 * Update all the source favicons with {@link get_favicon} and the headline_rss, then sends
 * a new sources.json file to the user.
 * @memberof new_update_sources
 * @async
 */
// export async function update_favicon_rss(source_objs) {
export async function update_favicon_rss() {
	let raw_src = await get_raw_src()
	let i = 0
	for (let k of Object.keys(raw_src)) {
		console.log(i++)
		/* let fav = µ.get_favicon(i)
		if (fav !== '' && fav !== src[i].favicon_url)
			raw_src[i].favicon_url= fav*/
		// if (typeof source_objs[k].headline_url === 'undefined')
		//	console.log(k)
		// continue
		// delete raw_src[k].headline_url
		const n = raw_src[k]
		if (typeof (n.tags.positive_test_search_term) !== 'undefined')
			delete n.tags.positive_test_search_term
		raw_src[k] = n
		/*if (n.tags.src_type && n.tags.src_type.includes('Indep.')){
			n.tags.tech.push('Indep.')
			_.array_remove_once(n.tags.src_type, 'Indep.')
		}
		if (typeof (n.h_title) !== 'undefined')
			delete n.h_title
		*//*
		if (typeof source_objs[k].news_rss_url !== 'undefined' ||
			typeof source_objs[k].headline_url === 'undefined'
		)
			continue
		let rss = ''
		// missing
		if (rss !== '' && rss !== source_objs[k].news_rss_url)
			raw_src[k].news_rss_url = rss
		*//*if (raw_src[i].news_rss_url) {
			let rss_url = ['/feed', '/rss']
			for (const j of rss_url) {
				if (mµ.test_url(src[i], i + j, 'news_rss_url', { '200': true }) === 200)
					raw_src[i].news_rss_url = i + j
			}
		}*/
		// break
	}
	let json_str = JSON.stringify(reformat_sources(raw_src, SRC_DEF_TESTS.format_order),null,'\t')
	µ.upload_file_to_user(`${_.ndt_human_readable()}_favicon_updated_sources.json`, json_str)
}
/*
 *
 */
export function format_search_url(search_url, token, max_res_by_src, search_ctype) {
	return search_url.replace('{}', !search_ctype ? encodeURIComponent(token) : token)
		.replace('{#}', max_res_by_src)
}
/*
 *
 */
export function get_search_url(n) { return n.search_url_web ? n.search_url_web : n.search_url }
