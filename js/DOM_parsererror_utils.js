// SPDX-FileName: ./DOM_parsererror_utils.js
// SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

/**
 * The DOM parser used to generate a DOM error example
 * More info : https://stackoverflow.com/questions/11563554/how-do-i-detect-xml-parsing-errors-when-using-javascripts-domparser-in-a-cross
 * @type {DOMParser}
 * @constant
 */
export const DOM_parser = new DOMParser()
console.groupCollapsed('DOM parser error initialization')
console.info('The next "XML Parsing Error: unclosed token" at line 1 col 1 is '
	+ 'useful for function "is_DOM_parsererror()".')
/**
 * A DOM error made to get its namespace
 * @type {Document}
 * @constant
 */
const DOM_err = DOM_parser.parseFromString('<', 'text/xml')
/**
 * The namespace of the DOM error example
 * @type {Document}
 * @constant
 */
const DOM_err_NS = DOM_err.getElementsByTagName('parsererror')[0].namespaceURI
setTimeout(() => {
	console.info('More errors would not be intended')
	console.groupEnd()
}, 0)
/**
 * Check if the dom has parser error.
 * @param {string} dom The dom fargment to check
 * @returns {boolean} parser error ?
 */
export function is_DOM_parsererror(DOM, dbg=false) {
	/* if (DOM_err_NS === 'http://www.w3.org/1999/xhtml') {
		// In PhantomJS the parserirror element doesn't seem to have a special namespace,
		// so we are just guessing here :(
		return dom.getElementsByTagName('parsererror').length > 0
	}*/
	return get_DOM_parsererror(DOM, dbg).length > 0
}
// https://developer.mozilla.org/fr/docs/Web/API/TextDecoder/TextDecoder
//
export function get_DOM_parsererror(DOM, dbg=false) {
	const e = DOM.getElementsByTagNameNS(DOM_err_NS, 'parsererror')
	dbg && e.length && console.error(e)
	return e
}
/**
 *
 */
export async function get_HTML_fragment(URL) {
	try {
		let page
		try {
			page = await fetch(URL)
		} catch (exc) {
			console.warn(exc)
			page = {ok:false}
		}
		if (!page.ok) return ''
		let page_text = await page.text()
		let ctype = page.headers.get('content-type')
		ctype = ctype.split(';')[0] // remove charset
		let rep = DOM_parser.parseFromString(page_text, ctype)
		if (is_DOM_parsererror(rep))
			throw new Error('DOM parser '+get_DOM_parsererror(rep).textContent)
		// console.log('rep', rep)
		return rep
	} catch (err) {
		console.error(err)
		return null
	}
}
/**
 * Remove HTML tags from a string
 * @example strip_HTML_tags('<html><body>Hello</body></html>') return 'Hello'
 * @param {string} str The string expected to content HTML code
 * @returns the string without HTML tags
 */
export function strip_HTML_tags (str) {
	return DOM_parser.parseFromString(str, 'text/html').body.textContent // sanitize inputs
}
