#!/usr/bin/env python3
# SPDX-FileName: ./selenium_testing.py
# SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
# SPDX-License-Identifier: GPL-3.0-only

import os
import re
import time
import traceback
import requests

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.chromium.options import ChromiumOptions
from selenium.webdriver.chromium.service import ChromiumService
from selenium.common.exceptions import StaleElementReferenceException

# Starter guide
# - Download the latest geckodriver: https://github.com/mozilla/geckodriver/releases
# - Download latest Firefox developper edition (it must be the developper edition)
# - Put those at FF_DRIVER_PATH and FF_BINARY_PATH folder
#
# Selenium Documentation
# - How to use new Selenium 4.0 functionalities to avoid having deprecation warnings
# -- https://www.selenium.dev/documentation/webdriver/getting_started/upgrade_to_selenium_4/
# - Useful guidebook for main Selenium commands in Python (deprecated from 2018... but helped)
# -- https://selenium-python.readthedocs.io/getting-started.html
# - Useful guidebook for main Selenium commands in other langages
# -- https://www.selenium.dev/documentation/webdriver/

FF_DRIVER_PATH = os.environ.get('FF_DRIVER_PATH') or '/usr/bin/geckodriver'
FF_BINARY_PATH = os.environ.get('FF_BINARY_PATH') or os.environ['HOME'] + \
	'/.mozilla/firefox-developer-edition/firefox/firefox-bin'
# Don't forget the row parameter 'r' before path string on Windows
CIUM_DRIVER_PATH = os.environ.get('CIUM_DRIVER_PATH') or '/usr/bin/chromedriver'
CIUM_BINARY_PATH = os.environ.get('CIUM_BINARY_PATH') or '/usr/bin/chromium'
# print(os.environ.get('HEADLESS_DRIVER'), bool(os.environ.get('HEADLESS_DRIVER')))
HEADLESS_DRIVER = bool(os.environ.get('HEADLESS_DRIVER'))  # set to '' to get False else 1
OFFLINE_TESTS = bool(os.environ.get('OFFLINE_TESTS'))
SELENIUM_BROWSER = os.environ.get('SELENIUM_BROWSER')
INTERNAL_PROTOCOL = SELENIUM_BROWSER == 'firefox' and 'moz-extension:' or 'chrome-extension:'
DBG = False


def txt(*args):
	return ' '.join(args)


def Exc(*args):
	return Exception(txt(*args))


class js_is_true(object):
	def __init__(self, js_bool_expr):
		self.js_bool_expr = js_bool_expr

	def __call__(self, driver):
		return driver.execute_script(f'return {self.js_bool_expr}')


def wait_until_javascript_loaded(wait):
	wait.until(js_is_true('document.body.classList.contains("javascript_loaded")'))


def get_mp_page(driver, wait, ext_id, filename):
	DBG and print('get_mp_page', filename)
	driver.get(INTERNAL_PROTOCOL + '//{}/html/{}'.format(ext_id, filename))
	try:
		wait_until_javascript_loaded(wait)
	except Exception as exc:
		print(txt('waiting for', filename))
		raise exc


def change_language(driver, lng):
	''' Changing language in settings.html '''
	drop = Select(driver.find_element(By.ID, 'mp_lang'))
	drop.select_by_value(lng)
	# drop.options


def test_empty_source_selections(driver, wait, ext_id):
	get_mp_page(driver, wait, ext_id, 'index.html')
	empty_sel_txt = driver.find_element(By.ID, 'empty_sel_txt').get_attribute('textContent')
	driver.get(INTERNAL_PROTOCOL + f'//{ext_id}/html/index.html?tech=cherry-pick_sources')
	wait.until(EC.alert_is_present())
	# assert driver.switch_to.alert.text == 'This choice would result in an empty selection.'
	assert driver.switch_to.alert.text == empty_sel_txt
	driver.switch_to.alert.accept()
	wait.until_not(EC.alert_is_present())
	tags_tech = Select(driver.find_element(By.ID, 'tags_tech'))
	tags_tech_list = [a.get_attribute('value') for a in tags_tech.all_selected_options]
	assert 'cherry-pick_sources' not in tags_tech_list


def get_all_pages_list():
	html_dir = os.scandir('html')
	file_lst = []
	for entry in html_dir:
		# print(txt('entry', str(entry), 'entry.is_file()', str(entry.is_file()), entry.name))
		if entry.is_file():
			file_lst.append(entry.name)
	return file_lst


def test_all_pages(driver, wait, ext_id, file_lst, hook=None, *args):
	# print(file_lst)
	for entry in file_lst:
		get_mp_page(driver, wait, ext_id, entry)
		if hook:
			hook(driver, wait, ext_id, entry, *args)


def test_bg_settings(driver, wait, ext_id, file_lst, ref_value):
	''' can't use test_all_pages because we want to avoid some exc and still finish the loop '''
	for entry in file_lst:
		get_mp_page(driver, wait, ext_id, entry)
		try:
			test_bg_color(driver, entry, ref_value)
		except Exception as exc:
			if hasattr(exc, 'args') and exc.args[0].startswith('background.html'):
				pass
			else:
				raise exc


def test_bg_color(driver, filename, rgb_css_color):
	bg = driver.execute_script('''
		let style = getComputedStyle(document.body)
		return style.getPropertyValue('background-color')
	''')
	# DBG and print(txt(filename, 'bg color should be', rgb_css_color, 'and is', bg))
	if not re.match(rgb_css_color, bg):
		raise Exc(filename, 'bg color is not /', rgb_css_color, '/ but', bg)


def test_page_health(driver, wait, ext_id, filename):
	driver.execute_script('are_imgs_loaded()')
	driver.execute_script('are_internal_links_broken()')
	not OFFLINE_TESTS and test_external_links(driver, wait, ext_id, filename)


def test_external_links(driver, wait, ext_id, filename):
	links = driver.find_elements(By.TAG_NAME, 'a')
	href = ''
	r = None
	for a in links:
		try:
			href = a.get_attribute('href')
			# DBG and print('will test ', href)
			# if href and href.startswith('https://developer.mozilla.org') or \
			# href and href.startswith('https://framagit.org'):
			# continue
			if href and href.startswith('mailto:') and href != 'mailto:contact@meta-press.es':
				raise Exc('[{}] {} unknown mailto:'.format(filename, href))
			if href and not href == '.' and a.is_displayed() and \
				not href.startswith(INTERNAL_PROTOCOL) and \
				not href.startswith('mailto:') and \
				not href.startswith('btc:') and \
				not href.startswith('eth:') and \
				not href.startswith('g1:'):
				# DBG and print(a.is_displayed(), a.text, a.tag_name, a.size, a.get_attribute('class'))
				r = requests.get(href)
				if r.status_code != 200:
					raise Exc('[{}] href={} id={} class={} not 200 but {}'.format(
						filename, href, a.get_attribute('id'), a.get_attribute('class'), r.status_code))
		except StaleElementReferenceException:
			print('StaleElementReferenceException', a)
			pass
		except Exception:
			raise Exc(filename, 'href', href, 'r', r)


def test_settings(driver, wait, ext_id, file_lst):
	get_mp_page(driver, wait, ext_id, 'settings.html')
	change_language(driver, lng='es')
	wait.until(EC.text_to_be_present_in_element((By.ID, 'settings'), '⚙ Configuración'))
	change_language(driver, lng='eo')
	wait.until(EC.text_to_be_present_in_element((By.ID, 'settings'), '⚙ Agordoj'))
	change_language(driver, lng='fr')
	wait.until(EC.text_to_be_present_in_element((By.ID, 'settings'), '⚙ Réglages'))
	change_language(driver, lng='en')
	wait.until(EC.text_to_be_present_in_element((By.ID, 'settings'), '⚙ Settings'))
	drop_bg = Select(driver.find_element(By.ID, 'dark_background'))
	drop_bg.select_by_value('dark')
	test_bg_settings(driver, wait, ext_id, file_lst, 'rgb\\(0, 0, 0\\)')
	get_mp_page(driver, wait, ext_id, 'settings.html')
	iframe = driver.find_elements(By.TAG_NAME, 'iframe')[0]
	driver.switch_to.frame(iframe)
	test_bg_color(driver, 'donation iframe', 'rgb\\(0, 0, 0\\)')
	driver.switch_to.window(driver.window_handles[0])
	drop_bg = Select(driver.find_element(By.ID, 'dark_background'))
	drop_bg.select_by_value('light')
	test_bg_settings(driver, wait, ext_id, file_lst, 'rgb\\(255, 255, 255\\)')
	get_mp_page(driver, wait, ext_id, 'settings.html')
	iframe = driver.find_elements(By.TAG_NAME, 'iframe')[0]
	driver.switch_to.frame(iframe)
	test_bg_color(driver, 'donation iframe', 'rgb\\(255, 255, 255\\)')


def test_index(driver, wait, ext_id):
	get_mp_page(driver, wait, ext_id, 'index.html')
	# if browser in french, tags are set to french previously
	# son they are not "default" now that we set lang=en
	driver.execute_script('document.getElementById("reset_tag_sel").click()')
	wait.until(js_is_true('document.getElementById("tags_lang").value == "en"'))
	driver.refresh()
	b = driver.find_element(By.ID, 'tags_handle')
	assert b.text == '+'
	b.click()
	wait.until(EC.text_to_be_present_in_element((By.ID, 'tags_handle'), '-'))
	wait.until(EC.visibility_of_element_located((By.ID, 'view_select_src')))
	# wait.until(EC.visibility_of_element_located((By.ID, 'cur_tags_perm_nb')))
	# Index.html : Importing sources
	# mp_import_RSS = driver.find_element(By.ID, 'mp_import_RSS')
	# mp_import_RSS.click()
	# mp_import_RSS.send_keys("C:\\meta-press\\source_test\\source_rss.rss", Keys.RETURN)
	# La fenêtre d'importation du fichier est la seule à s'ouvrir en mode headless...
	# Il faudra sans doute interagir avec cette fenêtre d'une autre façon... => "WyGet" ?
	# wait.until(EC.frame_to_be_available_and_switch_to_it)
	# driver.switch_to.parent_frame()
	# wait.until(EC.alert_is_present())
	# hit enter
	# Robot r = new Robot();
	# r.keyPress(KeyEvent.VK_ENTER);
	# r.keyRelease(KeyEvent.VK_ENTER);
	# // switch back
	# driver.switchTo().activeElement();

	# ActionChains(driver).double_click(clk_element).click(clk_element).perform()
	# print('Importing RSS search results OK.')


def gen_firefox_driver():
	ff_opt = FirefoxOptions()
	ff_opt.binary = FF_BINARY_PATH
	ff_opt.headless = HEADLESS_DRIVER
	ff_service = FirefoxService(executable_path=FF_DRIVER_PATH)
	driver = webdriver.Firefox(service=ff_service, options=ff_opt)
	driver.install_addon('.', temporary=True)
	# driver.implicitly_wait(10)
	# driver.maximize_window
	return driver


def gen_chromium_driver():
	cium_opt = ChromiumOptions()
	cium_opt.binary_location = CIUM_BINARY_PATH
	cium_opt.headless = HEADLESS_DRIVER
	cium_opt.add_argument("load-extension=.")
	cium_service = ChromiumService(executable_path=CIUM_DRIVER_PATH, start_error_message="coucou")
	driver = ChromiumDriver(service=cium_service, options=cium_opt,
		browser_name="MP Chromium",
		vendor_prefix="")
	return driver


def get_ext_id(driver):
	wait = WebDriverWait(driver, timeout=10)  # 10s
	# wait = WebDriverWait(driver, timeout=HEADLESS_DRIVER and 10 or 999999)  # 10s or ~10d
	wait.until(EC.number_of_windows_to_be(2))
	driver.switch_to.window(driver.window_handles[0])	 # Empty FF tab
	driver.close()
	driver.switch_to.window(driver.window_handles[0])	 # MP Welcome page
	ext_id = driver.current_url.split(INTERNAL_PROTOCOL+'//')[1].split('/html/welcome.html')[0]
	return (wait, ext_id)


def init():
	try:
		if (SELENIUM_BROWSER == 'firefox'):
			driver = gen_firefox_driver()
		else:
			driver = gen_chromium_driver()
		print('Selenium started browser', driver.name)
		(wait, ext_id) = get_ext_id(driver)
		file_lst = get_all_pages_list()
		test_all_pages(driver, wait, ext_id, file_lst, test_page_health)
		# + test if links are 200
		# + test if images are 200
		test_settings(driver, wait, ext_id, file_lst)
		test_index(driver, wait, ext_id)
		test_empty_source_selections(driver, wait, ext_id)
		# + test show advenced serach
		# test show display_panel / source list
		# test import results
		# test export results
		# - Select french only newspapers
		# - Check headline loading
		# - Search quadrature du net
		# - Filter results of only one newspaper
		# - Filter total back
		# - Filter only last week
		# - Sub-query with : quadrature
		# - Export all (JSON)
		# - Export sélection (JSON, RSS, ATOM)
		# - Import JSON, JSON, RSS, ATOM
		# - Remove tag to search in ALL newspapers
		# - Re-test in responsive design mode
		# - Test blockage d'image, et chargement à la demande
		# - Tester le chargement des headlines, automatique et à la demande
		#  - le changement de taille de pages
		#  - le rechargement
		# - tester les paramètres un par un

	except Exception:
		traceback.print_exc()
		not HEADLESS_DRIVER and time.sleep(999999)
	finally:
		driver.quit()


init()
