#!/bin/bash

#
# Starts Firefox with a new temporary profile which is deleted when FF exits.
#
# https://gist.github.com/oliver/5033c90d850c7cb99949eb04d12dbb21
#

tempdir=$(mktemp -d --tmpdir firefox-priv.XXXXXX)
echo "$(date): starting Firefox with temporary profile at '$tempdir'"

cleanupOnExit ()
{
    echo "$(date): removing temporary Firefox profile at '$tempdir'"
    rm -rf "$tempdir"
}
trap cleanupOnExit EXIT


# customize some preferences in new profile:
cat > "$tempdir/user.js" << END_OF_TEXT

// disable any data reporting and also any notification page about this:
user_pref("datareporting.policy.dataSubmissionEnabled", false);

// disable some WebRTC/multimedia stuff
user_pref("media.peerconnection.enabled", false);
user_pref("media.navigator.enabled", false);

// enable some privacy settings
user_pref("browser.selfsupport.url", "");

// set default download directory to /tmp
user_pref("browser.download.dir", "/tmp");
user_pref("browser.download.defaultFolder", "/tmp");
user_pref("browser.download.downloadDir", "/tmp");
user_pref("browser.download.start_downloads_in_tmp_dir", true);

// minimize starting time
user_pref("browser.tabs.remote.autostart", false);

user_pref("devtools.onboarding.telemetry.logged", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.ping-centre.telemetry", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
user_pref("toolkit.telemetry.hybridContent.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("devtools.onboarding.telemetry.logged", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("datareporting.sessions.current.clean", true);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("datareporting.sessions.current.clean", true);

END_OF_TEXT

firefox --headless --no-remote --new-instance --profile "$tempdir" --private-window "$@"
